<?php

use OAuth\Common\Storage\Session;

return [ 
	
	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => new Session(), 

	/**
	 * Consumers
	 */
	'consumers' => [    

		/**
		 * Facebook
		 */
		'Facebook' => [
		    'client_id'     => '1466663296989400',
		    'client_secret' => '5c11fa0b4cca0c4acddd7cd77a5644dd',
		    'scope'         => ['email'],
		],		

	]

];