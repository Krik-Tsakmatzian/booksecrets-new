<?php

return [
    
    'api_url' => env('COMMON_API_URL'),
    'base_url' => env('COMMON_BASE_URL'),
    'blog_url' => env('COMMON_BLOG_URL')
    
];
