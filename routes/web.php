<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@getIndex')->middleware('userTypeCheck');

Route::get('/visitors', 'HomeController@getVisitors')->middleware('userTypeCheck');

Route::get('/authors', 'HomeController@getAuthors')->middleware('userTypeCheck');

Route::get('/publishers', 'HomeController@getPublishers')->middleware('userTypeCheck');

Route::get('/who-we-are', 'HomeController@getWhoWeAre')->middleware('userTypeCheck');

Route::get('/who-we-are-en', 'HomeController@getWhoWeAreEN')->middleware('userTypeCheck');

Route::get('/terms', 'HomeController@getTerms')->middleware('userTypeCheck');

Route::post('/contact-form', 'HomeController@postContactForm');

// Route::get('/', function () {
//     return view('welcome');
// });


// Route::controller('user', 'UserController');

// =============================================
// API ROUTES ==================================
// =============================================

Route::prefix('author')->group(function() {
	Route::get('/{id}', 'AuthorController@getIndex');
});


Route::prefix('publisher')->group(function() {
	Route::get('/{id}', 'PublisherController@getIndex');
});


// =============================================
// CATCH ALL ROUTE =============================
// =============================================
// all routes that are not home or api will be redirected to the frontend
// this allows angular to route them
// App::missing(function($exception)
// {
// 	return View::make('errors.404');
// });



Route::any('{all}', function() {
	abort(404);
});