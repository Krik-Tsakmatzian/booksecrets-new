<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// VALIDATION - LOGIN - SIGN UP
Route::post('sign-up', 'ApiController@postSignUp');
Route::post('login', 'ApiController@postLogin');
Route::post('validate', 'ApiController@postValidate');
Route::post('validate-book', 'ApiController@postValidateBook');

// BOOKS
Route::post('bookslist', 'ApiController@postBookslist');
Route::post('bookview', 'ApiController@postBookview');

// FEED
Route::post('feedlist', 'ApiController@postFeedlist');
Route::post('feedsearch', 'ApiController@postFeedsearch');
Route::post('feed-booklist', 'ApiController@postFeedBooklist');
Route::post('feed-secretlist', 'ApiController@postFeedBooklist');

// FAVOURITE
Route::post('favorites', 'ApiController@postFavorites');
Route::post('edit-favorite', 'ApiController@postEditFavorite');

// SECRETS
Route::post('secret-rate', 'ApiController@postSecretRate');
Route::post('secret-report', 'ApiController@postSecretReport');
Route::post('secret-delete', 'ApiController@postSecretDelete');
Route::post('secret-post', 'ApiController@postSecretPost');
Route::post('secret-books', 'ApiController@postSecretBooks');
Route::post('secret-view', 'ApiController@postSecretView');
Route::post('my-secrets', 'ApiController@postMySecrets');
Route::post('adv-book-search', 'ApiController@postAdvBookSearch');

// LIBRARY SEARCH - REMOVE - ADD
Route::post('library-add', 'ApiController@postLibraryAdd');
Route::post('library-remove', 'ApiController@postLibraryRemove');
Route::post('user-book-search', 'ApiController@postUserBookSearch');
Route::post('book-update', 'ApiController@postBookUpdate');

// EMBED - UPLOAD - CROP
Route::post('embed', 'ApiController@postEmbed');
Route::post('upload', 'ApiController@postUpload');
Route::post('crop', 'ApiController@postCrop');

// PROFILE
Route::post('profile-update', 'ApiController@postProfileUpdate');
Route::post('profile-info', 'ApiController@postProfileInfo');

// ACCOUNT RESETE - FORGET PASS
Route::post('forgot-password', 'ApiController@postForgotPassword');
Route::post('reset-password', 'ApiController@postResetPassword');
Route::post('claim-account', 'ApiController@postClaimAccount');

// REPORTS
Route::post('books-report', 'ApiController@postBooksReport');
Route::post('books-users-report', 'ApiController@postBooksUsersReport');
Route::post('pub-authors-count', 'ApiController@postPubAuthorCount');
Route::post('secrets-report', 'ApiController@postSecretsReport');
Route::post('secrets-stats-report', 'ApiController@postSecretsStatsReport');