<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| SECRETS Routes
|--------------------------------------------------------------------------
|
| Here is where you can call 'secrets' routes for your application.
|
*/

Route::get('feed', 'SecretsController@getFeed');

Route::get('favorites', 'SecretsController@getFavorites');

Route::get('add', 'SecretsController@getAdd');

Route::get('add/{id}', 'SecretsController@getAddByUser');

Route::get('all', 'SecretsController@getAll');

Route::get('edit/{id}/{secret}', 'SecretsController@getEdit');

Route::get('{id}/{secret}', 'SecretsController@getIndex');

Route::get('{id}', 'SecretsController@getBook');