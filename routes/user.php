<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| USERS Routes
|--------------------------------------------------------------------------
|
| Here is where you can call 'users' routes for your application.
|
*/
Route::get('/user-info/{id}', 'UserController@getIndex');

Route::get('books', 'UserController@getBooks');

Route::get('library', 'UserController@getLibrary');

Route::get('login', 'UserController@getLogin');

Route::get('logout', 'UserController@getLogout');

Route::get('statistics', 'UserController@getStatistics');

Route::get('forgot-password', 'UserController@getForgotPassword');

Route::get('claim-account', 'UserController@getClaimAccount');

Route::get('facebooklogin-author', 'UserController@getFacebookloginAuthor');

Route::get('facebooklogin', 'UserController@getFacebooklogin');