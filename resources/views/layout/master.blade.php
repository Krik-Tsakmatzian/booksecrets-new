<!doctype html>
<html lang="en">
<head>
	<title>@yield('title')</title>
	<meta charset="UTF-8">

	@include('inc/head')

</head>

	<body ng-app="myApp">
	
	@include('inc/header')
	
	  <!--[if lt IE 7]>
	      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	  <![endif]-->
	
	  @yield('content')
		
		
	
	</body>
</html>
