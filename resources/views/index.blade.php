<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>booksecrets</title>

	@include('inc/head')

</head>

<body ng-app="myApp" id="main">

    <section class="news section" ng-style="{'background-image':'url('+background+')'}" ng-controller="mainController">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-md-6 blog-new-wrap">

                    <div class="header">
                        <img src="{{ asset('dist/img/logo.png') }}" alt="Bookesecrets logo">
                    </div>
                    <div class="main">
                        <h1 class="title">Τα βιβλία σας πλέον έχουν κι άλλα να σας πουν με την Νέα μας έκδοση!</h1>

                        <div class="news-slide-wrap">
                            <div id="owl-news" class="owl-carousel">
                                @foreach ($posts as $p)
                                    <div class="item">
                                        <div class="item-info">
                                            <h3 onclick="location.href = '{{config('common.blog_url')}}{{$p->title_slug}}';">{{ $p->title }}</h3>
                                            <p>{{ mb_substr($p->summary, 0, 100).'...' }}</p>
                                        </div>
                                        <div class="img-wrap" style="background-image: url('{{config('common.blog_url')}}{{ $p->image_cropped[$p->image] }}')">
                                            <img src="{{config('common.blog_url')}}{{ $p->image_cropped[$p->image] }}" alt="{{ $p->title }}">
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="social-wrap">
            <span>Follow Us</span>
            <ul class="list-unstyled">
                <li><a href="https://www.facebook.com/booksecretscom/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://twitter.com/booksecretscom" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://www.linkedin.com/company/5397319" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="https://www.instagram.com/booksecrets/" target="_blank"><i class="fa fa-instagram"></i></a></li>
            </ul>
        </div>
    </section>

    <section class="section user-sec" id="sign-up" ng-controller="mainController">
        <div class="container">
            <div class="row">
                <!--<div class="col-md-12 col-xs-12 col-sm-12 text-right">-->
                <!--    <em>en / </em><em> ελ</em>-->
                <!--</div>-->
                <div class="col-md-6 col-sm-12 col-xs-12 inner-sec">
                    @include('inc/svgs/lamp')
                    <h2 class="title">Πώς δουλεύει</h2>
                    <p>Σκοπός μας είναι να απογειώσουμε την αναγνωστική εμπειρία. Πιστεύουμε πως κάθε βιβλίο περιέχει ζωντανές σκέψεις και συναισθήματα, ας του δώσουμε την δυνατότητα, να συνεχίζει να μιλάει.</p>

                    <div class="video-wrap" ng-init="player=false">
                        <div class="preview">
                            <button class="btn btn-empty btn-play" ng-hide="player" ng-click="play()"><i class="fa fa-play"></i></button>
                            <iframe id="player1" ng-show="player" src="https://player.vimeo.com/video/145871496?api=1&player_id=player1" width="500" height="280" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="width: 100%;"></iframe>
                        </div>
                    </div>

                    <p>Κατεβάστε σήμερα την εφαρμογή μας!</p>
                    <ul class="list-unstyled list-inline">
                        <li><a href="https://play.google.com/store/apps/details?id=com.booksecrets" target="_blank"><img src="{{ asset('dist/img/googleplay.png') }}"></a></li>
                        <li><a href="https://itunes.apple.com/gr/app/booksecrets-discover-secrets/id1031918264?l=el&mt=8" target="_blank"><img src="{{ asset('dist/img/applestore.png') }}"></a></li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 inner-sec">
                    @include('inc/svgs/heart')
                    <h2 class="title">Your books, alive!</h2>

                    @if ( !session()->has('token') )
                    <uib-tabset id="user-tabs">
                        <span class="sign-as">Εγγραφείτε ως: </span>
                        <small>Έχετε ήδη λογαριασμό; <a href ng-click="tabs[3].active = true">Συνδεθείτε</a>.</small>
                        <uib-tab ng-repeat="tab in tabs" heading="<% tab.title %>" active="tab.active">
                            <!--visitor-->
                            <div ng-if="tab.id == 1">
                                @include('inc/registration/user')
                            </div>

                            <!--author-->
                            <div ng-if="tab.id == 2">
                                @include('inc/registration/author')
                            </div>

                            <!--publisher-->
                            <div ng-if="tab.id == 3">
                                @include('inc/registration/publisher')
                            </div>

                            <!--login-->
                            <div ng-if="tab.id == 4">
                                <form angular-validator-submit="login()" name="loginForm" class="row signupForm" novalidate angular-validator>
                                    <div class="form-group">
                                        <label class="sr-only" for="loginemail">Email address</label>
                                        <input
                                          id="loginemail"
                                          placeholder="Email"
                                          ng-model="loginUser.email"
                                          class="form-control"
                                          type = "email"
                                          name = "email"
                                          invalid-message="'Δεν έχετε δώσει πραγματικό email'"
                                          required-message="'Το πεδίο είναι υποχρεωτικό'"
                                          validate-on="dirty"
                                          required style="width: 100%;"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="loginpass">Password</label>
                                        <input
                                          id="loginpass"
                                          placeholder="Password"
                                          ng-model="loginUser.pwd"
                                          class="form-control"
                                          type="password"
                                          name = "password"
                                          required-message="'Το πεδίο είναι υποχρεωτικό'"
                                          validate-on="dirty"
                                          required style="width: 100%;" />
                                    </div>
                                    <div class="forgot-wrap text-left">
                                        <a href="{{ url('user/forgot-password') }}" target="_self">Ξεχάσατε τον κωδικό σας;</a>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="1" name="remember" ng-model="loginUser.remember">
                                                <span>Να με θυμάσαι</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-lg btn-block" ng-disabled="loginForm.$invalid">ΣΥΝΔΕΣΗ</button>
                                    </div>
                                </form>
                                <span class="or">
                                    <span>ή</span>
                                </span>
                                <div class="row">
                                    @if ( session()->has('redirect') )
                                        <input type="hidden" name="redirect" type="redirect" value="{{session('redirect')}}" id="redirect">
                                        <a class="btn btn-facebook btn-block btn-lg" href="{{url('/user/facebooklogin'). '?' . http_build_query(['redirect'=>session('redirect')])}}">
                                        Συνδεθειτε με facebook
                                        </a>
                                    @else
                                        <a href="{{url('/user/facebooklogin')}}" class="btn btn-facebook btn-block btn-lg"> Συνδεθειτε με facebook </a>
                                    @endif
                                </div>
                            </div>
                        </uib-tab>

                    </uib-tabset>
                    @elseif ( session()->has('token') )
                        <p>Καλώς ήρθες {{ session('name') }} !</p>
                        <p>
                        Δείτε <a href="{{ url( session()->has('role') && session('role') == 'MOBUSER' ? 'user/books' : 'secrets/feed') }}" target="_self">μυστικά</a>.
                            <br>
                            Δείτε τα βιβλία που έχετε στην <a href="{{ url('user/library') }}" target="_self">βιβλιοθήκη</a> σας.
                            <br>
                            Δείτε τα <a href="{{ url('secrets/favorites') }}" target="_self">αγαπημένα</a> σας μυστικά.
                            <br>
                            <a href="{{ url('user/logout') }}" target="_self"><i class="fa fa-sign-out"></i> Αποσύνδεση</a>
                        </p>
                    @endif

                </div>
            </div>
        </div>
    </section>

    <section class="app-about section">

        <div class="container">
            <ul class="list-unstyled row">
                <li class="col-md-4 col-sm-12 col-xs-12">
                    @include('inc/svgs/users')
                    <h3 class="title">Αναγνώστες</h3>
                    <p>Τα βιβλία πάντα, έχουν περισσότερα να πουν. Ανακαλύψτε σχετικό περιεχόμενο από την booksecrets και τους ίδιους τους δημιουργούς τους. Το τέλος του βιβλίου, δεν είναι πια η τελευταία σελίδα.</p>
                    <a href="{{ url('visitors') }}" class="read-more" target="_self">περισσότερα</a>
                </li>
                <li class="col-md-4 col-sm-12 col-xs-12">
                    @include('inc/svgs/authors')
                    <h3 class="title">Συγγραφείς</h3>
                    <p>Ζωντανέψτε τα αντίτυπα των βιβλίων σας, σε κάθε σπίτι, βιβλιοθήκη ή βιβλιοπωλείο και εκπλήξτε ευχάριστα τους αναγνώστες σας με αποκλειστικό και επικαιροποιημένο σχετικό περιεχόμενο.</p>
                    <a href="{{ url('authors') }}" class="read-more" target="_self">περισσότερα</a>
                </li>
                <li class="col-md-4 col-sm-12 col-xs-12">
                    @include('inc/svgs/publishers')
                    <h3 class="title">Εκδότες</h3>
                    <p>Χρησιμοποιήστε την booksecrets για να είστε σίγουροι, πως οτιδήποτε σχετικό με τα βιβλία σας θέλετε να επικοινωνήσετε, θα είναι πάντα άμεσα διαθέσιμο, για κάθε ενδιαφερόμενο αναγνώστη.</p>
                    <a href="{{ url('publishers') }}" class="read-more" target="_self">περισσότερα</a>
                </li>
            </ul>

            <div class="row about-wrap" id="about_booksecrets">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    @include('inc/svgs/about')
                    <h3 class="title">Tι είναι η booksecrets</h3>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 inner-sec">
                    <p>Θα μπορούσαν άραγε όλα τα βιβλία να μεταφέρουν μαζί τους ό,τι πληροφορία θα έκανε την αναγνωσή τους απολαυστικότερη ή ακόμα και συναρπαστικότερη; Θα μπορούσε αυτή η πληροφορία, όχι μόνο να μην απαιτεί από εμάς να την αναζητήσουμε, αλλά να είναι άμεσα διαθέσιμη και να έχουμε την δυνατότητα να ενημερωνόμαστε για κάθε τι νέο;</p>
                    <p>Από την άλλη μεριά, εκδότες, συγγραφείς και αναγνώστες δημιουργούν συνεχώς σχετικό περιεχόμενο ακόμα και για το πιο δυσεύρετο βιβλίο. Αυτό το περιεχόμενο δεν ακολουθεί τον τίτλο για τον οποίο δημιουργήθηκε, αλλά είναι σκορπισμένο σε εφήμερες αναρτήσεις στα κοινωνικά δίκτυα, σε blogs και ιστοσελίδες, με αποτέλεσμα να μην φτάνει σε κάθε ενδιαφερόμενο αναγνώστη.</p>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 inner-sec">
                    <p>Για τον λόγο αυτό δημιουργήσαμε την booksecrets. Πλέον τα αγαπημένα μας βιβλία, μπορούν να μας καλούν όταν έχουν κάτι να μας πουν, ενώ μπορούμε να ανακαλύψουμε το επόμενο βιβλίο μας, μέσω των μυστικών που μεταφέρει.</p>
                    <p>Τι είδους μυστικό μπορεί να είναι αυτό; Μια αφήγηση, επικαιροποιημένες σκέψεις του δημιουργού, μουσικές επιλογές, ένα επιπλέον κεφάλαιο, διορθώσεις, ιστορίες σχετικές με το θέμα ή με τους χαρακτήρες, την πηγή της έμπνευσης, διαδραστικές σελίδες, ηχητικά εφέ, στοχευμένες βιβλιοφιλικές προσφορές και ποιός ξέρει τί άλλο!</p>
                </div>
            </div>
        </div>

    </section>
    
    <section class="app-about section back" ng-controller="LogoController">

        <div class="container-fluid">
            <div class="row about-wrap" id="interviews">
                <div class="col-md-12 col-sm-12 col-xs-12 noborder">
                    <!--@include('inc/svgs/about')-->
                    <h3 class="title">Είπαν για εμάς</h3>
                </div>
                <div class="col-xs-12 ">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 padding">
                            <div class="row flexme">
                                <div class=" paddingtop30 text-center mywidth grow_money pad auto-m padding-bottom">
                                    <a href="http://www.newmoney.gr/oles-oi-eidiseis/288871-fanis-xaralampous-nikos-argiris-booksecrets-i-epomeni-mera-gia-to-biblio"><img class="height img-responsive" src="{{ asset('assets/img/interviews/newmoney.png') }}"></img></a>    
                                </div>
                                <div class="paddingtop30 text-center mywidth grow pad padtopHm auto-m padding-bottom">
                                    <a href="http://www.imerisia.gr/article.asp?catid=30980&subid=2&pubid=114084098"><img class="height img-responsive" src="{{ asset('assets/img/interviews/hmerisia.png') }}"></img></a>     
                                </div>
                                <div class="paddingtop30 text-center mywidth pad auto-m padding-bottom">
                                    <a href="https://vimeo.com/171915387"><img class="height img-responsive" src="{{ asset('assets/img/interviews/novalife.png') }}"></img></a>     
                                </div>
                                <div class="paddingtop30 text-center mywidth grow_first pad auto-m padding-bottom">
                                    <a href="" ng-click="openmodal()"><img class="height img-responsive" src="{{ asset('assets/img/interviews/first.png') }}"></img></a>     
                                </div>
                                <div class="paddingtop30 text-center mywidth grow_lifo pad padtopLifo auto-m padding-bottom">
                                    <a href="http://www.lifo.gr/articles/book_articles/106175"><img class="height img-responsive" src="{{ asset('assets/img/interviews/lifo.png') }}"></img></a>    
                                </div>
                                <div class="paddingtop30 text-center mywidth grow_popa pad auto-m padding-bottom">
                                    <a href="http://popaganda.gr/booksecrets-app/"><img class="height img-responsive" src="{{ asset('assets/img/interviews/popaganda.png') }}"></img></a>
                                </div>
                                <div class="paddingtop30 text-center mywidth grow_toc pad auto-m padding-bottom">
                                    <a href="http://www.thetoc.gr/politismos/article/booksecretsi-elliniki-kainotoma-efarmogi-pou-diabazei-ta-biblia-alliws"><img class="height img-responsive" src="{{ asset('assets/img/interviews/toc.png') }}"></img></a>     
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>


	@include('inc/footer')

</body>
</html>
