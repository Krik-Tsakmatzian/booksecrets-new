<!doctype html>
<html lang="el">
<head>
	<title>Oops! Page not found!</title>
	<meta charset="UTF-8">

	@include('inc/head')

</head>

    <body ng-app="myApp">
    
        <!--[if lt IE 7]>
          <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <section class="error-page">
            
            <div class="container">
                
                <div class="error-wrap">
                    <div class="row">
                        <h1 class="col-md-4 col-sm-12 col-xs-12">404</h1>
                        <h4 class="col-md-6 col-sm-12 col-xs-12">OOPS! <br> Η Σελίδα δεν βρέθηκε</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <a href="{{ url('/') }}" class="btn btn-empty btn-lg">ΕΠΙΣΤΡΟΦΗ ΣΤΟ WEBSITE</a>
                        </div>
                    </div>
                </div>
            
            </div>
            
        </section>
    
    </body>
</html>
