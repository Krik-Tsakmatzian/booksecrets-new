@if (!session()->has('token') )
  <header id="header" ng-controller="mainController">
      <nav class="navbar navbar-default logged-out">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <a class="navbar-brand" href="{{ url('/') }}" target="_self"><img src="{{ asset('dist/img/logo.png') }}" height="50"></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div id="sign_menu">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="{{ url('user/login') }}" target="_self">Σύνδεση</a></li>
              <li>/</li>
              <li><a href="{{ url('/#sign-up') }}" target="_self">Εγγραφή</a></li>
            </ul>
          </div><!-- /.navbar-collapse -->

        </div><!-- /.container -->
     </nav>

  </header>
@else
  <header id="header">
      <nav class="navbar navbar-default logged">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <a class="navbar-brand" href="{{ url('/') }}" target="_self"><img src="{{ asset('dist/img/logo.png') }}" height="50"></a>
          </div>

          <ul class="nav navbar-nav navbar-right">
            <!--<li><a href="#">en</a></li>-->
            <!--<li>/</li>-->
            <!--<li><a href="#">ελ</a></li>-->      
            @if ( session()->has('name') && session('name') != '' )
              <li>{{ session('name') }}</li>             
            @endif

            <li class="cog" uib-dropdown is-open="status.isopen">
              <button class="btn btn-empty" id="simple-dropdown" uib-dropdown-toggle><i class="fa fa-cog"></i></button>
                <ul uib-dropdown-menu role="menu" aria-labelledby="single-button" class="dropdown">     
                  <ul class="mobile-view">             
                    @include('inc/menu')
                  </ul>
                  @if ( session()->has('id') )
                    
                    @if ( session('role') == 'AUTHOR')
                      <li class="{{{ url()->current() == 'author/'.session('id') ? 'active' : '' }}}">
                          <a href="author/{{session('id')}}" target="_self"><i class="fa fa-user"></i> Το προφίλ μου </a>
                      </li>
                    @elseif (session('role') == 'MOBUSER' )
                      <li class="{{{ url()->current() == 'user-info/'.session('id') ? 'active' : '' }}}">
                        <a href="user/user-info/{{session('id')}}" target="_self"><i class="fa fa-user"></i> Το προφίλ μου</a>
                      </li>
                    @else
                      <li class="{{{ url()->current() == 'publisher/'.session('id') ? 'active' : '' }}}">
                          <a href="publisher/{{session('id')}}" target="_self"><i class="fa fa-user"></i> Το προφίλ μου</a>
                      </li>
                    @endif
                  @endif
                  <li role="menuitem"><a href="{{ url('user/logout') }}" target="_self"><i class="fa fa-sign-out"></i> Αποσύνδεση</a></li>
                </ul>
            </li>
          </ul>

        </div><!-- /.container -->
     </nav>

  </header>
@endif
