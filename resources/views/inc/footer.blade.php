<footer id="footer" ng-controller="FooterController" sticky-footer="#wrapper">

    <div class="container">
        <div class="row footer-wrap">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <img src="{{ asset('dist/img/footerlogo.png') }}" alt="Bookesecrets footer logo">
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4">
                <ul class="list-unstyled padding015">
                    <li><a href="https://www.nbg.gr/el/nbgseeds/competition" target="_blank"><img src="{{ asset('dist/img/award.png') }}" height="100"></a></li>
                    <li>
                        <a href="http://orangegrove.biz/" target="_blank"><img src="{{ asset('dist/img/orangegrove.png') }}"></a>
                        <a href="http://www.osdel.gr/" target="_blank"><img src="{{ asset('dist/img/osdelnet.png') }}"></a>
                    </li>
                    <!--<li><a href="http://www.osdel.gr/" target="_blank"><img src="{{ asset('dist/img/osdelnet.png') }}"></a></li>-->
                </ul>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4">
                <ul class="list-unstyled">
                    <li><a href="{{ url('/who-we-are') }}" target="_self">Ποιοί είμαστε</a></li>
                    <li><a href="{{ url('/#about_booksecrets') }}" target="_self">Τι είναι η booksecrets</a></li>
                    <li><a href="" ng-click="open()">Επικοινωνία</a></li>
                    <li><a href="http://blog.booksecrets.com" target="_self">Blog</a></li>
                    <li><a href="{{ url('/who-we-are-en') }}" target="_self">Who we are</a></li>
                </ul>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4">
                <p>Βρείτε μας</p>
                <ul class="list-unstyled social-wrap">
                    <li><a href="https://www.facebook.com/booksecretscom/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/booksecretscom" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://www.linkedin.com/company/5397319" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="https://www.instagram.com/booksecrets/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <small>© 2018 booksecrets | <a class="terms" href="{{ url('terms') }}" target="_self">Όροι χρήσης</a></small>
            </div>
        </div>

    </div>

</footer>
