@if ( session()->has('role') && session('role') == 'MOBUSER' )
    <li class="{{{ Request::path() == 'user/books' ? 'active' : '' }}}">
        <a href="/user/books" target="_self"><i class="fa fa-book"></i> Βιβλία</a>
    </li>
@endif
<li class="{{{ session()->has('role') && session('role') == 'MOBUSER' ? 'book-menu' : '' }}} {{{ Request::path() == 'secrets/feed' ? 'active' : '' }}}">    
    <a href="{{ url('secrets/feed') }}" target="_self"><i class="icon-feed"></i> Ροή μυστικών</a>
</li>
<li class="{{{ Request::path() == 'secrets/all' ? 'active' : '' }}}">
    <a href="/secrets/all" target="_self"><i class="fa fa-lock"></i> Τα μυστικά μου</a>
</li>
@if ( session()->has('role') && session('role') != 'MOBUSER' )
    <li class="{{{ Request::path() == 'secrets/add' ? 'active' : '' }}}">
        <a href="/secrets/add" target="_self" class="add-new"><i class="fa fa-pencil-square"></i> Προσθήκη</a>
    </li>
@endif
<li class="{{{ Request::path() == 'user/library' ? 'active' : '' }}}">
    <a href="/user/library" target="_self"><i class="icon-library"></i> Η Βιβλιοθήκη μου</a>
</li>
<li class="{{{ Request::path() == 'secrets/favorites' ? 'active' : '' }}}">
    <a href="/secrets/favorites" target="_self"><i class="icon-fav"></i> Αγαπημένα Μυστικά</a>
</li>

@if ( session()->has('role') && session('role') != 'MOBUSER' )      
    <li class="{{{ Request::path() == 'user/statistics' ? 'active' : '' }}}">
        <a href="/user/statistics" target="_self"><i class="icon-stats"></i> Στατιστικά</a>
    </li> 
@endif