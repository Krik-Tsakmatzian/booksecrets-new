<form id="advForm" angular-validator-submit="registerAdv('author')" name="registerAuthorForm" class="row signupForm" novalidate angular-validator>
    @if ( session()->has('result') )
        <input 
            type="hidden" 
            ng-model="result" 
            id="fbIn" />
    @endif
    
    <div id="step1" ng-show="stepAuthor == 1">
        <input 
            class="form-control" 
            type="text" 
            name="name"
            ng-model="author.name" 
            required-message="'Το πεδίο είναι υποχρεωτικό'"
            invalid-message="'θα πρέπει να δώσετε ένα username'"
            validate-on="dirty" 
            placeholder="Username"
            required />
        <input 
            class="form-control"
            type="email"
            name="email"
            ng-model="author.email" 
            required-message="'Το πεδίο είναι υποχρεωτικό'"
            invalid-message="'Δεν έχετε δώσει πραγματικό email'"
            validate-on="dirty"
            placeholder="Email"
            required/>
        <input 
            class="form-control" 
            type="password" 
            name="password"
            ng-model="author.pwd" 
            validator = "passwordValidator(author.pwd) === true"
            required-message="'Το πεδίο είναι υποχρεωτικό'"
            invalid-message = "passwordValidator(author.pwd)"
            validate-on="dirty"
            placeholder="Password"
            required />  
        <button 
            type="button" 
            class="btn btn-primary btn-lg btn-block" 
            ng-click="nextStep('author')" 
            ng-disabled="stepValidate('1', 'author')">Συνεχεια</button>
        
        <!--<span class="or">-->
        <!--    <span>or</span>-->
        <!--</span>-->
        <!--<div>-->
        <!--    <a href="{{url('/user/facebooklogin-author')}}" class="btn btn-facebook btn-block btn-lg"> Connect with facebook </a>-->
        <!--</div>  -->
    </div>
    
    <div id="step2" ng-show="stepAuthor == 2">
        <input 
            class="form-control" 
            type="text" 
            name="first_name"
            ng-model="author.first_name" 
            required-message="'Το πεδίο είναι υποχρεωτικό'"
            invalid-message="'Θα πρέπει να δώσετε το ονομά σας'"
            validate-on="dirty" 
            placeholder="First name"
            required />
        <input 
            class="form-control" 
            type="text" 
            name="last_name"
            ng-model="author.last_name" 
            required-message="'Το πεδίο είναι υποχρεωτικό'"
            invalid-message="'θα πρέπει να δώσετε το επιθετό σας'"
            validate-on="dirty" 
            placeholder="Last name"
            required />
         <button 
            type="button" 
            class="btn btn-primary btn-lg btn-block" 
            ng-click="nextStep('author')" 
            ng-disabled="stepValidate('2', 'author')">Συνεχεια</button>
        <button type="button" class="btn btn-empty margin-top-10 btn-back" ng-click="prevStep('author')">Επιστροφή</button>
    </div>
    
    <div id="step3" ng-show="stepAuthor == 3 || stepAuthor == 4">
        <div class="search-wrap">
            <input 
                class="form-control" 
                type="text" 
                name="search"
                ng-model="author.search"
                placeholder="Τίτλος ή ISBN οποιουδήποτε βιβλίου σας"/>
            <button 
                type="button" 
                class="btn btn-empty btn-search" 
                ng-click="bookSearch('author')"
                ng-disabled="stepValidate('3', 'author')"><i class="fa fa-arrow-circle-right"></i></button>
        </div>
        
        <div class="clearfix"></div>
        
        <div class="loader" ng-if="loading"><div class="loader-inner ball-clip-rotate"><div></div></div></div>
        
        <div id="step3b" ng-show="stepAuthor == 4">
            
            <div class="results" ng-show="resultBooklist">
                <h5>Επιλέξτε βιβλίο.</h5>
                <ul class="list-unstyled row books-row">
                    <li class="col-md-4" 
                        ng-repeat="book in resultBooklist" 
                        ng-click="selectBook(book.authors[0].id, $index, 'author')" 
                        ng-class="{ 'selected': $index == selectedIndex }"
                        uib-tooltip="<% book.authors[0].name %>">
                        <div>
                            <img ng-src="{{ config('common.base_url') }}<% book.cover %>" err-SRC="http://dummyimage.com/100x150/858585/fff">
                        </div>
                    </li>
                </ul>
                
                <input 
                    type="hidden" 
                    name="bookID" 
                    value="" 
                    ng-model="author.bookId" 
                    id="bookAuthorID" 
                    required>
                
                <div class="checkbox text-left">
                    <label>
                        <input type="checkbox" name="bookSelect" value="0" ng-click="openTerms('Author', $event)" required ng-model="author.bookAccept">
                        <span>Είμαι ο συγγραφέας του επιλεγμένου βιβλίου</span>
                    </label>
                </div>
                
                <small class="pull-left">Παρακαλούμε διαβάστε και συμφωνείστε με τους <a href="{{ url('terms') }}" target="_selft">όρους χρήσης</a>.</small>
                <div class="clearfix"></div>
                <button type="submit" class="btn btn-primary btn-lg btn-block" ng-disabled="registerAuthorForm.$invalid">Συμφωνω</button>
            </div>
            <div class="results-error" ng-if="error">
                Δεν βρέθηκαν βιβλία για αυτή την αναζήτηση.
            </div>
            
        </div>
        
        <button type="button" class="btn btn-empty margin-top-10 btn-back" ng-click="prevStep('author')">Επιστροφή</button>
    </div>
    
    
</form>
