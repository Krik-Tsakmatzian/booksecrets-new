<form id="advForm" angular-validator-submit="registerAdv('publisher')" name="registerPublisherForm" class="row signupForm" novalidate angular-validator>
    <div id="stepPub1" ng-show="stepPublisher == 1">
        <input 
            class="form-control" 
            type="text" 
            name="username"
            ng-model="publisher.username" 
            required-message="'Το πεδίο είναι υποχρεωτικό'"
            invalid-message="'θα πρέπει να δώσετε ένα username'"
            validate-on="dirty" 
            placeholder="Username"
            required />
        <input 
            class="form-control"
            type="email"
            name="emailPub"
            ng-model="publisher.email" 
            required-message="'Το πεδίο είναι υποχρεωτικό'"
            invalid-message="'Δεν έχετε δώσει πραγματικό email'"
            validate-on="dirty"
            placeholder="Email"
            required/>
        <input 
            class="form-control" 
            type="password" 
            name="password"
            ng-model="publisher.pwd" 
            validator = "passwordValidator(publisher.pwd) === true"
            required-message="'Το πεδίο είναι υποχρεωτικό'"
            invalid-message = "passwordValidator(publisher.pwd)"
            validate-on="dirty"
            placeholder="Password"
            required />  
        <button 
            type="button" 
            class="btn btn-primary btn-lg btn-block" 
            ng-click="nextStep('publisher')" 
            ng-disabled="stepValidate('1', 'publisher')">Συνεχεια</button>
    </div>
    
    <div id="stepPub2" ng-show="stepPublisher == 2">
        <input 
            class="form-control" 
            type="text" 
            name="name"
            ng-model="publisher.name" 
            required-message="'Το πεδίο είναι υποχρεωτικό'"
            invalid-message="'Θα πρέπει να δώσετε ένα ονομά'"
            validate-on="dirty" 
            placeholder="Publisher name"
            required />
        <input 
            class="form-control" 
            type="text" 
            name="contactPerson"
            ng-model="publisher.contactPerson" 
            required-message="'Το πεδίο είναι υποχρεωτικό'"
            invalid-message="'Θα πρέπει να δώσετε το ονομά υπευθύνου'"
            validate-on="dirty" 
            placeholder="Person in charge"
            required />
         <button 
            type="button" 
            class="btn btn-primary btn-lg btn-block" 
            ng-click="nextStep('publisher')" 
            ng-disabled="stepValidate('2', 'publisher')">Συνεχεια</button>
        <button type="button" class="btn btn-empty margin-top-10 btn-back" ng-click="prevStep('publisher')">Επιστροφή</button>
    </div>
    
    <div id="stepPub3" ng-show="stepPublisher == 3 || stepPublisher == 4">
        <div class="search-wrap">
            <input 
                class="form-control" 
                type="text" 
                name="search"
                ng-model="publisher.search"
                placeholder="Τίτλος ή ISBN οποιουδήποτε βιβλίου σας"/>
            <button 
                type="button" 
                class="btn btn-empty btn-search" 
                ng-click="bookSearch('publisher')"
                ng-disabled="stepValidate('3', 'publisher')"><i class="fa fa-arrow-circle-right"></i></button>
        </div>
        
        <div class="clearfix"></div>
        
        <div class="loader" ng-if="loadingPub"><div class="loader-inner ball-clip-rotate"><div></div></div></div>
        
        <div id="stepPub3b" ng-show="stepPublisher == 4">
            
            <div class="results" ng-show="resultBooklistPub">
                <h5>Επιλέξτε βιβλίο.</h5>
                <ul class="list-unstyled row books-row">
                    <li class="col-md-4" 
                        ng-repeat="book in resultBooklistPub" 
                        ng-click="selectBook(book.publishers[0].id, $index, 'publisher')" 
                        ng-class="{ 'selected': $index == selectedIndexPub }"
                        uib-tooltip="<% book.publishers[0].name %>">
                        <div>
                            <img ng-src="{{ config('common.base_url') }}<% book.cover %>" err-SRC="http://dummyimage.com/100x150/858585/fff">
                        </div>
                    </li>
                </ul>
                
                <input 
                    type="hidden" 
                    name="bookID" 
                    value="" 
                    ng-model="publisher.bookId" 
                    id="bookPublisherID" 
                    required>
                
                <div class="checkbox text-left">
                    <label>
                        <input type="checkbox" name="bookSelect" value="0" required ng-model="publisher.bookAccept" ng-click="openTerms('Publisher', $event)">
                        <span>Είμαι ο εκδότης του επιλεγμένου βιβλίου</span>
                    </label>
                </div>
                
                <small class="pull-left">Παρακαλούμε διαβάστε και συμφωνείστε με τους <a href="{{ url('terms') }}" target="_selft">όρους χρήσης</a>.</small>
                <div class="clearfix"></div>
                <button type="submit" class="btn btn-primary btn-lg btn-block" ng-disabled="registerPublisherForm.$invalid">Συμφωνω</button>
            </div>
            <div class="results-error" ng-if="errorPub">
                Δεν βρέθηκαν βιβλία για αυτή την αναζήτηση.
            </div>
            
        </div>
        
        <button type="button" class="btn btn-empty margin-top-10 btn-back" ng-click="prevStep('publisher')">Επιστροφή</button>
    </div>
    
    
</form>
