<form angular-validator-submit="register()" name="registerForm" class="row signupForm" novalidate angular-validator>
    <input 
        ng-model="credentials.name" 
        class="form-control" 
        type="text" 
        name="name"
        required-message="'Το πεδίο είναι υποχρεωτικό'"
        invalid-message="'θα πρέπει να δώσετε ένα username'"
        validate-on="dirty" 
        placeholder="Username"
        required />
    <input 
        ng-model="credentials.email" 
        class="form-control"
        type = "email"
        name = "email"
        invalid-message="'Δεν έχετε δώσει πραγματικό email'"
        required-message="'Το πεδίο είναι υποχρεωτικό'"
        validate-on="dirty"
        placeholder="Email"
        required/>
    <input 
        ng-model="credentials.pwd" 
        class="form-control" 
        type="password" 
        name = "password"
        validator = "passwordValidator(credentials.pwd) === true"
        required-message="'Το πεδίο είναι υποχρεωτικό'"
        invalid-message = "passwordValidator(credentials.pwd)"
        validate-on="dirty"
        placeholder="Password"
        required />    
    <button type="submit" class="btn btn-primary btn-lg btn-block" ng-disabled="registerForm.$invalid">ΕΓΓΡΑΦΗ</button>
</form>
<span class="or">
    <span>ή</span>
</span>
<div class="row">
    @if ( session()->has('redirect') )
        <input type="hidden" name="redirect" type="redirect" value="{{session('redirect')}}" id="redirect">
        <a class="btn btn-facebook btn-block btn-lg" href="{{url('/user/facebooklogin'). '?' . http_build_query(['redirect'=>session('redirect')])}}">
        Συνδεθειτε με facebook
        </a>
    @else
        <a href="{{url('/user/facebooklogin')}}" class="btn btn-facebook btn-block btn-lg"> Συνδεθειτε με facebook </a>
    @endif 
</div>  