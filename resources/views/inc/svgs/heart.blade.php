<svg class="heart svg-icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 150 150" style="enable-background:new 0 0 150 150;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#282828;}
	.st1{fill:#00AEEF;}
</style>
<g>
	<path class="st0" d="M74.7,115.1c-1.4,0-6.1-1.2-19.9-12.6c-6.9-5.7-16.1-14.2-19.7-19.6c-3.8-5.8-5.4-11.2-5.1-17.3
		c0.3-6.1,2.9-11.9,7.3-16.3c4.5-4.4,10.3-6.8,16.4-6.8c9.7,0,16.4,7.7,19.6,11.3l0.3,0.5c0.3,0.3,0.6,0.6,1,0.6c0,0,0,0,0,0
		c0.4,0,0.8-0.3,1-0.6c0.2-0.2,0.5-0.6,0.7-0.8c3.5-3.9,10.1-11.1,19.2-11.1c6.1,0,11.9,2.4,16.4,6.8c4.4,4.3,7,10.1,7.3,16.2
		c0.4,7.9-2.5,13.4-5.1,17.3c-3.6,5.4-12.8,13.9-19.6,19.6C80.9,113.9,76.1,115.1,74.7,115.1z M53.7,45.3
		c-10.9,0-20.3,9.2-20.9,20.4c-0.3,5.5,1.1,10.3,4.6,15.7c2.7,4.1,10.3,11.6,18.8,18.7c9.8,8.1,16.2,12,18.3,12.2c0,0,0.1,0,0.1,0
		c0,0,0.1,0,0.1,0c2-0.2,8.5-4,18.2-12.1c8.5-7.1,16.1-14.7,18.8-18.8c3.6-5.4,4.9-10.1,4.6-15.7c-0.6-11.3-10-20.4-20.9-20.4
		c-7.8,0-13.7,6.3-17.1,10.1c-1.9,2.1-2.7,2.9-3.8,2.9c-1,0-1.6-0.7-3.4-2.7C68.1,52.1,62.1,45.3,53.7,45.3z"/>
	<g>
		<path class="st0" d="M58.9,36.9c-5.9-2-12.1-1.1-12.3-1.1c-0.8,0.1-1.6,0.8-1.6,1.6v29.1c0,0.7,0.6,1.3,1.2,1.5
			c0.6,0.3,1.4,0.1,1.9-0.4l4.3-4.2l4.2,4.2c0.3,0.3,0.7,0.5,1.2,0.5c0.2,0,0.4,0,0.6-0.1c0.6-0.3,1-0.8,1-1.5L60,38.4
			C60,37.7,59.6,37.1,58.9,36.9z"/>
		<path class="st1" d="M52.1,60.5l-4.7,4.6l-0.2-26.8c0.7-0.1,2.2-0.3,4.1-0.3c2.2,0,4.3,0.3,6.1,0.9l-0.4,26.3L52.1,60.5z"/>
	</g>
</g>
</svg>