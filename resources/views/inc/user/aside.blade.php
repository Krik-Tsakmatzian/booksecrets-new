<aside class="sidebar">
    
    @if ( !isset($hideNav) || $hideNav == false )
    <ul class="list-unstyled side-nav">
        <li class="{{{ url()->current() == 'visitors' ? 'active' : '' }}}"><a href="{{ url('visitors') }}">@include('inc/svgs/users')</a></li>
        <li class="{{{ url()->current() == 'authors' ? 'active' : '' }}}"><a href="{{ url('authors') }}">@include('inc/svgs/authors')</a></li>
        <li class="{{{ url()->current() == 'publishers' ? 'active' : '' }}}"><a href="{{ url('publishers') }}">@include('inc/svgs/publishers')</a></li>
    </ul>
    @endif
    
    <a href="{{{ !url()->previous() ? url('/') : url()->previous() }}}" class="btn btn-blue-border"><i class="fa fa-angle-left"></i>@if (url()->current() == 'who-we-are-en') Back @else Επιστροφη @endif</a>
</aside>