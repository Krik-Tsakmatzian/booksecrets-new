@if ( isset($name) )
	<meta property="og:title" content="{{ $name }}" />
@else
	<meta property="og:title" content="Booksecrets" />
@endif

@if ( isset($meta_desc) )
	<meta property="og:description" content="{{ $meta_desc }}" />
	<meta property="og:type" content="article" />
@else
	<meta property="og:description" content="Η booksecrets, μέσω των βιβλίων που όλοι έχουμε σπίτι μας, δημιουργεί ένα νέο κανάλι επικοινωνίας μεταξύ δημιουργού και αναγνώστη. Με τη δωρεάν εφαρμογή μας, μπορείτε να αποκαλύπτετε μυστικό περιεχόμενο για τα αγαπημένα σας βιβλία." />
	<meta property="og:type" content="website" />
@endif

@if ( isset($meta_img) )
	<meta property="og:image" content="{{ $meta_img }}" />
@else
	<meta property="og:image" content="{{ asset('dist/img/share.jpg') }}" />
@endif

<meta property="fb:app_id" content="1466663296989400">
<meta property="og:url" content="{{ url()->current() }}" />

<meta name="viewport" content="width=device-width" />

<base href="/">

<!-- CSS -->
<link rel="shortcut icon" href="{{{ asset('dist/img/favicon.ico') }}}">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300italic,300,400italic,700,700italic&subset=latin,greek' rel='stylesheet' type='text/css'>
<!--<link href='https://fonts.googleapis.com/css?family=Fira+Mono&subset=latin,greek' rel='stylesheet' type='text/css'>-->
<link href='https://fonts.googleapis.com/css?family=Fira+Sans&subset=latin,greek' rel='stylesheet' type='text/css'>

<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/vendor.min.css') }}">

@if ( session()->has('role'))
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/advanced.min.css') }}">
@endif

<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/icons.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/style.min.css') }}">
<link rel="stylesheet" href="//assets-cdn.ziggeo.com/v1-stable/ziggeo.css" />
<script src="//assets-cdn.ziggeo.com/v1-stable/ziggeo.js"></script>
<script>
	ZiggeoApi.token = "e62b54d95b0e3d18e64ee82b88f9ca2f";
	var video = "";
	var imgFlag = false;
	var videoFlag = false;
	var audioFlag = false;
</script>
@if (request()->path() == '/' || request()->path() == 'who-we-are-en')
	<script src="https://f.vimeocdn.com/js/froogaloop2.min.js"></script>
@endif

<script type="text/javascript" src="{{ asset('dist/js/vendor.min.js') }}"></script>

@if ( session()->has('role') )
	<script type="text/javascript" src="{{ asset('assets/redactor/redactor.js') }}"></script>
	<script type="text/javascript" src="{{ asset('dist/js/advanced.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/redactor/plugins/imagecropper.js') }}"></script>
@else
	<script type="text/javascript" src="{{ asset('assets/redactor/redactor.min.js') }}"></script>
@endif


@if (env('ENV') == 'DEV')
	<script type="text/javascript" src="{{ asset('dist/js/appDev.js') }}"></script>
@else
	<script type="text/javascript" src="{{ asset('dist/js/app.js') }}"></script>
@endif

{{-- <script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
	{ (i[r].q=i[r].q||[]).push(arguments)}
	,i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-74182126-1', 'auto');
	ga('send', 'pageview');
</script> --}}