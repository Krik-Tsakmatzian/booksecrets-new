@extends('user.wrap')

@section('userChild')

<h2>Το προφίλ μου</h2>
<section id="wrapper" class="main-wrapper" ng-controller="AuthorController">

    <div class="row main-wrap profile-wrap" ng-init="id = {{ $id }}">
        
        <div class="col-md-4 col-sm-4 col-xs-12">

            <div class="img-wrap">
                <div>
                    <img class="img-responsive" width="200" height="200" id="avatar" ng-src="<% profileImage %>" err-SRC="<% profileImage %>">
                </div>
                    
                <button type="file"
                    class="btn btn-empty btn-block"
                    ngf-select="uploadFiles($file, $invalidFiles)" 
                    accept="image/*" 
                    ngf-max-height="1000"
                    ngf-max-size="2MB">
                    <i class="fa fa-camera"></i>
                    <span>Αλλαγή Avatar</span>
                </button>
            </div>
            <h5 class="center" style="color: grey; font-size:11px;">* Μέγιστο επιτρεπτό μέγεθος 2ΜΒ</h5>
            <h1 class="title center"><% name %></h1>
            
        </div>
        
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="text-preview slide-right">
                
                <div class="text-summary" ng-show="!edit">                   
                    <p> 
                        <span ng-if="user.summary != '' || user.summary != null"><% user.summary %></span>
                        <span ng-if="user.summary == '' || user.summary == null">Γράψτε μια περιγραφή για τον εαυτό σας...</span>
                        <button style="padding: 4px 6px;" class="btn btn-empty" ng-click="edit = true"><i class="icon-pencil"></i></button>
                    </p>                                                
                </div>
                
                <div class="text-edit slide-right" ng-show="edit">
                    <form novalidate>
                        <textarea class="form-control" rows="14" ng-model="user.summary"><% user.summary %></textarea>
                        
                        <div class="actions">
                            <button type="submit" class="btn btn-empty" ng-click="update(user.summary)"><i class="fa fa-floppy-o"></i></button>
                        </div> 
                    </form>
                </div>
            </div>
            
            <div class="social-preview slide-right">
                <ul class="list-unstyled list-inline" ng-show="!editSocial">
                    <li ng-if="user.facebook && user.facebook != ''"><a href="<% user.facebook %>" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a></li>
                    <li ng-if="user.twitter && user.twitter != ''"><a href="<% user.twitter %>" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>
                    <li ng-if="user.website && user.website != ''"><a href="<% user.website %>" target="_blank"><i class="fa fa-globe"></i></a></li>
                    <li ng-if="user.youtube && user.youtube != ''"><a href="<% user.youtube %>" target="_blank" class="youtube"><i class="fa fa-youtube"></i></a></li>
                    <li ng-if="!user.youtube && !user.facebook && !user.twitter && !user.website"><span>Συμπληρώστε τα social media σας</span></li>
                    <li><button class="btn btn-empty fading" ng-click="editSocial = true" ng-hide="editSocial"><i class="icon-pencil"></i></button></li>
                </ul>
                
                <div class="social-edit slide-right" ng-show="editSocial">
                    <form novalidate>
                        <input id="facebook" placeholder="Facebook" ng-model="user.facebook" class="form-control" type = "text" name = "facebook" />
                        <input id="twitter" placeholder="Twitter" ng-model="user.twitter" class="form-control" type = "text" name = "twitter" />
                        <input id="website" placeholder="Website" ng-model="user.website" class="form-control" type = "text" name = "website" />
                        <input id="youtube" placeholder="Youtube" ng-model="user.youtube" class="form-control" type = "text" name = "youtube" />
                        <div class="actions">
                            <button type="submit" class="btn btn-empty fading" ng-click="updateSocial()"><i class="fa fa-floppy-o"></i></button>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>
        
        @if ( isset($secrets) && count($secrets)>0 )
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h3 class="title"><b>Μυστικά</b> - Τα τελευταία {{ count($secrets) }} </h3>                    
                <div class="books-carousel profile-carousel base-carousel">
                    @foreach ( $secrets as $sec )
                        <div class="item">
                            @if ( file_get_contents(config('common.base_url').$sec->book->cover) )
                                <img width='200' height='200' src="{{ config('common.base_url') }}{{ $sec->book->cover }}">
                            @else
                                <img width='200' height='200' src="{{ asset('dist/img/bookimage.jpg') }}">
                            @endif
                            <span onclick="location.href = '{{ url('secrets') }}/{{ $sec->book->id }}/{{ $sec->id }}';" class="caption">{{ $sec->title }}</span>
                        </div>
                    @endforeach
                </div>
            
            </div>
        @endif
        
    </div>
    
    

</section>

@endsection
