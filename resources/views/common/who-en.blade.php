@extends('layout.master')

@section('title') Ποιοί είμαστε | booksecrets @stop 

@section('content')

<section id="user-wrapper" ng-controller="mainController">
    
    <div class="container">
        
        @include('inc/user/aside', array('hideNav' => true))
        
        <main class="main">
            <h1 class="title">Who we are</h1>  
            <p><img src="{{ asset('dist/img/who_we_are.jpg') }}" class="img-responsive" style="margin: 0 auto;"></p>
            <p>Booksecrets is a platform that brings a whole new book experience by connecting authors and publishers with their book owners, in a direct and interactive way.</p>
            <p>The platform and free smartphone app are currently available in Greece and Cyprus, and refer to all greek books in print.</p>
            <p>We are currently working towards expanding our service to more countries. Please feel free to contact us at info@booksecrets.com</p>
            <div class="video-wrap" ng-init="playerwho=false">
                <div class="preview who">
                    <button class="btn btn-empty btn-play" ng-hide="playerwho" ng-click="playwho()"><i class="fa fa-play"></i></button>
                    <iframe id="player2" ng-show="playerwho" src="http://player.vimeo.com/video/142898784?api=1&player_id=player2" width="500" height="280" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="width: 100%;"></iframe>
                </div>
            </div>
        </main>
        
    </div>
    
</section>

@include('inc/footer')

	
@stop
