@extends('layout.master')

@section('title') Authors | booksecrets @stop 

@section('content')

<section id="user-wrapper">
    
    <div class="container">
        
        @include('inc/user/aside')
        
        <main class="main">
            <h1 class="title">Για συγγραφείς</h1>            
            <p>Η booksecrets σας δίνει την δυνατότητα να συνεχίζετε να μεταφέρετε συναισθήματα μέσα απο τα ίδια τα αντίτυπα των βιβλίων σας. Προσθέστε οτιδήποτε πιστεύετε πως στηρίζει το βιβλίο σας, με την σιγουριά ότι το περιεχόμενο αυτό θα συνεχίσει να ακολουθεί το βιβλίο σας.</p>
            <p><img src="{{ asset('dist/img/author_content_creation.jpg') }}" class="img-responsive"></p>
            <p>Αυτόματη παρουσίαση των βιβλίων σας μέσω της βιβλιογραφικής βάσης του ΟΣΔΕΛ. </p>
            <p>Μοντέρνο και φιλικό προς τον χρήστη περιβάλλον δημιουργίας περιεχομένου με δυνατότητα δημιουργίας αποκλειστικού περιεχομένου (video και audio).</p>
            <p><img src="{{ asset('dist/img/author_secrets.jpg') }}" class="img-responsive"></p>
            <p>Διόρθωση και επικαιροποίηση μυστικών κάθως επίσης και αποτελέσματα βαθμολογίας απο τους χρήστες.</p>
            <p><img src="{{ asset('dist/img/author_profile.jpg') }}" class="img-responsive"></p>
            <p>Αύξηση της προβολής σας μέσω των κοινοποιήσεων των μυστικών σας από αναγνωστες και αναβάθμιση της παρουσίας  σας online μέσω του προσωπικου σας προφίλ.</p>
            <p>Αν έχετε ή θέλετε να δημιουργήσετε περιεχόμενο σχετικό με το βιβλίο σας, με τη booksecrets είστε σίγουροι πως ο ενδιαφερόμενος αναγνώστης δεν χρειάζεται να αναζητήσει πλέον το περιεχόμενο αυτό στο internet. Ο κάθε αναγνώστης μπορεί να χρησιμοποιήσει την web ή mobile εφαρμογή μας, όπου και αν βρίσκεται: στο βιβλιοπωλείο, στο σπίτι, στο ταξίδι. Δημιουργήστε σήμερα τον λογαριασμό σας, ζωντανέψτε όλα τα αντίτυπα των βιβλίων σας και εκπλήξτε ευχάριστα τους αναγνώστες σας.</p>

            <a href="{{ url('/#sign-up') }}" target="_self" class="btn btn-primary btn-lg">ΔΗΜΙΟΥΡΓΙΑ ΛΟΓΑΡΙΑΣΜΟΥ</a>
            
            <ul class="list-unstyled double row">
                <li class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <img src="{{ asset('dist/img/step01_02.jpg') }}">
                    <p class="blue">ΒΗΜΑ 1</p>
                    <p>Oι αναγνώστες μέσω της booksecrets ανακαλύπτουν σχετικά μυστικά, και το επόμενο βιβλίο που θα διαβάσουν.</p>
                </li>
                <li class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <img src="{{ asset('dist/img/step02_02.jpg') }}">
                    <p class="blue">ΒΗΜΑ 2</p>
                    <p>Οι συγγραφείς, οι εκδότες και η booksecrets, προσθέτουν μυστικά, σε βιβλία κάθε ενδιαφέροντος.</p>
                </li>
                <li class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <img src="{{ asset('dist/img/step03_02.jpg') }}">
                    <p class="blue">ΒΗΜΑ 3</p>
                    <p>Oι αναγνώστες λαμβάνουν ειδοποίηση για νέα μυστικά που αφορούν την βιβλιοθήκη τους.</p>
                </li>
            </ul>
            
        </main>
        
    </div>
    
</section>

@include('inc/footer')

	
@stop
