@extends('user.wrap')

@section('userChild')

<div class="main-wrap secret-wrap" ng-controller="SecretController">
    
    <div class="row feedlist-wrap">
        
        <div class="col-md-12 col-sm-12 col-xs-12">
            
        <div class="info-wrap" ng-init="item = {{ $secretId }}; secret = {{ json_encode($secret) }}">
                <h2> {{ $secret->title }} </h2>             

                @if ( $secret->locked == 1 && $secret->userLocked == 1 )
                
                    <p ng-class="isCollapsed ? 'locked' : ''" ng-if="isCollapsed">
                        <i class="icon-locked"></i> <span>Κλειδωμένο μυστικό</span>
                    </p>
                    
                    <div ng-hide="isCollapsed" class="collapse-con">
                        <div>
                            <p>Εισάγετε το ISBN του βιβλίου για να ξεκλειδώσετε το μυστικό: </p>
                            <form class="text-right" angular-validator-submit="unlockSecret()" name="unlockForm" novalidate angular-validator>
                                <label for="isbn_input"></label>
                                <input 
                                    id="isbn_input"
                                    ng-model="isbn[item]" 
                                    class="form-control"
                                    type = "text"
                                    name = "isbn_number"
                                    validator = "isbnValidatorSecret(isbn[item]) === true"
                                    required-message="'Το πεδίο είναι υποχρεωτικό'"
                                    invalid-message = "isbnValidatorSecret(isbn[item])"
                                    validate-on="dirty"
                                    required style="width: 100%;"/>
                                <button type="submit" class="btn btn-green" ng-disabled="unlockForm.$invalid">Ξεκλείδωμα</button>
                            </form>
                        </div>
                	</div>
                        
                	<div class="info-actions">
                        <button class="btn collapse-link" ng-click="isCollapsed = !isCollapsed">
                            <span>Ξεκλειδώστε το μυστικό</span>
                            <i class="fa" ng-class="isCollapsed ? 'fa-caret-down' : 'fa-caret-up'"></i>
                        </button>
                    </div>
                    
                @else
                    
                    <div style="padding-bottom: 30px;">{!! $secret->text !!}</div>
                    
                    @if ( $secret->userByCreatedByUserId != null)
                        <div class="info-actions" ng-init="profileImage='{{ $secret->userByCreatedByUserId->profileImage->filePath or '' }}'">
                            @if ( isset($secret->userByCreatedByUserId) )
                                @if ( $secret->userByCreatedByUserId->role == 'AUTHOR' )
                                    <span class="hidden" ng-init="role='AUTHOR'; id={{ $secret->userByCreatedByUserId->author->id or '' }}; name='{{ $secret->userByCreatedByUserId->author->name or '' }}'"></span>
                                    <a  tabindex="0"
                                        class="author red-border btn btn-empty" 
                                        uib-popover-template="dynamicPopover.templateUrl"
                                        popover-placement="right"
                                        popover-trigger="focus">
                                            ΣΥΓΓΡΑΦΕΙΣ
                                    </a>
                                @elseif ( $secret->userByCreatedByUserId->role=='PUBLISHER' )
                                    <span class="hidden" ng-init="role='PUBLISHER'; id={{ $secret->userByCreatedByUserId->publisher->id or '' }}; name='{{ $secret->userByCreatedByUserId->publisher->name }}'"></span>
                                    <a  tabindex="0"
                                        class="author purple-border btn btn-empty" 
                                        uib-popover-template="dynamicPopover.templateUrl"
                                        popover-placement="right"
                                        popover-trigger="focus">
                                            ΕΚΔΟΤΕΣ
                                    </a>
                                @elseif ( $secret->userByCreatedByUserId->role == 'MOBUSER')                                                                        
                                    <span class="hidden" ng-init="role='MOBUSER'"></span>  
                                    <a  tabindex="0" 
                                        class="author orange-border btn btn-empty"
                                        uib-popover-template="dynamicPopover.templateUrl"
                                        popover-placement="right"
                                        popover-trigger="focus" >
                                            {{ strtoupper($secret->userByCreatedByUserId->name) }}
                                    </a>
                                 @else                                                                   
                                    <span class="hidden" ng-init="role='BOOKSECRETS'"></span>  
                                    <a  tabindex="0"
                                        class="author blue-border btn btn-empty" 
                                        uib-popover-template="dynamicPopover.templateUrl"
                                        popover-placement="right"
                                        popover-trigger="focus">
                                            BOOKSECRETS
                                    </a>
                                @endif
                            @else
                                <button class="author red-border btn btn-empty">
                                    @if ( $secret->userByCreatedByUserId->role == 'AUTHOR' )
                                        ΣΥΓΓΡΑΦΕΙΣ
                                    @elseif ( $secret->userByCreatedByUserId->role == 'PUBLISHER' )
                                        ΕΚΔΟΤΕΣ
                                    @elseif ( $secret->userByCreatedByUserId->role == 'MOBUSER' )
                                        ΧΡΗΣΤΕΣ
                                    @else
                                        BOOKSECRETS
                                    @endif
                                </button>
                            @endif
                        </div>
                        
                    @else
                        <div class="info-actions" ng-init="type='{{ $secret->type }}';">                          
                            @if ( $secret->userByCreatedByUserId->role == 'AUTHOR' )
                                <button class="author red-border btn btn-empty">{{ $secret->type }}</button>
                            @elseif ( $secret->userByCreatedByUserId->role == 'PUBLISHER' )
                                <button class="author purple-border btn btn-empty">{{ $secret->type }}</button>
                            @elseif ( $secret->userByCreatedByUserId->role == 'MOBUSER' )
                                <button class="author orange-border btn btn-empty">USER</button>
                            @else
                                <button class="author blue-border btn btn-empty">{{ $secret->type }}</button>
                            @endif
                        </div>
                    @endif
                    
                @endif
                
            </div>
            
            <div class="actions-wrap" ng-init="sec = { id:{{ $secretId }}, userFavorite: {{ $secret->userFavorite }} }; sec.book.id = {{ $secret->bookID }}">
                <uib-rating ng-init="avgRating = {{ $secret->avgRating }}"
                    ng-model="avgRating" 
                    max="5" 
                    readonly="true" 
                    titles="['one','two','three', 'four', 'five']" 
                    aria-labelledby="user-rating" 
                    class="rating">
                </uib-rating>
                
                @if (session()->has('token') && $secret->locked != 1 && $secret->userLocked != 1)
                <button 
                    class="btn btn-empty" 
                    ng-click="editFavorite(sec)">
                    <i class="green" ng-class="sec.userFavorite == 1 ? 'icon-heart-full' : 'icon-un-fav'"></i>
                </button>
                @endif
                
                <span><small>{{ date('d-m-Y', strtotime($secret->createDate)) }}</small></span>
            </div>
            
            <div class="collapsed-book">
                @if ( $secret->locked != 1 && $secret->userLocked != 1 )            
                
                <div class="secret-actions" ng-init="sec = { id:{{ $secretId }}, userFlag: {{ $secret->userFlag }} }">
                   
                    @if (session()->has('token'))                       
                        <uib-rating ng-init="userRating = {{ $secret->userRating }}"
                            ng-model="userRating" 
                            max="max" 
                            ng-click="secretRate({{ $secretId }}, overStar); userRating=overStar;"
                            readonly="false" 
                            on-hover="hoveringOver(value)" 
                            on-leave="overStar = null" 
                            titles="['one','two','three', 'four', 'five']" 
                            aria-labelledby="user-rating" 
                            class="rating">
                        </uib-rating>
                    @endif
                    
                    @if ( $secret->locked != 1 )
                        <ul class="list-inline list-unstyled social">
                            <li>
                                <a href="#" 
                                    socialshare 
                                    socialshare-provider="facebook"
                                    socialshare-media="{{{ isset($secret->book->cover) ? config('common.base_url').$secret->book->cover : asset('dist/img/share.jpg') }}}"
                                    socialshare-type="feed"
                                    socialshare-text="{{ $secret->title }}"
                                    socialshare-description="{{ mb_substr(strip_tags($secret->text), 0, 100) }}..."
                                    socialshare-via="1466663296989400"
                                    socialshare-redirect-uri="{{ url('/') }}"
                                    socialshare-url="{{ url()->current() }}" class="fb">
                                        <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" 
                                    socialshare 
                                    socialshare-provider="twitter"
                                    socialshare-via="booksecretscom"
                                    socialshare-text="{{ $secret->title }}"
                                    socialshare-url="{{ url()->current() }}" class="twitter">
                                        <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" 
                                    socialshare 
                                    socialshare-provider="linkedin"
                                    socialshare-text="{{ $secret->title }}"
                                    socialshare-description="{{ mb_substr(strip_tags($secret->text), 0, 100) }}..."
                                    socialshare-url="{{ url()->current() }}" class="linkedin">
                                        <i class="fa fa-linkedin"></i>
                                </a>
                            </li>
                        </ul>
                    @endif
                    @if (session()->has('token'))
                        <button ng-if="sec.userFlag !== 1" class="report btn btn-empty" ng-click="report(sec)">
                            <i class="icon-flag"></i> Αναφορά
                        </button>
                    @endif
                </div>
                
                @endif
                
            </div>
            
            
        </div>

        <div align="center">
            @if (session('id') == $secret->userByCreatedByUserId->id)                            
                <a href="{{ url('secrets/edit') }}/<% secret.book.id %>/<% secret.id %>" target="_self"><i class="fa fa-pencil-square-o"></i> Επεξεργασία μυστικού</a>
                <button class="btn-empty btn" ng-click="secretDelete(secret)">
                    <i class="fa fa-trash-o"></i> Διαγραφή μυστικού
                </button>
            @endif
        </div>
        
        @if (session()->has('token'))
        <div class="col-md-12 margin-top-40 col-sm-12 col-xs-12">
            <p>Βιβλία με αυτό το μυστικό</p>
            <ul class="list-unstyled secret-books" ng-style="{ 'max-height' : height }">
                
                <li class="clearfix" ng-repeat="book in feedbooks">
                    <a href="{{ url('secrets') }}/<% book.id %>" target="_self" class="cover-wrap feed-wrap">
                        <div>
                            <img alt="<% book.title %>" 
                            ng-src="{{ config('common.base_url') }}<% book.cover %>" 
                            err-SRC="{{ asset('dist/img/bookimage.jpg') }}"> 
                        </div>
                    </a>
                    
                    <div class="info-wrap book-info" style="width: 68%!important;">
                        <div><a href="{{ url('secrets') }}/<% book.id %>" target="_self"> <% book.title %> </a></div>
                        <div>
                            <span class="info-sec">
                                <span>Συγγραφέας: </span>
                                <a href="{{ url('author') }}/<% author.id %>" ng-repeat="author in book.authors" target="_self"> <% author.name %>
                                    <span ng-if="!$last">,</span>
                                </a>
                            </span><br>
                            <span class="info-sec">
                                <span>Κατηγορίες: </span> 
                                <span ng-repeat="category in book.categories"> 
                                    <% category.category %><span ng-if="!$last">,</span>
                                </span>
                            </span>
                        </div>
                        <div> 
                            <span class="info-sec">
                                <span>Εκδότης: </span>
                                <a href="{{ url('publisher') }}/<% publisher.id %>" ng-repeat="publisher in book.publishers" target="_self"> <% publisher.name %>
                                    <span ng-if="!$last">,</span>
                                </a>
                            </span><br>
                            <span class="info-sec">
                                <span> Έτος έκδοσης: </span> <span><% book.pubYear %></span>
                            </span>
                        </div>
                    </div>
                    <div class="actions-wrap actions">
                        <button 
                            class="btn btn-empty" 
                            ng-click="libraryBook(book)" style="font-size:13px;">
                            <i class="icon-library" ng-class="book.userBookStatus === 1 || book.userBookStatus === 0 ? 'green' : ''"></i> 
                            <span ng-if="book.userBookStatus === 1 || book.userBookStatus === 0"><b style="font-size:14px;">Αφαίρεση</b> <br> από τη Βιβλιοθήκη</span>
                            <span ng-if="book.userBookStatus !== 1 && book.userBookStatus !== 0"><b style="font-size:14px;">Προσθήκη</b> <br> στη Βιβλιοθήκη</span>
                            
                        </button>
                        
                    </div>
                </li>
                
            </ul>
        </div>
        @else
        <div class="col-md-12 margin-top-40 col-sm-12 col-xs-12">
            <p>Εξερευνήστε μυστικά για τα δικά σας αγαπημένα βιβλία εδώ!</p>
            <div class="row margin-top-20">
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <a href="{{ url('user/login') }}" target="_self" class="btn btn-primary btn-lg btn-block">ΑΝΑΚΑΛΥΨΤΕ</a>
                </div>
            </div>
        </div>
        @endif
        
        
    </div>
      
</div>
    
@endsection