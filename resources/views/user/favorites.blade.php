@extends('user.wrap')

@section('userChild')


    <section ng-controller="FavoritesController">
        <div class="filter-wrap feed-filters">
            <div class="form-inline">
                <div class="row">
                    <div class="col-md-12 custom-filters feed-custom-filters">
                        <span class="label">ΦΙΛΤΡΑΡΙΣΜΑ: </span>
                        <div class="btn filter-btn" ng-class="order_item == 'createDate' ? 'active' : ''">
                            <button class="btn btn-empty" ng-click="sort()">ΜΕ ΗΜΕΡΟΜΗΝΙΑ <i class="fa" ng-class="order == 'asc' ? 'fa-caret-up' : 'fa-caret-down'"></i></button>
                        </div>

                        <div class="pull-right">
                            <span class="label">ΜΥΣΤΙΚΑ ΑΠΟ: </span>
                            <button class="btn filter-btn filter-tab all-tab" ng-click="showType('all')" ng-class="chosenType == 'all' ? 'active' : ''">ΟΛΑ</button>
                            <button class="btn filter-btn filter-tab b-tab" ng-click="showType('booksecrets')" ng-class="chosenType == 'booksecrets' ? 'active' : ''">BOOKSECRETS</button>
                            <button class="btn filter-btn filter-tab a-tab" ng-click="showType('authors')" ng-class="chosenType == 'authors' ? 'active' : ''">ΣΥΓΓΡΑΦΕΙΣ</button>
                            <button class="btn filter-btn filter-tab p-tab" ng-click="showType('publishers')" ng-class="chosenType == 'publishers' ? 'active' : ''">ΕΚΔΟΤΕΣ</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="main-wrap">
            <div class="feedlist-wrap">
                <h4 ng-if="(!favorites || favorites.length === 0 || total === 0) && !loading" class="center padding-top-50" style="color: #848484;">
                    Δεν έχετε επιλέξει αγαπημένα μυστικά.
                </h4>
                <h4 ng-if="(!favorites || favorites.length === 0 || total === 0) && !loading" class="center" style="color: #848484; font-size:15px;">
                    Aνακαλύψτε περιεχόμενο από τη ροή μυστικών/βιβλίων ή από τη βιβλιοθήκη σας.
                </h4>
                <ul class="list-unstyled row">
                    <li class="col-md-12 col-sm-12 col-xs-12" ng-repeat="favorite in favorites" ng-if="favorite.userFavorite == 1">
                        <a href="{{ url('secrets') }}/<% favorite.book.id %>" target="_self" class="cover-wrap feed-wrap">
                            <div>
                                <img width='140' height='200' 
                                ng-src="{{ config('common.base_url') }}<% favorite.book.cover %>" err-SRC="{{ asset('dist/img/bookimage.jpg') }}">
                            </div>
                        </a>
                        <div class="info-wrap feeds-info-wrap" ng-init="isCollapsed = true">
                            <h4> <a href="{{ url('secrets') }}/<% favorite.book.id %>/<% favorite.id %>" target="_self"><% favorite.title %></a> </h4>
                            <p ng-hide="!isCollapsed" ><% favorite.text | htmlToPlaintext | limitTo:100 %>...</p>
        
                            <div ng-hide="isCollapsed" class="collapse-con ng-hide">
                                <p ng-bind-html="renderHtml(favorite.text)"></p>
                            </div>
                            <div class="info-actions">
                                
                                <a  
                                    tabindex="0"
                                    class="author red-border btn btn-empty" 
                                    ng-if="feed.userByCreatedByUserId.role == 'AUTHOR'" 
                                    uib-popover-template="dynamicPopover.templateUrl"
                                    popover-placement="right"
                                    popover-trigger="focus">ΣΥΓΓΡΑΦΕΙΣ</a>
                                <a 
                                    tabindex="0"
                                    class="author blue-border btn btn-empty" 
                                    ng-if="feed.userByCreatedByUserId.role == 'SUPERADMIN'" 
                                    uib-popover-template="dynamicPopover.templateUrl"
                                    popover-placement="right"
                                    popover-trigger="focus">BOOKSECRETS</a>
                                <a  
                                    tabindex="0"
                                    class="author purple-border btn btn-empty" 
                                    ng-if="feed.userByCreatedByUserId.role == 'PUBLISHER'" 
                                    uib-popover-template="dynamicPopover.templateUrl"
                                    popover-placement="right"
                                    popover-trigger="focus">ΕΚΔΟΤΕΣ</a>

                                <a  
                                    tabindex="0"
                                    class="author orange-border btn btn-empty"
                                    style="text-transform: uppercase;" 
                                    ng-if="feed.userByCreatedByUserId.role == 'MOBUSER'"
                                    uib-popover-template="dynamicPopover.templateUrl"
                                    popover-placement="right"
                                    popover-trigger="focus"><% favorite.userByCreatedByUserId.name %></a>
                                
                                <a href="/secrets/<% favorite.book.id %>/<% favorite.id %>" ng-if="favorite.booksCount > 1" target="_self">Click to see more books that contain this secret</a>
                                <button class="btn collapse-link <% isCollapsed %>" ng-click="isCollapsed=readMore(favorite,isCollapsed)">
                                    <span ng-if="isCollapsed" class="load-more">Δείτε περισσότερα</span>
                                    <i class="fa" ng-class="isCollapsed ? 'fa-caret-down' : 'fa-caret-up'"></i>
                                </button>
                            </div>
                        </div>
                        <div class="actions-wrap">
                            <uib-rating ng-init="avgRating = favorite.avgRating"
                                ng-model="avgRating" 
                                max="5" 
                                readonly="true" 
                                titles="['one','two','three', 'four', 'five']" 
                                aria-labelledby="user-rating" 
                                class="rating">
                            </uib-rating>
                            
                            <button 
                                class="btn btn-empty" 
                                ng-click="editFavoriteCtrl(favorite)">
                                <i class="green icon-heart-full"></i></button>
                            
                            <span><small><% favorite.createDate | date:'dd-MM-yyyy' %></small></span>
                        </div>
                        <div class="collapsed-book" ng-show="!isCollapsed">
                            <div class="secret-actions" ng-init="idR={{{session()->has('id') ? session('id') : false }}}">
                                @if (session()->has('role') && session('role') != 'MOBUSER')
                                    <uib-rating ng-init="rating = favorite.userRating"
                                        @if (session('role') == 'AUTHOR')
                                            ng-if="(favorite.userByCreatedByUserId.author && favorite.userByCreatedByUserId.author.id != idR) || !favorite.userByCreatedByUserId.author"
                                        @else
                                            ng-if="(favorite.userByCreatedByUserId.publisher && favorite.userByCreatedByUserId.publisher.id != idR) || !favorite.userByCreatedByUserId.publisher"
                                        @endif
                                        ng-model="rating" 
                                        max="max" 
                                        readonly="false" 
                                        on-hover="hoveringOver(value)" 
                                        on-leave="overStar = null"
                                        ng-click="secretRate(favorite.id, rating)"
                                        titles="['one','two','three', 'four', 'five']" 
                                        aria-labelledby="user-rating" 
                                        class="rating">
                                    </uib-rating>
                                @else
                                    <uib-rating ng-init="rating = favorite.userRating"
                                        ng-model="rating" 
                                        max="max" 
                                        readonly="false" 
                                        on-hover="hoveringOver(value)" 
                                        on-leave="overStar = null"
                                        ng-click="secretRate(favorite.id, rating)"
                                        titles="['one','two','three', 'four', 'five']" 
                                        aria-labelledby="user-rating" 
                                        class="rating">
                                    </uib-rating>
                                @endif
                                <ul class="list-inline list-unstyled social" ng-if="favorite.locked != 1">
                                    <li>
                                        <a href="#" 
                                            socialshare 
                                            socialshare-provider="facebook"
                                            socialshare-media="{{config('common.base_url') }}<% favorite.book.cover %>"
                                            socialshare-type="feed"
                                            socialshare-text="<% favorite.title %>"
                                            socialshare-description="<% favorite.text | htmlToPlaintext | limitTo:150 %>..."
                                            socialshare-via="1466663296989400"
                                            socialshare-redirect-uri="{{ url('/') }}"
                                            socialshare-url="{{ url('secrets') }}/<% favorite.book.id %>/<% favorite.id %>" class="fb">
                                                <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" 
                                            socialshare 
                                            socialshare-provider="twitter"
                                            socialshare-via="booksecretscom"
                                            socialshare-text="<% favorite.title %>"
                                            socialshare-url="{{ url('secrets') }}/<% favorite.book.id %>/<% favorite.id %>" class="twitter">
                                                <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" 
                                            socialshare 
                                            socialshare-provider="linkedin"
                                            socialshare-text="<% favorite.title %>"
                                            socialshare-description="<% favorite.text | htmlToPlaintext | limitTo:150 %>..."
                                            socialshare-url="{{ url('secrets') }}/<% favorite.book.id %>/<% favorite.id %>" class="linkedin">
                                                <i class="fa fa-linkedin"></i>
                                        </a>
                                    </li>
                                </ul>
                                <button class="report btn btn-empty" ng-if="favorite.userLocked != 1 && favorite.userFlag != 1" ng-click="report(favorite)"><i class="icon-flag"></i> Αναφορά</button>
                            </div>
                            <div class="about-book">
                                <span class="hidden-xs">Σχετικά<br> με το<br> βιβλίο</span>
                                <span class="hidden-sm hidden-md hidden-lg">Σχετικά με το βιβλίο</span>
                            </div>
                            <div class="book-info">
                                <div>
                                    <a href="{{ url('secrets') }}/<% favorite.book.id %>" target="_self"> <% favorite.book.title %> </a> 
                                    <span ng-if="favorite.book.totalSecrets > 1">Το βιβλίο έχει περισσότερα μυστικά</span>
                                </div>
                                <div>
                                    <span class="info-sec">
                                        <span>Συγγραφέας: </span> 
                                        <a href="{{ url('author') }}/<% author.id %>" ng-repeat="author in favorite.book.authors" target="_self"> <% author.name %>
                                            <span ng-if="!$last">,</span>
                                        </a>
                                    </span><br>
                                    <span class="info-sec">
                                        <span> Κατηγορίες: </span> 
                                        <span ng-repeat="category in favorite.book.categories"> 
                                            <% category.category %><span ng-if="!$last">,</span>
                                        </span>
                                    </span>
                                </div>
                                <div> 
                                    <span class="info-sec">
                                        <span> Έτος έκδοσης: </span> <span><% favorite.book.pubYear %></span>
                                    </span><br>
                                    <span class="info-sec">
                                        <span>Εκδότης: </span> 
                                        <a href="{{ url('publisher') }}/<% publisher.id %>" ng-repeat="publisher in favorite.book.publishers" target="_self"> <% publisher.name %>
                                            <span ng-if="!$last">,</span>
                                        </a>
                                    </span>
                                </div>
                            </div>
                            <div class="actions">
                                <button 
                                    class="btn btn-empty" 
                                    ng-click="libraryBook(favorite.book)">
                                    <i class="icon-library" ng-class="favorite.book.userBookStatus == 1 || favorite.book.userBookStatus == 0 ? 'green' : ''"></i> 
                                    <% favorite.book.userBookStatus == 1 || favorite.book.userBookStatus == 0 ? 'Αφαίρεση' : 'Προσθήκη' %> </button>
                            </div>
                        </div>
                    </li>
                </ul>
                
                <div class="row margin-top-40" ng-if="favorites==null && loading">
                    <div class="col-xs-12 text-center">
                        <div class="loader" ng-if="loading"><div class="loader-inner ball-clip-rotate"><div></div></div></div>
                    </div>
                </div>
                
                <div class="row margin-top-40" ng-if="favorites && moreFeeds < total">
                    <div class="col-xs-12 text-center">
                        <button class="btn btn-empty load-btn" ng-click="loadMore()">Περισσότερα αγαπημένα</button> 
                        <div class="loader" ng-if="loading"><div class="loader-inner ball-clip-rotate"><div></div></div></div>
                    </div>
                </div>
                
            </div>
            
        </div>
        
    </section>


@endsection