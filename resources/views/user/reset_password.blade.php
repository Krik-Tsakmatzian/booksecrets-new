@extends('user.wrap')

@section('userChild')


    @extends('layout.master')

    @section('title') Reset Password | bookesecrets @stop 

    @section('content')

    <section id="user-wrapper" ng-controller="UserController" class="section">
        
        <div class="container">
            
            <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
                <main class="main" style="padding-left: 0; width: 100%;">
                    <h1 class="title">Συμπληρώστε τον νέο κωδικό πρόσβασης σας</h1>   
                    
                    <div class="row">
                        <form angular-validator-submit="resetPassword()" name="resetForm" class="signupForm col-md-12 col-sm-12 col-xs-12" novalidate angular-validator>
                            <div class="form-group">
                                <label class="sr-only" for="resetPass">Κωδικός</label>
                                <input 
                                id="resetPass"
                                class="form-control" 
                                type="password" 
                                name="password"
                                ng-model="reset.pwd" 
                                validator = "passwordValidator(reset.pwd) === true"
                                required-message="'Το πεδίο είναι υποχρεωτικό'"
                                invalid-message = "passwordValidator(reset.pwd)"
                                validate-on="dirty"
                                placeholder="Password"
                                required /> 
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block" ng-disabled="resetForm.$invalid">ΕΠΑΝΑΦΟΡΑ</button>
                            </div>
        
                        </form>
                    </div>
                    
                </main>
            </div>

        </div>
        
    </section>

    <div ng-controller="mainController">
        @include('inc/footer')
    </div>

        
    @stop
    

@endsection    