<div class="filter-wrap feed-filters" ng-controller="FiltersController">
    <div class="form-inline">
        <div class="row">
            <div class="col-md-12">
                <p>Ανάζητηστε βιβλία από την <a href="{{ url('user/library') }}" target="_self"><b>βιβλιοθήκη σας</b></a> και αποκαλύψτε τα μυστικά τους!</p>
            </div>
            <div class="col-md-12 custom-filters">
                <span class="label">ΦΙΛΤΡΑΡΙΣΜΑ: </span>
                <div class="btn filter-btn" ng-class="order_item == 'createDate' ? 'active' : ''">
                    <button class="btn btn-empty" ng-click="sortBy('date')" >ΜΕ ΗΜΕΡΟΜΗΝΙΑ</button>
                    <button class="btn btn-empty" ng-click="sort('date')"><i class="fa" ng-class="order_date == 'asc' ? 'fa-caret-down' : 'fa-caret-up'"></i></button>
                </div>
                <!--<div class="btn filter-btn" ng-class="order_item == 'title' ? 'active' : ''">-->
                <!--    <button class="btn btn-empty" ng-click="sortBy('title')" >BY TITLE</button>-->
                <!--    <button class="btn btn-empty" ng-click="sort('title')"><i class="fa" ng-class="order_title == 'asc' ? 'fa-caret-down' : 'fa-caret-up'"></i></button>-->
                <!--</div>-->
                
                @if ( url()->current() != 'secrets/all' )
                    <div class="pull-right">
                        <span class="label">ΜΥΣΤΙΚΑ ΑΠΟ: </span>
                        <button class="btn filter-btn filter-tab all-tab" ng-click="showType('all')" ng-class="chosenType == 'ALL' ? 'active' : ''">ΟΛΑ</button>
                        <button class="btn filter-btn filter-tab b-tab" ng-click="showType('booksecrets')" ng-class="chosenType == 'BOOKSECRETS' ? 'active' : ''">BOOKSECRETS</button>
                        <button class="btn filter-btn filter-tab a-tab" ng-click="showType('authors')" ng-class="chosenType == 'AUTHOR' ? 'active' : ''">ΣΥΓΓΡΑΦΕΙΣ</button>
                        <button class="btn filter-btn filter-tab p-tab" ng-click="showType('publishers')" ng-class="chosenType == 'PUBLISHER' ? 'active' : ''">ΕΚΔΟΤΕΣ</button>
                    </div>
                @else
                    <div class="pull-right">
                        <span class="label">ΦΙΛΤΡΑΡΙΣΜΑ ΜΥΣΤΙΚΩΝ: </span>
                        <button class="btn filter-btn filter-tab all-tab" ng-click="showType('all')" ng-class="chosenType == 'ALL' ? 'active' : ''">ΟΛΑ</button>
                        <button class="btn filter-btn filter-tab published-tab" ng-click="showType('published')" ng-class="chosenType == 'PUBLISHED' ? 'active' : ''">ΔΗΜΟΣΙΕΥΜΕΝΑ</button>
                        <button class="btn filter-btn filter-tab draft-tab" ng-click="showType('draft')" ng-class="chosenType == 'DRAFT' ? 'active' : ''">ΠΡΟΧΕΙΡΑ</button>
                    </div>
                @endif
                
            </div>
        </div>
    </div>
</div>