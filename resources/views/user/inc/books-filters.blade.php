@if ( session()->has('role') && session('role') != 'MOBUSER' && url()->current() != 'user/library' )
<div class="filter-wrap" ng-controller="FiltersController">
@else
<div class="filter-wrap" ng-controller="LibraryController">
@endif
    <form id="filters" ng-submit="bookSearch()">
        <div class="row">
            <div class="form-group col-md-12">
                <label class="sr-only" for="search">Αναζήτηση</label>
                <div class="input-group search-group">
                    
                    <input type="text" class="form-control" id="search" placeholder="Αναζήτηση βιβλίων" ng-model="search.title">
                    <div class="input-group-addon">
                        <div class="loader search-loading" ng-if="search_loading"><div class="loader-inner ball-clip-rotate"><div></div></div></div>
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                    
                    <div class="input-group-addon filters-collapse">
                        <button type="button" class="btn btn-default" ng-click="showFilters()">
                            Εργαλεια <i class="fa fa-caret-down"></i></button>
                    </div>
                </div>
            </div>
            
            <div ng-show="filters" class="col-md-12 more-filters">
                
                <div class="form-horizontal">
                      
                    <div class="form-group">
                        <label class="control-label col-md-2" for="author">Συγγραφέας</label>
                        <input type="text" class="form-control col-md-4" id="author" placeholder="Συγγραφέας" ng-model="search.author">
                    
                        <label class="control-label col-md-2" for="category">Κατηγορία</label>
                        <input type="text" class="form-control col-md-4" id="category" placeholder="Κατηγορία" ng-model="search.category">
                    </div>
                      
                    <div class="form-group">
                        <label class="control-label col-md-2" for="publisher">Εκδότης</label>
                        <input type="text" class="form-control col-md-4" id="publisher" placeholder="Εκδότης" ng-model="search.publisher">
                        
                        <label class="control-label col-md-2" for="isbn">ISBN</label>
                        <input type="text" class="form-control col-md-4" id="isbn" placeholder="ISBN" ng-model="search.isbn">
                    </div>   
                    
                    <div class="form-group">
                        <label class="control-label col-md-2" for="year">Έτος</label>
                        <input type="number" class="form-control col-md-4" id="year" placeholder="Έτος" ng-model="search.year">
                    </div>   
                    
                    <div class="form-group text-right">
                        <button class="btn btn-primary" ng-click="bookSearch()">Αναζητηση βιβλιων</button>
                    </div>
                
                </div>
            
            </div>
          
        </div>
    </form>
</div>