@extends('user.wrap')

@section('userChild')


    <section ng-controller="FeedsController">
        <div class="filter-wrap feed-filters">
            <form id="filters" class="feed-filters">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="sr-only" for="search">Αναζήτηση</label>
                        <div class="input-group search-group">
                            <input type="text" ng-keyup="$event.keyCode == 13 ? bookSearch() : null" class="form-control" id="search" placeholder="Αναζητήστε μυστικά με τίτλο βιβλίου, συγγραφέα ή εκδότη" ng-model="query">
                            <div class="input-group-addon">
                                <div class="loader search-loading" ng-if="search_loading && !loading"><div class="loader-inner ball-clip-rotate"><div></div></div></div>
                                <button type="submit" class="btn btn-default" ng-click="feedSearch()"><i class="fa fa-search"></i></button>
                            </div>
                            
                        </div>
                        <div class="margin-top-20">
                            <p ng-if="search_res == true && search_loading == false">
                                <span ng-if="total > 0 && total != 1">Βρέθηκαν <% total %> αποτελέσματα για: <% old_query %></span>
                                <span ng-if="total == 1">Βρέθηκε <% total %> αποτέλεσμα για: <% old_query %></span>
                                <span ng-if="total == 0">Δεν βρέθηκαν 0 αποτελέσματα για: <% old_query %></span>
                                <a href="#" ng-click="clearSearch()">Καθαρισμός αναζήτησης <i class="fa fa-times" aria-hidden="true"></i></a>
                            </p>
                        </div>
                    </div>
                </div>
            </form>
            
            <div class="form-inline">
                <div class="row">
                    <div class="col-md-12 custom-filters feed-custom-filters">
                        <span class="label">ΦΙΛΤΡΑΡΙΣΜΑ: </span>
                        <div class="btn filter-btn" ng-class="order_item == 'createDate' ? 'active' : ''">
                            <button class="btn btn-empty" ng-click="sort()">ΜΕ ΗΜΕΡΟΜΗΝΙΑ <i class="fa" ng-class="order == 'asc' ? 'fa-caret-up' : 'fa-caret-down'"></i></button>
                        </div>

                        <div class="pull-right">
                            <span class="label">ΜΥΣΤΙΚΑ ΑΠΟ: </span>
                            <button class="btn filter-btn filter-tab all-tab" ng-click="showType('all')" ng-class="chosenType == 'all' ? 'active' : ''">ΟΛΑ</button>
                            <button class="btn filter-btn filter-tab b-tab" ng-click="showType('booksecrets')" ng-class="chosenType == 'booksecrets' ? 'active' : ''">BOOKSECRETS</button>
                            <button class="btn filter-btn filter-tab a-tab" ng-click="showType('authors')" ng-class="chosenType == 'authors' ? 'active' : ''">ΣΥΓΓΡΑΦΕΙΣ</button>
                            <button class="btn filter-btn filter-tab p-tab" ng-click="showType('publishers')" ng-class="chosenType == 'publishers' ? 'active' : ''">ΕΚΔΟΤΕΣ</button>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
        <div class="main-wrap">
            <div class="feedlist-wrap">
                <ul class="list-unstyled row">
                    <li class="col-md-12 col-sm-12 col-xs-12" ng-repeat="feed in feedlist">                        
                        <a href="{{ url('secrets') }}/<% feed.book.id %>" target="_self" class="cover-wrap feed-wrap">
                            <div>
                                <img width='140' height='200' 
                                ng-src="{{ config('common.base_url') }}<% feed.book.cover %>" err-SRC="{{ asset('dist/img/bookimage.jpg') }}">
                            </div>
                        </a>
                        <div class="info-wrap feeds-info-wrap" ng-init="iscollapsed = true">
                            <h4 ng-if="feed.userLocked != 1"> <a href="{{ url('secrets') }}/<% feed.book.id %>/<% feed.id %>" target="_self"><% feed.title %></a> </h4>
                            <h4 ng-if="feed.userLocked == 1"> <% feed.title %> </h4>
                            
                            <p ng-if="feed.userLocked !== 1"><% feed.text | htmlToPlaintext | limitTo:100 %>...</p>

                            <p ng-class="feed.userLocked === 1 && iscollapsed ? 'locked' : ''" ng-if="feed.userLocked === 1 && iscollapsed">
                                <i class="icon-locked"></i> <span>Κλειδωμένο μυστικό</span>
                            </p>
                            <div ng-hide="iscollapsed" class="collapse-con ng-hide">
                                {{-- <div ng-if="feed.userLocked != 1">
                                    <p ng-bind-html="renderHtml(feed.text)"></p>
                                </div> --}}
                                <div ng-if="feed.userLocked === 1">
                                    <p>Εισάγετε το ISBN του βιβλίου για να ξεκλειδώσετε το μυστικό: </p>
                                    <form class="text-right" angular-validator-submit="unlock(feed)" name="unlockForm" novalidate angular-validator>
                                        <label for="isbn_input"></label>
                                        <input 
                                            id="isbn_input"
                                            ng-model="isbn[feed.id]" 
                                            class="form-control"
                                            type = "text"
                                            name = "isbn_number"
                                            validator = "isbnValidator(isbn[feed.id]) === true"
                                            required-message="'Το πεδίο είναι υποχρεωτικό'"
                                            invalid-message = "isbnValidator(isbn[feed.id])"
                                            validate-on="dirty"
                                            required style="width: 100%;"/>
                                        <button type="submit" class="btn btn-green" ng-disabled="unlockForm.$invalid">Ξεκλείδωμα</button>
                                    </form>
                                </div>
                            </div>
                            <div class="info-actions">
                                {{-- <% feed.type=='USER' ? feed : '' %> --}}
                                <a  
                                    tabindex="0"
                                    class="author red-border btn btn-empty" 
                                    ng-if="feed.userByCreatedByUserId.role === 'AUTHOR'" 
                                    uib-popover-template="dynamicPopover.templateUrl"
                                    popover-placement="right"
                                    popover-trigger="focus">ΣΥΓΓΡΑΦΕΙΣ</a>
                                <a  
                                    tabindex="0"
                                    class="author blue-border btn btn-empty" 
                                    ng-if="feed.userByCreatedByUserId.role === 'SUPERADMIN'" 
                                    uib-popover-template="dynamicPopover.templateUrl"
                                    popover-placement="right"
                                    popover-trigger="focus">BOOKSECRETS</a>
                                <a  
                                    tabindex="0"
                                    class="author purple-border btn btn-empty" 
                                    ng-if="feed.userByCreatedByUserId.role === 'PUBLISHER'" 
                                    uib-popover-template="dynamicPopover.templateUrl"
                                    popover-placement="right"
                                    popover-trigger="focus">ΕΚΔΟΤΕΣ</a>

                                <a  
                                    tabindex="0"
                                    class="author orange-border btn btn-empty"
                                    style="text-transform: uppercase;" 
                                    ng-if="feed.userByCreatedByUserId.role === 'MOBUSER'"
                                    uib-popover-template="dynamicPopover.templateUrl"
                                    popover-placement="right"
                                    popover-trigger="focus"><% feed.userByCreatedByUserId.name %></a>

                                <a href="/secrets/<% feed.book.id %>/<% feed.id %>" ng-if="feed.booksCount > 1 && feed.userLocked != 1" target="_self">
                                    Βιβλία με το ίδιο μυστικό</a>

                                <button class="btn collapse-link" ng-click="iscollapsed = redirectToSecret(feed, iscollapsed)">
                                    <span ng-if="feed.userLocked !== 1">Περισσότερα...</span>
                                    <span ng-if="feed.userLocked === 1">Ξεκλειδώστε το μυστικό</span>
                                    <i ng-if="feed.hasImage == true && feed.userLocked != 1" class="icon-photo"></i>
                                    <i ng-if="feed.hasAudio == true && feed.userLocked != 1" class="icon-voice"></i>
                                    <i ng-if="feed.hasVideo == true && feed.userLocked != 1" class="icon-camera"></i>
                                    <i class="fa fa-caret-right"></i>
                                </button>
                            </div>
                        </div>
                        <div class="actions-wrap">
                            <uib-rating ng-init="avgRating = feed.avgRating"
                                ng-model="avgRating" 
                                max="5" 
                                readonly="true" 
                                titles="['one','two','three', 'four', 'five']" 
                                aria-labelledby="user-rating" 
                                class="rating">
                            </uib-rating>
                            
                            <button ng-if="feed.userLocked != 1" class="btn btn-empty" ng-click="editFavorite(feed)">
                                <i class="green" ng-class="feed.userFavorite == 1 ? 'icon-heart-full' : 'icon-un-fav'"></i></button>
                            
                            <span><small><% feed.createDate | date:'dd-MM-yyyy' %></small></span>
                        </div>                        
                    </li>
                </ul>
                
                <div class="row margin-top-40" ng-if="feedlist==null && loading">
                    <div class="col-xs-12 text-center">
                        <div class="loader" ng-if="loading"><div class="loader-inner ball-clip-rotate"><div></div></div></div>
                    </div>
                </div>
                
                <div class="row margin-top-40" ng-if="feedlist && moreFeeds < total">
                    <div class="col-xs-12 text-center">
                        <button class="btn btn-empty load-btn" ng-click="loadMore()">Περισσότερα μυστικά</button> 
                        <div class="loader" ng-if="loading"><div class="loader-inner ball-clip-rotate"><div></div></div></div>
                    </div>
                </div>
                
            </div>
        </div>
        
    </section>

@endsection