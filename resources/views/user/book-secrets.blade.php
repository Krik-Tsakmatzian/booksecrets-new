@extends('user.wrap')

@section('userChild')

<div class="main-wrap secret-wrap" ng-controller="BookSecretsController">
    
    <div class="row feedlist-wrap">
        @if (isset($book_secrets))
            <h2 style="line-height: 36px; padding-left: 15px;">Μυστικά Βιβλιου</h2>
            <h4 style="color: #848484; padding-left: 15px;">{{ $book_secrets->title }}</h4>
        @endif
        <div class="col-md-12 col-sm-12 col-xs-12 padding-top-30">
            @if (isset($book_secrets))           
            <div class="cover-wrap img-wrap book-wrapper">
                <div>
                    <img alt="<% book.title %>" 
                    ng-src="{{ config('common.base_url') }}/{{ $book_secrets->cover }}" 
                    err-SRC="{{ asset('dist/img/bookimage.jpg') }}"> 
                </div>
                
                @if ( isset($book_secrets->buyBookURL) )
                    @if ( $book_secrets->buyBookURL != '' && $book_secrets->buyBookURL != null )
                        <div class="buy-wrap">
                           <a href="{{ $book_secrets->buyBookURL }}" class="btn btn-buy btn-sm" target="_blank">Buy Book</a>     
                        </div>
                    @endif
                @endif
            </div>
            <div ng-init="item = {{ $bookId }}">
                <ul ng-if="secrets && secrets.length > 0" class="list-unstyled secret-books secret-list" ng-style="{ 'max-height' : height }">
                    
                    <li class="clearfix" ng-repeat="secret in secrets">
                        <div class="info-wrap">
                            <h4 ng-if="secret.userLocked !== 1"> <a href="{{ url('secrets') }}/{{ $bookId }}/<% secret.id %>" target="_self"><% secret.title %></a> </h4>
                            <h4 ng-if="secret.userLocked === 1"> <% secret.title %> </h4>
                            
                            <p ng-if="secret.userLocked !== 1"><% secret.text | htmlToPlaintext | limitTo:100 %>...</p>
                        
                            <p ng-class="secret.userLocked === 1 && isCollapsed ? 'locked' : ''" ng-if="secret.userLocked === 1 && isCollapsed">
                                <i class="icon-locked"></i> <span>Κλειδωμένο μυστικό</span>
                            </p>
                            
                            <div ng-hide="isCollapsed" class="collapse-con">                                
                                <div ng-if="secret.userLocked == 1">
                                    <p>Εισάγετε το ISBN του βιβλίου για να ξεκλειδώσετε το μυστικό: </p>
                                    <form class="text-right" angular-validator-submit="unlock(secret, bookInfo)" name="unlockForm" novalidate angular-validator>
                                        <label for="isbn_input"></label>
                                        <input 
                                            id="isbn_input"
                                            ng-model="isbn[secret.id]" 
                                            class="form-control"
                                            type = "text"
                                            name = "isbn_number"
                                            validator = "isbnValidator(isbn[secret.id]) === true"
                                            required-message="'Το πεδίο είναι υποχρεωτικό'"
                                            invalid-message = "isbnValidator(isbn[secret.id])"
                                            validate-on="dirty"
                                            required style="width: 100%;"/>
                                        <button type="submit" class="btn btn-green" ng-disabled="unlockForm.$invalid">Ξεκλείδωμα</button>
                                    </form>
                                </div>
                            </div>
                                
                            <div class="info-actions" ng-init="profileImage=secret.userByCreatedByUserId.profileImage.filePath">                               
                                <a  ng-init="role='AUTHOR'"
                                    tabindex="0"
                                    class="author red-border btn btn-empty" 
                                    ng-if="secret.userByCreatedByUserId.role === 'AUTHOR'" 
                                    uib-popover-template="dynamicPopover.templateUrl"
                                    popover-placement="right"
                                    popover-trigger="focus">ΣΥΓΓΡΑΦΕΙΣ</a>
                                <a  ng-init="role='PUBLISHER'"
                                    tabindex="0"
                                    class="author purple-border btn btn-empty" 
                                    ng-if="secret.userByCreatedByUserId.role === 'PUBLISHER'" 
                                    uib-popover-template="dynamicPopover.templateUrl"
                                    popover-placement="right"
                                    popover-trigger="focus">ΕΚΔΟΤΕΣ</a>
                                <a  ng-init="role='MOBUSER'"
                                    tabindex="0"
                                    class="author orange-border btn btn-empty"
                                    style="text-transform: uppercase;" 
                                    ng-if="secret.userByCreatedByUserId.role === 'MOBUSER'"
                                    uib-popover-template="dynamicPopover.templateUrl"
                                    popover-placement="right"
                                    popover-trigger="focus"><% secret.userByCreatedByUserId.name %></a>
                                <a  ng-init="role='BOOKSECRETS';"
                                    tabindex="0"
                                    class="author blue-border btn btn-empty" 
                                    ng-if="secret.userByCreatedByUserId.role === 'SUPERADMIN'" 
                                    uib-popover-template="dynamicPopover.templateUrl"
                                    popover-placement="right"
                                    popover-trigger="focus">BOOKSECRETS</a>
                                

                                

                                <button class="btn collapse-link" ng-click="isCollapsed = redirectToSecret(secret, isCollapsed)">
                                    <span ng-if="secret.userLocked !== 1">Περισσότερα...</span>
                                    <span ng-if="secret.userLocked === 1">Ξεκλειδώστε το μυστικό</span>
                                    <i ng-if="secret.hasImage == true && secret.userLocked != 1" class="icon-photo"></i>
                                    <i ng-if="secret.hasAudio == true && secret.userLocked != 1" class="icon-voice"></i>
                                    <i ng-if="secret.hasVideo == true && secret.userLocked != 1" class="icon-camera"></i>
                                    <i class="fa fa-caret-right"></i>
                                </button>
                            </div>
                        </div>
                        <div class="actions-wrap" ng-init="secret.book.id = {{ $bookId }}">
                            <uib-rating ng-init="avgRating = secret.avgRating"
                                ng-model="avgRating" 
                                max="max" 
                                readonly="true" 
                                on-leave="overStar = null" 
                                titles="['one','two','three', 'four', 'five']" 
                                aria-labelledby="average-rating" 
                                class="rating">
                            </uib-rating>
                            
                            <button ng-if="secret.userLocked != 1" class="btn btn-empty" ng-click="editFavorite(secret)">
                                <i class="green" ng-class="secret.userFavorite == 1 ? 'icon-heart-full' : 'icon-un-fav'"></i></button>
                                
                            <ul class="list-inline list-unstyled social" ng-if="favorite.locked != 1">
                                <li>
                                    <a href="#" socialshare socialshare-provider="facebook"
                                    socialshare-media="{{ config('common.base_url') }}/{{ $book_secrets->cover }}"
                                    socialshare-type="feed"
                                    socialshare-text="<% secret.title %>"
                                    socialshare-description="<% secret.text | htmlToPlaintext | limitTo:150 %>..."
                                    socialshare-url="{{ url('secrets') }}/{{ $bookId }}/<% secret.id %>" class="fb">
                                    <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" socialshare socialshare-provider="twitter"
                                    socialshare-via="booksecretscom"
                                    socialshare-text="<% secret.title %>"
                                    socialshare-url="{{ url('secrets') }}/{{ $bookId }}/<% secret.id %>" class="twitter">
                                    <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" socialshare socialshare-provider="linkedin"
                                    socialshare-text="<% secret.title %>"
                                    socialshare-description="<% secret.text | htmlToPlaintext | limitTo:150 %>..."
                                    socialshare-url="{{ url('secrets') }}/{{ $bookId }}/<% secret.id %>" class="linkedin">
                                    <i class="fa fa-linkedin"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="center loader" ng-if="loading && (!secrets || secrets.length === 0)">
                <div class="loader-inner ball-clip-rotate">
                    <div></div>
                </div>
            </div>
            <h4 ng-if="!loading && (!secrets || secrets.length === 0)" class="center padding-top-30" style="color: #848484;">Δεν υπάρχουν ακόμα μυστικά για το συγκεκριμένο βιβλίο</h3>
            <div class="center" style="padding: 30px 0;">
                @if ( session()->has('role') && session('role') == 'MOBUSER')
                <a href="/secrets/add/{{ $book_secrets->isbn13}}" target="_self" class="add-button">
                    <span style="font-size: 16px;">
                    Nέο Μυστικό
                    </span>
                </a> 
                @endif
                <a ng-if="bookInfo && !(bookInfo.userBookStatus == 1 || bookInfo.userBookStatus == 0)" ng-click="libraryBook(bookInfo)" class="add-button" style="background: #00ac53;">
                    <span style="font-size: 16px;">
                    Βιβλιοθήκη
                    </span>
                </a>    
            </div>
            
            <div class="about-book padding-top-50">
                <h4>Σχετικά με το βιβλίο</h4>
            </div>
            <div class="collapsed-book margin-top-20">
                
                <div class="book-info">
                    <div><b>{{ $book_secrets->title }}</b></div>
                    <div>
                        <span class="info-sec">
                            <span>Συγγραφείς: </span> 
                            @foreach ( $book_secrets->authors as $k=>$author )
                                <a href="{{ url('author') }}/{{ $author->id }}" target="_self">{{ $author->name }}{{{ $k != count($book_secrets->authors)-1 ? ',' : '' }}}</a>
                            @endforeach
                        </span><br>
                        <span class="info-sec">
                            <span>Κατηγορίες: </span>
                            @foreach ( $book_secrets->categories as $k=>$cat )
                                <span>{{ $cat->category }}{{{ $k != count($book_secrets->categories)-1 ? ',' : '' }}}</span>
                            @endforeach
                        </span>
                    </div>
                    <div> 
                        <span class="info-sec">
                            <span>Εκδότες: </span> 
                            @foreach ( $book_secrets->publishers as $k=>$publisher )
                                <a href="{{ url('publisher') }}/{{ $publisher->id }}" target="_self">{{ $publisher->name }}{{{ $k != count($book_secrets->publishers)-1 ? ',' : '' }}}</a>
                            @endforeach
                        </span><br>
                        <span class="info-sec">
                            <span>Έτος έκδοσης: </span> <span>{{ $book_secrets->pubYear }}</span>
                        </span>
                    </div>
                </div>
                <div class="actions">
                    <button 
                        ng-if="bookInfo && (bookInfo.userBookStatus == 1 || bookInfo.userBookStatus == 0)"
                        class="btn btn-empty" 
                        ng-click="libraryBook(bookInfo)" style="font-size:13px;">
                            <i class="icon-library" ng-class="bookInfo.userBookStatus == 1 || bookInfo.userBookStatus == 0 ? 'green' : ''"></i> 
                            <b style="font-size:14px;">Αφαίρεση</b> <br> από την Βιβλιοθήκη
                    </button>
                </div>
                @if (isset($book_secrets->desr) && $book_secrets->desr!='')
                <div class="clearfix"></div>
                <div class="summary-sec margin-top-20">
                    {!! $book_secrets->desr !!}
                </div>
                @endif
            </div>
            @endif
        </div>
        
    </div>
      
</div>
    
@endsection