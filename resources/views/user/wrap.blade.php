@extends('layout.master')

@section('title') 
    {{{ $name or 'Secrets' }}} | booksecrets 
@stop 

@section('content')

<section id="wrapper" class="main-wrapper">
    
    <div class="container">
        
        <div class="row">
            
            @if ( session()->has('token') )

                <div class="col-md-3 sidebar-wrap hidden-sm hidden-xs">
                    <ul class="sidebar-nav list-unstyled" set-class-when-at-top="fix-to-top">
                        @include('inc/menu')
                    </ul>
                </div>
                
                <div class="col-md-9 col-xs-12 col-sm-12">    
                    @yield('userChild')
                </div>

            @else

                <div class="col-md-12 col-xs-12 col-sm-12">
                    @yield('userChild')
                </div>

            @endif
            
        </div>
        
        
    </div>
    

</section>

@include('inc/footer')

	
@stop
