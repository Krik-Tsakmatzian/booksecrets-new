@extends('user.wrap')

@section('userChild')

<section ng-controller="BooksController">
    <div class="filter-wrap">
        <form id="filters">
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="sr-only" for="search">Αναζήτηση</label>
                    <div class="input-group search-group">
                        
                        <input type="text" ng-keyup="$event.keyCode == 13 ? bookSearch() : null" class="form-control" id="search" placeholder="Αναζητήστε ένα οποιοδήποτε βιβλίο" ng-model="search.title">
                        <div class="input-group-addon">
                            <div class="loader search-loading" ng-if="search_loading"><div class="loader-inner ball-clip-rotate"><div></div></div></div>
                            <button type="button" class="btn btn-default" ng-click="bookSearch()"><i class="fa fa-search"></i></button>
                        </div>
                        
                        <div class="input-group-addon filters-collapse" ng-if="!hideSearch">
                            <button type="button" class="btn btn-default" ng-click="showFilters()">
                                Εργαλεια <i class="fa fa-caret-down"></i></button>
                        </div>
                    </div>
                </div>
                
                <div ng-show="filters" ng-show="!hideSearch" class="col-md-12 more-filters">
                    
                    <div class="form-horizontal">
                          
                        <div class="form-group">
                            <label class="control-label col-md-2" for="author">Συγγραφέας</label>
                            <input type="text" class="form-control col-md-4" id="author" placeholder="Συγγραφέας" ng-model="search.author">
                            
                            <label class="control-label col-md-2" for="year">Έτος</label>
                            <input type="number" class="form-control col-md-4" id="year" placeholder="Έτος" ng-model="search.year">
                        
                            <!--<label class="control-label col-md-2" for="category">Κατηγορία</label>-->
                            <!--<input type="text" class="form-control col-md-4" id="category" placeholder="Κατηγορία" ng-model="search.category">-->
                        </div>
                          
                        <div class="form-group">
                            <label class="control-label col-md-2" for="publisher">Εκδότης</label>
                            <input type="text" class="form-control col-md-4" id="publisher" placeholder="Εκδότης" ng-model="search.publisher">
                            
                            <label class="control-label col-md-2" for="isbn">ISBN</label>
                            <input type="text" class="form-control col-md-4" id="isbn" placeholder="ISBN" ng-model="search.isbn">
                        </div>   
                        
                        <!--<div class="form-group">-->
                        <!--    <label class="control-label col-md-2" for="year">Έτος</label>-->
                        <!--    <input type="number" class="form-control col-md-4" id="year" placeholder="Έτος" ng-model="search.year">-->
                        <!--</div>   -->
                        
                        <div class="form-group text-right">
                            <button type="button" class="btn btn-primary" ng-click="bookSearch()">Αναζητηση βιβλιων</button>
                        </div>
                    
                    </div>
                
                </div>
              
            </div>
        </form>
    </div>
    
    <div class="main-wrap books-wrap">
        
        <ul class="list-unstyled list-inline library-nav" ng-init="visibleView = 'list'">
            <li ng-class="filterTab == 'data' ? 'active' : ''" ng-show="searchlist">
                <button ng-click="changeFilterTab('data')" class="btn btn-empty">Λίστα βιβλίων</button></li>
            <li ng-class="filterTab == 'search' ? 'active' : ''" ng-show="filterTab == 'search' || searchlist">
                <button ng-click="changeFilterTab('search')" class="btn btn-empty">Αποτελέσματα αναζήτησης</button></li>
            <li ng-class="visibleView == 'table' ? 'active' : ''"><button ng-click="viewChange('table')" class="btn btn-empty"><i class="fa fa-th"></i></button></li>
            <li ng-class="visibleView == 'list' ? 'active' : ''"><button ng-click="viewChange('list')" class="btn btn-empty"><i class="fa fa-list"></i></button></li>
        </ul>
        
        <div class="books-data-wrap feedlist-wrap" ng-if="filterTab ==='data'">           
            <ul class="list-unstyled row">
                <li class="col-md-12 col-sm-12 col-xs-12" 
                    ng-repeat="book in bookslist"                      
                    ng-class="visibleView == 'table' ? 'table' : 'list'">
                    <a href="{{ url('secrets') }}/<% book.id %>" target="_self" class="cover-wrap feed-wrap">
                        <div>
                            <img alt="<% book.title %>" ng-src="{{ config('common.base_url') }}<% book.cover %>" err-SRC="{{ asset('dist/img/bookimage.jpg') }}"> 
                        </div>
                    </a>
                
                    <div class="info-wrap feeds-info-wrap" ng-hide="visibleView == 'table'">
                        <h4> <a href="{{ url('secrets') }}/<% book.id %>" target="_self"><% book.title %></a> </h4>
                        <p ng-if="book.totalSecrets == 0"><em>Το συγκεκριμένο βιβλίο δεν περιέχει ακόμα κάποιο μυστικό.</em></p>
    
                        <div class="collapse-con clearfix">
                            <div class="book-info">
                                <div>
                                    <div class="info-sec">
                                        <span>Συγγραφέας: </span> 
                                        <a href="{{ url('author') }}/<% author.id %>" ng-repeat="author in book.authors | limitTo:5" target="_self"> <% author.name %>
                                            <span ng-if="!$last">,</span>
                                            <span ng-if="$last && book.authors.length > 5">... </span>
                                        </a>
                                    </div>
                                    <br>
                                    <div class="info-sec">
                                        <span> Κατηγορίες: </span> 
                                        <span ng-repeat="category in book.categories"> 
                                            <% category.category %><span ng-if="!$last">,</span>
                                        </span>
                                    </div>                                   
                                </div>
                                <div> 
                                    <div class="info-sec">
                                        <span>Εκδότης: </span> 
                                        <a href="{{ url('publisher') }}/<% publisher.id %>" ng-repeat="publisher in book.publishers" target="_self"> <% publisher.name %>
                                            <span ng-if="!$last">,</span>
                                        </a>
                                    </div>
                                    <br>
                                    <div class="info-sec">
                                        <span> Έτος έκδοσης: </span> <span><% book.pubYear %></span>
                                    </div>                                
                                </div>
                            </div>
                    	</div>
                        <div class="info-actions">                                                                             
                            <button class="btn collapse-link" ng-click="isCollapsed = !isCollapsed" ng-show="book.desr && book.desr!=''">
                                <span>Περίληψη βιβλίου</span><i class="fa" ng-class="isCollapsed ? 'fa-caret-down' : 'fa-caret-up'"></i>
                            </button> 
                        </div>
                        
                    </div>
                    <div class="actions-wrap" ng-if="visibleView != 'table'">
                        <a href="{{ url('secrets') }}/<% book.id %>" class="btn secret-btn" ng-class="book.totalSecrets == 0 ? 'gray' : ''" target="_self">
                            <span><% book.totalSecrets %></span>
                            Μυστικά
                        </a>                            
                        <div>
                            <a href="/secrets/add/<% book.isbn13 %>" target="_self" class="add-button">
                                <span>
                                Νέο Μυστικό
                                </span>
                            </a>     
                        </div> 
                                      
                    </div>
                    <div class="clear"></div>                    
                    <div class="collapsed-book" ng-show="!isCollapsed && visibleView != 'table'">
                        <p ng-bind-html="renderHtml(book.desr)"></p>
                    </div>
                </li>
            </ul>
            
            <div class="row margin-top-40" ng-if="bookslist==null && loading">
                <div class="col-xs-12 text-center">
                    <div class="loader" ng-if="loading"><div class="loader-inner ball-clip-rotate"><div></div></div></div>
                </div>
            </div>
            
            <div class="row margin-top-40" ng-if="bookslist && moreFeeds < total">
                <div class="col-xs-12 text-center">
                    <button class="btn btn-empty load-btn" ng-click="loadMore('books')">Περισσότερα βιβλία</button> 
                    <div class="loader" ng-if="loading"><div class="loader-inner ball-clip-rotate"><div></div></div></div>
                </div>
            </div>
        </div>
        
        
        <div class="books-search-wrap feedlist-wrap" ng-if="filterTab=='search'">
            <ul class="list-unstyled row">
                <li class="col-md-12 col-sm-12 col-xs-12" 
                    ng-repeat="book in searchlist" 
                    ng-class="visibleView == 'table' ? 'table' : 'list'">
                    <a href="{{ url('secrets') }}/<% book.id %>" target="_self" class="cover-wrap feed-wrap">
                        <img alt="<% book.title %>" 
                        ng-src="{{ config('common.base_url') }}<% book.cover %>" 
                        err-SRC="http://dummyimage.com/100x150/858585/fff">
                    </a>
                
                    <div class="info-wrap feeds-info-wrap" ng-hide="visibleView == 'table'">
                        <h4> <a href="{{ url('secrets') }}/<% book.id %>" target="_self"><% book.title %></a> </h4>
                        <p ng-if="book.totalSecrets == 0"><em>Το συγκεκριμένο βιβλίο δεν περιέχει ακόμα κάποιο μυστικό.</em></p>
                        
                        <div class="collapse-con clearfix">
                            <div class="book-info">
                                <div>
                                    <div class="info-sec">
                                        <span>Συγγραφείς: </span> 
                                        <a href="{{ url('author') }}/<% author.id %>" ng-repeat="author in book.authors" target="_self"> <% author.name %>
                                            <span ng-if="!$last">,</span>
                                        </a>
                                    </div>
                                    <br>
                                    <div class="info-sec">
                                        <span> Κατηγορίες: </span> 
                                        <span ng-repeat="category in book.categories"> 
                                            <% category.category %><span ng-if="!$last">,</span>
                                        </span>
                                    </div>
                                </div>
                                <div> 
                                    <div class="info-sec">
                                        <span>Εκδότες: </span> 
                                        <a href="{{ url('publisher') }}/<% publisher.id %>" ng-repeat="publisher in book.publishers" target="_self"> <% publisher.name %>
                                            <span ng-if="!$last">,</span>
                                        </a>
                                    </div>
                                    <br>
                                    <div class="info-sec">
                                        <span> Έτος έκδοσης: </span> <span><% book.pubYear %></span>
                                    </div><br>
                                    <div class="info-sec">
                                        <span>Country: </span>
                                        <span>Greece</span>
                                    </div>
                                </div>
                            </div>
                        </div>   
                        <div class="info-actions">                                                                            
                            <button class="btn collapse-link" ng-show="book.desr && book.desr!=''" ng-click="isCollapsed = !isCollapsed">
                                <span>Περίληψη βιβλίου</span><i class="fa" ng-class="isCollapsed ? 'fa-caret-down' : 'fa-caret-up'"></i>
                            </button> 
                        </div>                    
                    </div>
                    <div class="actions-wrap" ng-if="visibleView != 'table'">
                        <a href="{{ url('secrets') }}/<% book.id %>" class="btn secret-btn" ng-class="book.totalSecrets == 0 ? 'gray' : ''" target="_self">
                            <span><% book.totalSecrets %></span>
                            Μυστικά
                        </a>
                        
                        <div>
                                
                            <a href="/secrets/add/<% book.isbn13 %>" target="_self" class="add-button">
                                <span>
                                Νέο Μυστικό
                                </span>
                            </a>     
                        </div>
                   
                    </div>
                    <div class="clear"></div>
                   
                    <div class="collapsed-book" ng-show="!isCollapsed && visibleView != 'table'">
                        <p ng-bind-html="renderHtml(book.desr)"></p>
                    </div>
                </li>
            </ul>
            
            <div class="row margin-top-40" ng-if="searchlist && moreSearch < totalSearch">
                <div class="col-xs-12 text-center">
                    <button class="btn btn-empty load-btn" ng-click="loadMore('search')">Περισσότερα βιβλία</button> 
                    <div class="loader" ng-if="loading">
                        <div class="loader-inner ball-clip-rotate">
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>
        
        </div>
            
    </div>
</section>

@endsection
    
