@extends('layout.master')

@section('title') Login | bookesecrets @stop 

@section('content')

<section id="user-wrapper" ng-controller="mainController" class="section">
    
    <div class="container">
        
        <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
            <main class="main" style="padding-left: 0; width: 100%;">
                <h1 class="title">Συνδεθείτε για να συνεχίσετε!</h1>   
                
                <div class="row">
                    <form angular-validator-submit="login()" name="loginForm" class="signupForm col-md-12 col-sm-12 col-xs-12" novalidate angular-validator>
                        <div class="form-group">
                            <label class="sr-only" for="loginemail">Διεύθυνση Email</label>
                            <input 
                              id="loginemail"
                              placeholder="Email"
                              ng-model="loginUser.email" 
                              class="form-control"
                              type = "email"
                              name = "email"
                              invalid-message="'Δεν έχετε δώσει πραγματικό email'"
                              required-message="'Το πεδίο είναι υποχρεωτικό'"
                              validate-on="dirty"
                              required style="width: 100%;"/>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="loginpass">Κωδικός</label>
                            <input 
                              id="loginpass"
                              placeholder="Κωδικός"
                              ng-model="loginUser.pwd" 
                              class="form-control" 
                              type="password" 
                              name = "password"
                              required-message="'Το πεδίο είναι υποχρεωτικό'"
                              validate-on="dirty"
                              required style="width: 100%;" />
                        </div>
                        <div class="forgot-wrap">
                            <a href="{{ url('user/forgot-password') }}" target="_self">Ξεχάσατε τον κωδικό σας;</a>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="1" name="remember" ng-model="loginUser.remember">
                                    <span>Να με θυμάσαι</span>
                                </label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg btn-block" ng-disabled="loginForm.$invalid">ΣΥΝΔΕΣΗ</button>
                        </div>
                        <span class="or">
                            <span>ή</span>
                        </span>
                        
                        @if ( session()->has('redirect') )
                            <input type="hidden" name="redirect" type="redirect" value="{{session('redirect')}}" id="redirect">
                            <a class="btn btn-facebook btn-block btn-lg" href="{{url('/user/facebooklogin'). '?' . http_build_query(['redirect'=>session('redirect')])}}">
                            Συνδεθειτε με facebook
                            </a>
                        @else
                            <a href="{{url('/user/facebooklogin')}}" class="btn btn-facebook btn-block btn-lg"> Συνδεθειτε με facebook </a>
                        @endif 
                        
    
                    </form>
                </div>
                
            </main>
        </div>

    </div>
    
</section>

<div ng-controller="mainController">
    @include('inc/footer')
</div>

	
@stop
