@extends('user.wrap')

@section('userChild')


    @extends('layout.master')

    @section('title') Forgot Password | bookesecrets @stop 

    @section('content')

    <section id="user-wrapper" ng-controller="UserController" class="section">
        
        <div class="container">
            
            <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
                <main class="main" style="padding-left: 0; width: 100%;">
                    <h1 class="title">Συμπληρώστε το email σας για να επαναφέρετε τον κωδικό πρόσβασης σας</h1>   
                    
                    <div class="row">
                        <form angular-validator-submit="forgotAccount()" name="forgotForm" class="signupForm col-md-12 col-sm-12 col-xs-12" novalidate angular-validator>
                            <div class="form-group">
                                <label class="sr-only" for="forgotemail">Διεύθυνση Email</label>
                                <input 
                                id="forgotemail"
                                placeholder="Email"
                                ng-model="forgotUser.email" 
                                class="form-control"
                                type = "email"
                                name = "email"
                                invalid-message="'Δεν έχετε δώσει πραγματικό email'"
                                required-message="'Το πεδίο είναι υποχρεωτικό'"
                                validate-on="dirty"
                                required style="width: 100%;"/>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block" ng-disabled="forgotForm.$invalid">ΑΠΟΣΤΟΛΗ</button>
                            </div>
        
                        </form>
                    </div>
                    
                </main>
            </div>

        </div>
        
    </section>

    <div ng-controller="mainController">
        @include('inc/footer')
    </div>

        
    @stop


@endsection
