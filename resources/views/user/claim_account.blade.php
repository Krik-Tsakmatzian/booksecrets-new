@extends('user.wrap')

@section('userChild')


    @extends('layout.master')

    @section('title') Claim Account | bookesecrets @stop 

    @section('content')

    <section id="user-wrapper" ng-controller="UserController" class="section">
        
        <div class="container">
            
            <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
                <main class="main" style="padding-left: 0; width: 100%;">
                    <h1 class="title">Συμπληρώστε την παρακάτω φόρμα για να διεκδικήσετε το λογαριασμό που επιθυμείτε</h1>   
                    
                    <div class="row">
                        <form angular-validator-submit="claimAccount()" name="claimForm" class="signupForm col-md-12 col-sm-12 col-xs-12" novalidate angular-validator>
                            <div class="form-group">
                                <label class="sr-only" for="claimname">Όνομα</label>
                                <input 
                                id="claimname"
                                placeholder="Όνομα"
                                ng-model="claimUser.firstname" 
                                class="form-control"
                                type = "text"
                                name = "first_name"
                                required-message="'Το πεδίο είναι υποχρεωτικό'"
                                validate-on="dirty"
                                required style="width: 100%;"
                                required/>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="claimlast">Επίθετο</label>
                                <input 
                                id="claimlast"
                                placeholder="Επίθετο"
                                ng-model="claimUser.lastname" 
                                class="form-control"
                                type = "text"
                                name = "last_name"
                                required-message="'Το πεδίο είναι υποχρεωτικό'"
                                validate-on="dirty"
                                required style="width: 100%;"
                                required/>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="claimemail">Διεύθυνση Email</label>
                                <input 
                                id="claimemail"
                                placeholder="Email"
                                ng-model="claimUser.email" 
                                class="form-control"
                                type = "email"
                                name = "email"
                                invalid-message="'Δεν είναι πραγματική διεύθυνση email'"
                                required-message="'Το πεδίο είναι υποχρεωτικό'"
                                validate-on="dirty"
                                required style="width: 100%;"/>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="claimnum">Αριθμός τηλεφώνου</label>
                                <input 
                                id="claimnum"
                                placeholder="Τηλέφωνο"
                                ng-model="claimUser.phone" 
                                class="form-control"
                                type = "number"
                                name = "tel"/>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="claimres">Λόγος</label>
                                <textarea 
                                    class="form-control" 
                                    rows="5"
                                    placeholder="Επιθυμώ τον συγκεκριμένο λογαριασμό διότι..."
                                    ng-model="claimUser.message" 
                                    required-message="'Το πεδίο είναι υποχρεωτικό'"
                                    validate-on="dirty"
                                    required style="width: 100%;" 
                                    id="claimres"></textarea>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block" ng-disabled="claimForm.$invalid">ΑΠΟΣΤΟΛΗ</button>
                            </div>
        
                        </form>
                    </div>
                    
                </main>
            </div>

        </div>
        
    </section>

    <div ng-controller="mainController">
        @include('inc/footer')
    </div>

        
    @stop

@endsection
