<!DOCTYPE html>
<html lang="el">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Επικοινωνία μέσω μηνύματος</h2>

		<div>
		    <p>Μήνυμα από: {{ $name }}, <a href="mailto:{{ $email }}">{{ $email }}</a></p>
		    @if ($phone && $phone!='')
		    <p>Τηλέφωνο: {{ $phone }}</p>
		    @endif
		    <p>
		    	Θέμα: <strong>{{ $subject }}</strong><br>
		    	Μήνυμα: {{ $messages }}
		    </p>
		</div>
	</body>
</html>