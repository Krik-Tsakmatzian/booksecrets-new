<!DOCTYPE html>
<html lang="el">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Επαναφορά κωδικού</h2>

		<div>
			Για να επαναφέρεις τον κωδικό σου, συμπλήρωσε αυτή την φόρμα: {{ url('password/reset', array($token)) }}.
		</div>
	</body>
</html>