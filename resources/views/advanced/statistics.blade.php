@extends('user.wrap')

@section('userChild')


  <section ng-controller="StatisticsController">
      
      <div class="main-wrap advanced-wrap statistics-wrap">
        
        <uib-tabset>
          <uib-tab heading="Βιβλία">
            
            <div class="clearfix"></div>
            
            <div class="row radio-wrap">
              <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-2 col-lg-10 col-lg-offset-2">
                <div class="radio">
                  <label><input ng-change="updateBookStats()" value="total" type="radio" name="books_stats" ng-model="stats.book"><span>Τίτλοι και αντίτυπα σε βιβλιοθήκες χρηστών</span></label>
                </div>
                <div class="radio">
                  <label><input ng-change="updateBookStats()" value="monthly" type="radio" name="books_stats" ng-model="stats.book"><span>Τίτλοι και αντίτυπα που προστίθενται ανά μήνα</span></label>
                </div>
              </div>
            </div>
            
            <canvas id="line" class="chart chart-line" chart-data="data"
                chart-labels="labels" chart-legend="true" chart-series="series"
                chart-click="onClick" >
            </canvas> 
            
            @if (session('role') != 'AUTHOR')
            <div class="radio-wrap-table radio">
              <label>
                <input type="radio" name="book_table" ng-model="stats.book_table" value="books" ng-change="bookTableUpdate()"><span>ΤΑ ΒΙΒΛΙΑ ΜΟΥ</span>
              </label>
              <label>
                <input type="radio" name="book_table" ng-model="stats.book_table" value="writers" ng-change="bookTableUpdate()"><span>ΣΥΓΓΡΑΦΕΙΣ</span>
              </label>
            </div>
            @else
            <h4 class="title">ΤΑ ΒΙΒΛΙΑ ΜΟΥ</h4>
            @endif
            
            <table class="table table-striped stats-table" ng-if="stats.book_table == 'books'">
              <thead>
                <tr>
                  <th class="col-xs-8">Βιβλία</th>
                  <th class="col-xs-4 text-center">Έχουν προστεθεί σε βιβλιοθήκες</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="book in books track by $index">
                  <td><a href="{{ url('secrets') }}/<% ids[$index] %>" target="_self"><% book %></a></td>
                  <td class="text-center"><% userscount[$index] %></td>
                </tr>
              </tbody>
            </table>
            
            @if (session('role') != 'AUTHOR')
            <table class="table table-striped stats-table" ng-if="stats.book_table == 'writers'">
              <thead>
                <tr>
                  <th class="col-xs-8">Συγγραφείς</th>
                  <th class="col-xs-4 text-center">Έχουν προστεθεί σε βιβλιοθήκες</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="author in authors track by $index">
                  <td><a href="{{ url('author') }}/<% authors_ids[$index] %>" target="_self"><% author %></a></td>
                  <td class="text-center"><% userscount[$index] %></td>
                </tr>
              </tbody>
            </table>
            @endif
            
            <ul uib-pagination 
              total-items="total" 
              ng-model="pagination.currentPage"
              max-size="5" 
              class="pagination-md" 
              boundary-link-numbers="true" 
              previous-text="&lsaquo;" next-text="&rsaquo;"
              rotate="false" ng-change="pageChanged()"></ul>
            <div class="clearfix"></div>
            
          </uib-tab>
        
          <uib-tab heading="Μυστικά">
            
            <div class="clearfix"></div>
            
            <h4 class="title">Πόσοι βλέπουν τα μυστικά μου</h4>
            
            <canvas id="line" class="chart chart-line" chart-data="secret_data"
                chart-labels="secret_labels" chart-legend="true" chart-series="secret_series"
                chart-click="onClick" >
            </canvas> 
            
            <h4 class="title">ΤΑ ΜΥΣΤΙΚΑ ΜΟΥ</h4>
            
            <table class="table table-striped stats-table">
              <thead>
                <tr>
                  <th class="col-xs-4">Μυστικά</th>
                  <th class="col-xs-4 text-center">Μοναδικές θεάσεις </th>
                  <th class="col-xs-4 text-center">Συνολικές θεάσεις </th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="secret in secrets track by $index">
                  <td><a href="{{ url('secrets') }}/<% secret_bookIds[$index] %>/<% secret_ids[$index] %>" target="_self"><% secret %></a></td>
                  <td class="text-center"><% secrets_uniqueViews[$index] %></td>
                  <td class="text-center"><% secrets_views[$index] %></td>
                </tr>
              </tbody>
            </table>
            
            <ul uib-pagination 
              total-items="secret_total" 
              ng-model="secret_pagination.currentPage"
              max-size="5" 
              class="pagination-md" 
              boundary-link-numbers="true" 
              previous-text="&lsaquo;" next-text="&rsaquo;"
              rotate="false" ng-change="secretpageChanged()"></ul>
            <div class="clearfix"></div>
            
          </uib-tab>
        
        </uib-tabset>
        
          
      </div>
      
  </section>


@endsection
