@extends('user.wrap')

@section('userChild')

<section ng-controller="AdvancedController">
    
    <div class="main-wrap advanced-wrap" ng-init="item = {{ $secretId }}">
        
        <div class="select-books section selected-section">
            <h3 class="title">ΕΠΙΛΕΓΜΕΝΑ ΒΙΒΛΙΑ (<% selectedBooks.length %>)</h3>  
            
            <ul class="list-unstyled books-list row" ng-if="selectedBooks">
                <li class="col-md-2" ng-repeat="book in selectedBooks">
                    <div>
                        <img width='140' height='200' ng-src="{{ config('common.base_url') }}<% book.cover %>" err-SRC="{{ asset('dist/img/bookimage.jpg') }}">
                    </div>
                </li>
            </ul>
        </div>
        
        <div class="select-books section">
            <h3 class="title">ΠΡΟΣΘΗΚΗ ΜΥΣΤΙΚΟΥ</h3>
            
            <form ng-submit="postSecret()" novalidate>
                <div class="advanced-form">
                    <div class="form-group">
                        <input type="text" ng-init="title = '{{ $secret->title }}'" class="form-control" name="title" ng-model="title" id="title" placeholder="Εισάγετε τίτλο" required/>
                    </div>
                    
                    <textarea cols="100" rows="20" ng-model="text" placeholder="Γράψτε κάτι..." id="textaa" required redactor>{{ $secret->text }}</textarea>
                </div>
                
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="locked" value="{{{ $secret->locked == 1 ? 1 : 0 }}}" ng-model="locked" {{{ $secret->locked == 1 ? 'checked' : '' }}}>
                        <span>Εδώ μπορείς να κλειδώσεις το μυστικό.Κλειδωμένο μυστικό σημαίνει πως δεν μπορούν να το δουν οι χρήστες εαν πρώτα δεν αναζητήσουν το ISBN του/των βιβλίων σου.</span>
                    </label>
                </div>
                
                @if ( $secret->status == 1 || $secret->status == 2 )
                <div class="form-group text-right row margin-top-40" ng-init="status=true">
                    <div class="col-lg-4 text-right margin-top-10">
                @else
                <div class="form-group text-right row margin-top-40" ng-init="status=false">
                    <div class="col-lg-4 text-right margin-top-10">
                        <button class="btn-empty btn btn-danger" ng-click="secretDelete(item)"><i class="fa fa-trash-o"></i></button>
                @endif
                        <switcher class="styled"
                                  ng-model="status"
                                  ng-change="onChange(newValue, oldValue)"
                                  true-label="Δημοσίευση"
                                  false-label="Πρόχειρο">
                        </switcher>
                    </div>
                    
                    @if ( $secret->status == 2 )
                    <div class="col-lg-5 schedule_wrap" ng-hide="status == false" ng-init="setDate('{{ $secret->startDate }}'); schedule='later'">
                    @else
                    <div class="col-lg-5 schedule_wrap" ng-hide="status == false" ng-init="schedule='now'">
                    @endif
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="radio">
                                    <label class="radio-inline" ng-class="{dis: schedule=='later'}">
                                        <input type="radio" value="now" name="schedule" ng-model="schedule">
                                        <span>Τώρα</span>
                                    </label>
                                    <label class="radio-inline" ng-class="{dis: schedule=='now'}">
                                        <input type="radio" value="later" name="schedule" ng-model="schedule">
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            
                            <div class="col-lg-8" ng-class="{dis: schedule=='now'}">
                                <p class="input-group datetimepicker-wrapper">
                                    <input type="text" placeholder="Προγραμματισμένα" ng-click="openCalendar($event, prop)" class="form-control" timepicker-options="{'show-meridian': false}" datetime-picker="MMM d, y HH:mm" datepicker-options="dateOptions" ng-model="myDate.date" is-open="isOpen"/>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" ng-click="openCalendar($event, prop, 'btn')"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 text-right pull-right">
                        <button class="btn btn-default btn-lg" type="submit">ΑΠΟΘΗΚΕΥΣΗ</button>
                    </div>
                </div>
            </form>
            
        </div>
          
    </div>
    
</section>
    
@endsection