@extends('user.wrap')

@section('userChild')

<section ng-controller="AdvancedController">
    @if ( session('role') == 'PUBLISHER' )
        <div class="filter-wrap">
            <form id="filters">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="sr-only" for="search">Αναζήτηση</label>
                        <div class="input-group search-group">
                            
                            <input type="text" class="form-control" id="search" placeholder="Αναζήτηση τίτλου βιβλίου" ng-model="search.title">
                            <div class="input-group-addon">
                                <div class="loader search-loading" ng-if="search_loading"><div class="loader-inner ball-clip-rotate"><div></div></div></div>
                                <button type="button" class="btn btn-default" ng-click="bookSearch()"><i class="fa fa-search"></i></button>
                            </div>
                            
                            <div class="input-group-addon filters-collapse" ng-if="!hideSearch">
                                <button type="button" class="btn btn-default" ng-click="showFilters()">
                                    Εργαλεια <i class="fa fa-caret-down"></i></button>
                            </div>
                        </div>
                    </div>
                    
                    <div ng-show="filters" ng-if="!hideSearch" class="col-md-12 more-filters publisher">
                        
                        <div class="form-horizontal">
                              
                            <div class="form-group">
                                <label class="control-label col-md-2" for="author">Συγγραφέας</label>
                                <input type="text" class="form-control col-md-4" id="author" placeholder="Συγγραφέας" ng-model="search.author">
                            
                                <!--<label class="control-label col-md-2" for="category">Κατηγορία</label>-->
                                <!--<input type="text" class="form-control col-md-4" id="category" placeholder="Κατηγορία" ng-model="search.category">-->
                                <label class="control-label col-md-2" for="year">Έτος</label>
                                <input type="number" class="form-control col-md-4" id="year" placeholder="Έτος" ng-model="search.year">
                            </div>
                              
                            <div class="form-group">
                                
                                <label class="control-label col-md-2" for="isbn">ISBN</label>
                                <input type="text" class="form-control col-md-4" id="isbn" placeholder="ISBN" ng-model="search.isbn">
                            </div>   
                            
                            <div class="form-group text-right">
                                <button class="btn btn-primary" ng-click="bookSearch()">Αναζητηση βιβλιων</button>
                            </div>
                        
                        </div>
                    
                    </div>
                  
                </div>
            </form>
        </div>
    @endif
    
    <div class="main-wrap advanced-wrap">
        
        @if ( session('role') == 'AUTHOR' )
        <div class="select-books section" ng-init="role = 'author'">
            <h3 class="title">ΕΠΙΛΕΞΤΕ ΒΙΒΛΙΑ (<% booksSecret.length %>) 
                <div class="checkbox pull-right" ng-if="booksFound">
                    <label>
                        <input type="checkbox" id="selectAll" ng-click="selectAll()">
                        <span>Επιλογή όλων</span>
                    </label>
                </div>
            </h3>
            <div class="clearfix"></div>
            <div class="loader text-center" ng-if="booksLoading">
                <div class="loader-inner ball-clip-rotate">
                    <div></div>
                </div>
            </div>
        @elseif ( session('role') == 'PUBLISHER' )
        <div class="select-books section"  ng-init="role = 'publisher'">
            <h3 class="title">ΕΠΙΛΕΞΤΕ ΒΙΒΛΙΑ
                <div class="checkbox pull-right" ng-if="booksFound">
                    <label>
                        <input type="checkbox" id="selectAll" ng-click="selectAll()">
                        <span>Επιλογή όλων</span>
                    </label>
                </div>
            </h3>
            <div class="clearfix"></div>
            <div class="loader text-center" ng-if="booksLoading">
                <div class="loader-inner ball-clip-rotate">
                    <div></div>
                </div>
            </div>
            <p ng-if="!booksFound" ng-if="!booksLoading">Χρησιμοποιείστε την αναζήτηση βιβλίων για να βρείτε τα βιβλία που αφορά το μυστικό.</p>
        @else
        <div class="select-books section"  ng-init="role = 'mobuser'">           
        @endif
            
        @if ( session('role') == 'AUTHOR' )
            <ul class="list-unstyled books-list row author-select" ng-if="booksFound">
                <li class="col-md-3" ng-repeat="book in booksFound"
                    ng-click="selectBook(book.id, $index, book)" 
                    ng-class="{ 'selected': booksSecret.indexOf(book.id) > -1 }"
                    uib-tooltip="<% book.publishers[0].name %>">
                    <div>
                        <img width='140' height='200' ng-src="{{ config('common.base_url') }}<% book.cover %>" err-SRC="{{ asset('dist/img/bookimage.jpg') }}">
                    </div>
                </li>
            </ul>
        @elseif ( session('role') == 'PUBLISHER' )
            <ul class="list-unstyled books-list row author-select" ng-if="booksFound" when-scrolled="pubLoadMore()">
                <li class="col-md-3" ng-repeat="book in booksFound"
                    ng-click="selectBook(book.id, $index, book)" 
                    ng-class="{ 'selected': booksSecret.indexOf(book.id) > -1 }"
                    uib-tooltip="<% book.authors[0].name %>">
                    <div>
                        <img width='140' height='200' ng-src="{{ config('common.base_url') }}<% book.cover %>" err-SRC="{{ asset('dist/img/bookimage.jpg') }}">
                    </div>
                </li>
            </ul>
            <div class="loader text-center pub-loader" ng-if="loadingMore"><div class="loader-inner ball-clip-rotate"><div></div></div></div>
        @endif
        </div>
        
        @if ( session('role') == 'PUBLISHER' )
        <div class="select-books section selected-section">
            <h3 class="title">ΕΠΙΛΕΓΜΕΝΑ ΒΙΒΛΙΑ (<% selectedBooks.length %>)
                <a href="" class="pull-right action-link" ng-if="selectedBooks.length != 0" ng-click="unselectAll()"><em>Αφαίρεση όλων</em></a>
            </h3>  
            
            <div class="clearfix"></div>
            
            <ul class="list-unstyled books-list row" ng-if="selectedBooks">
                <li class="col-md-2" ng-repeat="book in selectedBooks">
                    <div>
                        <img width='140' height='200' ng-src="{{ config('common.base_url') }}<% book.cover %>" err-SRC="{{ asset('dist/img/bookimage.jpg') }}">
                        <button class="btn btn-empty btn-block" ng-click="selectBook(book.id, $index, book)"><i class="fa fa-times"></i></button>
                    </div>
                </li>
            </ul>
        </div>
        @elseif ( session('role') == 'MOBUSER' )
            {{-- @php
                var_dump($book);
            @endphp --}}
        
        <div class="select-books section selected-section" ng-init="role = 'mobuser'; book = {{ json_encode($book, TRUE) }}">
            <h3 class="title">1 ΕΠΙΛΕΓΜΕΝO ΒΙΒΛΙO - <b>{{ $book->title}}</b></h3>              
            <div class="clearfix"></div>            
            <ul class="list-unstyled books-list row" > 
                <li class="col-md-2">                    
                    <img style="max-height: 200px" src="{{ config('common.base_url') }}{{ $book->cover}}" err-SRC="{{ asset('dist/img/bookimage.jpg') }}">   
                </li>
            </ul>
        </div>
        @endif
        
        <div class="select-books section">
            <h3 class="title">ΠΡΟΣΘΗΚΗ ΜΥΣΤΙΚΟΥ</h3>
            
            <form ng-submit="postSecret()" novalidate>
                <div class="advanced-form">
                    <div class="form-group">
                        <input type="text" class="form-control" name="title" ng-model="title" placeholder="Εισάγετε τίτλο" id="title" required/>
                    </div>
                    
                    <textarea cols="100" rows="20" redactor ng-model="text" placeholder="Γράψτε κάτι..." id="textaa" required></textarea>
                </div>
                
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="locked" value="0" ng-model="locked">
                        <span>Εδώ μπορείς να κλειδώσεις το μυστικό.Κλειδωμένο μυστικό σημαίνει πως δεν μπορούν να το δουν οι χρήστες εαν πρώτα δεν αναζητήσουν το ISBN του/των βιβλίων σου.</span>
                    </label>
                </div>
                
                <div class="form-group row margin-top-40">
                    <div class="col-lg-4 text-right margin-top-10">
                        <switcher class="styled"
                                  ng-model="status"
                                  ng-change="onChange(newValue, oldValue)"
                                  true-label="Δημοσίευση"
                                  false-label="Πρόχειρο">
                        </switcher>
                    </div>
                    
                    <div class="col-lg-5 schedule_wrap" ng-hide="status == false">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="radio" ng-init="schedule='now'">
                                    <label class="radio-inline" ng-class="{dis: schedule=='later'}">
                                        <input type="radio" value="now" name="schedule" ng-model="schedule">
                                        <span>Τώρα</span>
                                    </label>
                                    <label class="radio-inline" ng-class="{dis: schedule=='now'}">
                                        <input type="radio" value="later" name="schedule" ng-model="schedule">
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            
                            <div class="col-lg-8" ng-class="{dis: schedule=='now'}">
                                <p class="input-group datetimepicker-wrapper">
                                    <input type="text" placeholder="Προγραμματισμένα" ng-click="openCalendar($event, prop)" class="form-control" timepicker-options="{'show-meridian': false}" datetime-picker="MMM d, y HH:mm" datepicker-options="dateOptions" ng-model="myDate.date" is-open="isOpen"/>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" ng-click="openCalendar($event, prop, 'btn')"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 text-right pull-right">
                        <button class="btn btn-default btn-lg" type="submit">ΑΠΟΘΗΚΕΥΣΗ</button>
                    </div>
                </div>

            </form>
            
        </div>
          
    </div>
    
</section>

@endsection