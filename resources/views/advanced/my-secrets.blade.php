@extends('user.wrap')

@section('userChild')

<section ng-controller="MySecretsController">
    <div class="filter-wrap feed-filters">
        <div class="form-inline">
            <div class="row">
                {{-- <div class="col-md-12">
                    <p>Ανάζητηστε βιβλία από την <a href="{{ url('user/library') }}" target="_self"><b>βιβλιοθήκη σας</b></a> και αποκαλύψτε τα μυστικά τους!</p>
                </div> --}}
                <div class="col-md-12 custom-filters">
                    <span class="label">ΦΙΛΤΡΑΡΙΣΜΑ: </span>
                    <div class="btn filter-btn" ng-class="order_item == 'createDate' ? 'active' : ''">
                        <button class="btn btn-empty" ng-click="sort()">ΜΕ ΗΜΕΡΟΜΗΝΙΑ <i class="fa" ng-class="order == 'asc' ? 'fa-caret-up' : 'fa-caret-down'"></i></button>
                    </div>

                    <div class="pull-right">
                        <span class="label">ΦΙΛΤΡΑΡΙΣΜΑ ΜΥΣΤΙΚΩΝ: </span>
                        <button class="btn filter-btn filter-tab all-tab" ng-click="showType('all')" ng-class="chosenType == 'all' ? 'active' : ''">ΟΛΑ</button>
                        <button class="btn filter-btn filter-tab published-tab" ng-click="showType('published')" ng-class="chosenType == 'published' ? 'active' : ''">ΔΗΜΟΣΙΕΥΜΕΝΑ</button>
                        <button class="btn filter-btn filter-tab draft-tab" ng-click="showType('draft')" ng-class="chosenType == 'draft' ? 'active' : ''">ΠΡΟΧΕΙΡΑ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="main-wrap">
        <div class="feedlist-wrap">
            <ul class="list-unstyled row" ng-if="secretslist && secretslist.length > 0">
                <li class="col-md-12 col-sm-12 col-xs-12" ng-repeat="secret in secretslist" ng-if="secret.status != 'deleted'">
                    <a href="{{ url('secrets') }}/<% secret.book.id %>" target="_self" class="cover-wrap feed-wrap">
                        <div>
                            <img width='140' height='200' 
                            ng-src="{{ config('common.base_url') }}<% secret.book.cover %>" err-SRC="{{ asset('dist/img/bookimage.jpg') }}">
                        </div>
                    </a>
                    <div class="info-wrap feeds-info-wrap">
                        <h4> 
                            <a href="{{ url('secrets') }}/<% secret.book.id %>/<% secret.id %>" target="_self"><% secret.title %></a> 
                            <span class="label draft" ng-if="secret.status == 0">πρόχειρο</span>
                            <span class="label published" ng-if="(secret.status == 1) || (secret.status == 2 && secret.scheduledPublished == true)">δημοσιευμένο</span>
                            <span class="label sheduled" ng-if="secret.status == 2 && secret.scheduledPublished == false">εκκρεμεί</span>
                            
                            <a href="{{ url('secrets/edit') }}/<% secret.book.id %>/<% secret.id %>" target="_self" class="pull-right"><i class="fa fa-pencil-square-o"></i></a>
                            <button ng-if="secret.status == 0" class="btn-empty btn pull-right btn-danger" ng-click="secretDelete(secret)"><i class="fa fa-trash-o"></i></button>
                        </h4>
                        <div class="clearfix"></div>
                        <p><% secret.text | htmlToPlaintext | limitTo:100 %>...</p>                                               
                        
                        <div class="info-actions">
                            <a  
                                tabindex="0"
                                class="author red-border btn btn-empty" 
                                ng-if="secret.userByCreatedByUserId.role === 'AUTHOR'" 
                                >ΣΥΓΓΡΑΦΕΙΣ</a>                           
                            <a  
                                tabindex="0"
                                class="author purple-border btn btn-empty" 
                                ng-if="secret.userByCreatedByUserId.role === 'PUBLISHER'" 
                                >ΕΚΔΟΤΕΣ</a>
                            <a  
                                tabindex="0"
                                class="author orange-border btn btn-empty" 
                                ng-if="secret.userByCreatedByUserId.role === 'MOBUSER'" 
                                ><% secret.userByCreatedByUserId.name %></a>
                            <a  
                                tabindex="0"
                                class="author blue-border btn btn-empty" 
                                ng-if="secret.userByCreatedByUserId.role === 'SUPERADMIN'" 
                                >BOOKSECRETS</a>
    
                            <a href="/secrets/<% secret.book.id %>/<% secret.id %>" ng-if="secret.booksCount > 1" target="_self">
                                Βιβλία με το ίδιο μυστικό</a>
    
                            <button class="btn collapse-link" ng-click="isCollapsed = redirectToSecret(secret, isCollapsed)">
                                <span ng-if="isCollapsed" class="load-more">Περισσότερα...</span>
                                <i ng-if="secret.locked == 1" class="icon-locked"></i>
                                <i ng-if="secret.hasImage == true" class="icon-photo"></i>
                                <i ng-if="secret.hasAudio == true" class="icon-voice"></i>
                                <i ng-if="secret.hasVideo == true" class="icon-camera"></i>
                                <i class="fa fa-caret-right"></i>
                        </div>
                    </div>
                </li>
            </ul>
            
            
            <div class="row margin-top-40" ng-if="!secretslist && loading">
                <div class="col-xs-12 text-center">
                    <div class="loader" ng-if="loading">
                        <div class="loader-inner ball-clip-rotate">
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>

            <h4 ng-if="!loading && (!secretslist  || secretslist.length === 0)" class="center padding-top-50" style="color: #848484;">Δεν έχετε προσθέσει ακόμα μυστικό σε κάποιο βιβλίο</h4>
            
            <div class="row margin-top-40" ng-if="secretslist && moreFeeds < total">
                <div class="col-xs-12 text-center">
                    <button class="btn btn-empty load-btn" ng-click="loadMore()">Περισσότερα μυστικά</button> 
                    <div class="loader" ng-if="loading"><div class="loader-inner ball-clip-rotate"><div></div></div></div>
                </div>
            </div>
            
        </div>
    </div>
</section>

@endsection