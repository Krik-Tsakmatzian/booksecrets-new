@extends('user.wrap')

@section('userChild')

<h2>Προφίλ συγγραφέα</h2>
<section id="wrapper" class="main-wrapper" ng-controller="AuthorController">
    
        
    <div class="row main-wrap profile-wrap">
        
        <div class="col-md-4 col-sm-4 col-xs-12">

            <div class="img-wrap">
                
                @if ( isset($data->userByUserId->profileImage) && ( $data->userByUserId->profileImage != null || $data->userByUserId->profileImage != '' ) )
                    @if ( isset($data->userByUserId->profileImage->filePath) )
                     {{-- class="rounded" --}}
                        <div style="background-image: url('{{config('common.base_url')}}{{$data->userByUserId->profileImage->filePath}}')">
                            <img class="img-responsive" width="200" height="200" id="avatar" src="{{ config('common.base_url')}}{{$data->userByUserId->profileImage->filePath}}">
                        </div>
                    @else
                    {{-- class="rounded" --}}
                        <div style="background-image: url('{{ asset('dist/img/author.jpg') }}')">
                            <img class="img-responsive" width="200" height="200" id="avatar" src="{{ asset('dist/img/author.jpg') }}">
                        </div>
                    @endif
                @else
                    {{-- class="rounded" --}}
                    <div style="background-image: url('{{ asset('dist/img/author.jpg') }}')">
                        <img class="img-responsive" width="200" height="200" id="avatar" src="{{ asset('dist/img/author.jpg') }}">
                    </div>
                @endif

            </div>
            
            <h1 class="title center">{{ $data->name or '' }}</h1>
            
        </div>
        
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="text-preview slide-right">
                
                <div class="text-summary" ng-show="!edit">
                    
                    @if ( isset($data->userByUserId) && isset($data->userByUserId->summary) )
                        @if ( $data->userByUserId->summary != null && $data->userByUserId->summary != '' )
                            <p>{{ $data->userByUserId->summary }}</p>
                        @endif
                    @else
                        <p>Δεν υπάρχει διαθέσιμη περιγραφή.</p>
                    @endif

                </div>
        
            </div>
            
            <div class="social-preview slide-right">
                <ul class="list-unstyled list-inline" ng-show="!editSocial">
                    @if ( isset($data->userByUserId->media) )
                        @foreach ($data->userByUserId->media as $media)
                            @if ($media->type == 'FACEBOOK')
                                <li><a href="{{{ strpos($media->value, 'http://') === false && strpos($media->value, 'https://') === false ? 'http://'.$media->value : $media->value }}}" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a></li>
                            @elseif ($media->type == 'TWITTER')
                                <li><a href="{{{ strpos($media->value, 'http://') === false && strpos($media->value, 'https://') === false ? 'http://'.$media->value : $media->value }}}" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>
                            @elseif ($media->type == 'WEBSITE')
                                <li><a href="{{{ strpos($media->value, 'http://') === false && strpos($media->value, 'https://') === false ? 'http://'.$media->value : $media->value }}}" target="_blank"><i class="fa fa-globe"></i></a></li>
                            @elseif ($media->type == 'YOUTUBE')
                                <li><a href="{{{ strpos($media->value, 'http://') === false && strpos($media->value, 'https://') === false ? 'http://'.$media->value : $media->value }}}" target="_blank" class="youtube"><i class="fa fa-youtube"></i></a></li>
                            @endif
                        @endforeach
                    @endif
                </ul>
                
            </div>
            
        </div>
        
        @if ( isset($secrets) && count($secrets)>0 )       
            <div class="col-md-12 col-sm-12 col-xs-12">
            <h3 class="title"><b>Μυστικά</b> - Τα τελευταία {{ count($secrets) }}</h3>  
                <div class="books-carousel profile-carousel base-carousel">
                    @foreach ( $secrets as $sec )
                        <div class="item">
                            @if ( file_get_contents(config('common.base_url').$sec->book->cover) )
                                <img width='200' height='200' src="{{ config('common.base_url') }}{{ $sec->book->cover }}">
                            @else
                                <img width='200' height='200' src="{{ asset('dist/img/bookimage.jpg') }}">
                            @endif
                            <span onclick="location.href = '{{ url('secrets') }}/{{ $sec->book->id }}/{{ $sec->id }}';" class="caption">{{ $sec->title }}</span>
                        </div>
                    @endforeach
                </div>
            
            </div>
        @endif
        
    </div>
        
        
    

</section>

@endsection
