<?php

namespace App\Http\Middleware;

use \Illuminate\Http\Request;
use Closure;

class UserTypeCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( $request->hasCookie('cookie_token') && !session()->has('token') ){
            $response = (array)ApiCalls::post('user/view', ['token' => $request->cookie('cookie_token')]);
            
            if ( isset($response['data']->username) && $response['data']->username != '' ){
                $name = $response['data']->username;
            }
            elseif ( $response['data']->role == 'AUTHOR' && isset($response['data']->author) && isset($response['data']->author->name) ){
                $name = $response['data']->author->name;
            }
            elseif ( $response['data']->role == 'PUBLISHER' && isset($response['data']->publisher) && isset($response['data']->publisher->name) ){
                $name = $response['data']->publisher->name;
            }
            else{
                $name = '';	
            }
            session(['name' => $name]);
            session(['token' => $response['data']->token]);
            session(['role' => $response['data']->role]);
            
            if ( $response['data']->role == 'PUBLISHER' ){
                if (isset($response['data']->publisher->id)){
                    session(['id' => $response['data']->publisher->id]);
                }
            }
            elseif ( $response['data']->role == 'AUTHOR' ){
                if (isset($response['data']->author->id)){           
                    session(['id' => $response['data']->author->id]);
                }
            }else {
                session(['id' => $response['data']->id]);
            }
        }

        return $next($request);
    }
}
