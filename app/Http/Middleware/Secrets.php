<?php

namespace App\Http\Middleware;

use Closure;
use App\Classes\ApiCalls;
use Illuminate\Http\Request;
use Route;

class Secrets
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {              
        $routeParamters = $request->route()->parameters();

        // $this->routeParamters = Route::current()->parameters();
        // return dd(Route::current());  

        if(! session()->has('token') && ! $request->hasCookie('cookie_token')){
            if ( !isset($routeParamters['secret']) && !isset($routeParamters['id']) ){
                //if not secret page              
                return redirect('/user/login')->with('redirect', $request->fullUrl());
            }
        }            

        return $next($request);
    }
}
