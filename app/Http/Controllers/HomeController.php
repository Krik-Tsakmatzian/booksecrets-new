<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use App\Classes\ApiCalls;

class HomeController extends Controller {	

	public function getIndex()
	{
		$posts = json_decode(file_get_contents(config('common.blog_url').'cockpit/index.php/rest/api/collections/get/Blog?token=52c26d9be13df9ce2d7b8a1d&limit=4&sort[date]=-1'));
		
		if ($posts){
			foreach ($posts as $k=>$post){
				$img = file_get_contents(config('common.blog_url').'cockpit/index.php/rest/api/mediamanager/thumbnails?token=52c26d9be13df9ce2d7b8a1d&images[]='.$post->image.'&w=170&h:220&options[quality]=100&options[mode]=crop&options[base64]=0');
				
				$img = json_decode($img);
				
				$value = get_object_vars($img);
				$post->image_cropped = $value;
			}
			
			View::share('posts', $posts);
		}
		
		return view('index');
	}
	
	public function getTerms()
	{
		return view('common.terms');
	}
	
	public function getWhoWeAre()
	{
		return view('common.who');
	}
	
	public function getWhoWeAreEN()
	{
		return view('common.who-en');
	}
	
	public function getVisitors()
	{
		return view('common.visitors');
	}
	
	public function getAuthors()
	{
		return view('common.authors');
	}
	
	public function getPublishers()
	{
		return view('common.publishers');
	}
	
	public function postContactForm(Request $request)
	{
		$data = $request->all();
		$response = (array)ApiCalls::post('contact/send', $data);
		
		return $response;
	}
	
}