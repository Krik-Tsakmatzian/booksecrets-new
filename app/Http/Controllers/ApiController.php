<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Classes\ApiCalls;
use Embed\Embed;
use Image;
use File;
use DateTime;


class ApiController extends Controller {
	
	public function postSignUp(Request $request)
	{
		$response = (array)ApiCalls::post('user/register', $request->all());
		
		if ( $response['success'] == true ){
			
			if ( isset($response['data']->username) && $response['data']->username != '' ){
				$name = $response['data']->username;
			}
			elseif ( $response['data']->role == 'AUTHOR' && isset($response['data']->author) && isset($response['data']->author->name) ){
				$name = $response['data']->author->name;
			}
			elseif ( $response['data']->role == 'PUBLISHER' && isset($response['data']->publisher) && isset($response['data']->publisher->name) ){
				$name = $response['data']->publisher->name;
			}
			else{
				$name = '';	
			}
			
			session(['name' => $name]);
			session(['token' => $response['data']->token]);			
			session(['role' => $response['data']->role]);
			
			if ( $response['data']->role == 'PUBLISHER' ){
				if (isset($response['data']->publisher->id)){
					session([ 'id' => $response['data']->publisher->id ]);
				}
			}
			elseif ( $response['data']->role == 'AUTHOR' ){
				if (isset($response['data']->author->id)){
					session(['id' => $response['data']->author->id]);			
				}
			}
			else {
				session(['id' => $response['data']->id]);	
			}
		}
		
		return $response;
	}
	
	public function postLogin(Request $request)
	{
		$attributes = $request->except('remember');
		
		$response = (array)ApiCalls::post('user/login', $attributes);
		
		if ( $response['success'] == true ){

			if ( isset($response['data']->username) && $response['data']->username != '' ){
				$name = $response['data']->username;
			}
			elseif ( $response['data']->role == 'AUTHOR' && isset($response['data']->author) && isset($response['data']->author->name) ){
				$name = $response['data']->author->name;
			}
			elseif ( $response['data']->role == 'PUBLISHER' && isset($response['data']->publisher) && isset($response['data']->publisher->name) ){
				$name = $response['data']->publisher->name;
			}
			else{
				$name = $response['data']->name;
			}	

			session(['token'=>$response['data']->token, 'name'=>$name, 'krik'=>$response['data'], 'role'=>$response['data']->role]);
			// $request->session()->put('name', $name);
			// $request->session()->put('token', $response['data']->token);
			// $request->session()->put('role', $response['data']->role);

			
			if ( $response['data']->role == 'PUBLISHER' ){
				if (isset($response['data']->publisher->id)){			
					session(['id'=>$response['data']->publisher->id]);
				}
			}
			elseif ( $response['data']->role == 'AUTHOR' ){
				if (isset($response['data']->author->id)){				
					// $request->session()->put('id', $response['data']->author->id);
					session(['id'=>$response['data']->author->id]);
				}
			}else {
				$request->session()->put('id', $response['data']->id);
				session(['id'=>$response['data']->id]);
			}
			
			
			if ($request->input('remember')  === 'true' ){
				$cookie = cookie()->forever('cookie_token', $response['data']->token);
				$response = response()->json($response);
                $response->headers->setCookie($cookie);
			}
						
		}
		
		return $response;
		
	}
	
	public function postFeedlist(Request $request)
	{
		$attributes = $request->all();
		
		if ($request->hasCookie('cookie_token')){
			$attributes['token'] = $request->cookie('cookie_token');
		}else{
			$attributes['token'] = session('token');
		}
		
		$response = (array)ApiCalls::post('feed/list', $attributes);
		
		return $response;
	}
	
	public function postFeedsearch(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		
		if ($request->hasCookie('cookie_token')){
			$attributes['token'] = $request->cookie('cookie_token');
		}else{
			$attributes['token'] = session('token');
		}
		
		$response = (array)ApiCalls::post('feed/search', $attributes);
		
		return $response;
	}
	
	public function postBookslist(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('user/books/list', $attributes);
		
		return $response;
	}
	
	public function postFavorites(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('user/favorites', $attributes);
		
		return $response;
	}
	
	public function postFeedBooklist(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('feed/item/books/list', $attributes);
		
		return $response;
	}
	
	public function postMySecrets(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('user/secrets/list', $attributes);
		
		return $response;
	}
	
	public function postBookview(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('user/books/view', $attributes);
		
		return $response;
	}
	
	public function postSecretView(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('secret/view', $attributes);
		
		return $response;
	}
	
	public function postEditFavorite(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('secret/favorite/edit', $attributes);
		
		return $response;
	}
	
	public function postSecretRate(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('secret/rating/edit', $attributes);
		
		return $response;
	}
	
	public function postSecretReport(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('secret/flag/edit', $attributes);
		
		return $response;
	}
	
	public function postSecretDelete(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('secret/delete', $attributes);
		
		return $response;
	}
	
	public function postSecretPost(Request $request)
	{
		$attributes = $request->except('bookId', 'locked');
		$attributes['bookId[]'] = implode(",", $request->input('bookId'));
		$attributes['locked'] = ( $request->input('locked') == 'false' ? 0 : 1 );
		$attributes['token'] = session('token');
		
		if ($request->has('startDate')){
			if ($request->input('startDate') == '' || $request->input('startDate') == null) {
				unset($attributes['startDate']);
			}else {
				$newdate = preg_replace("/\((.*?)\)/", "", $request->input('startDate'));
				$date = new DateTime($newdate);
				$attributes['startDate'] = $date->format(DateTime::ISO8601);
			}
		}
		
		if ($request->has('id')){
			$response = (array)ApiCalls::post('secret/edit', $attributes);
			return $response;
		}
		
		$response = (array)ApiCalls::post('secret/add', $attributes);

		return $response;
	}
	
	public function postLibraryAdd(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('user/books/add', $attributes);
		
		return $response;
	}
	
	public function postLibraryRemove(Request $request)
	{
		$response = (array)ApiCalls::post('user/books/delete', ['token' => session('token'), 'bookId[]' => $request->input('bookId')]);
		
		return $response;
	}
	
	public function postBookUpdate(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('user/books/update', $attributes);
		
		return $response;
	}
	
	public function postEmbed(Request $request)
	{
		$url = $request->input('video');				

		try {
			$info = Embed::create($url);
		} catch (\Exception $error) {			
			$info = null;
		}
		
		if ($info == '' || $info == null || json_encode($info) == false){
			$status = 0;
			$data = 'Not valid url';
		} else {
			$status = 1;
			$data = $info->code;
		}
		
		return response()->json([		
			'status' => $status == 1 ? 'success' : 'error',
			'data' => $data,
			'provider'=> $info->providerName
		]);
	}
	
	public function postProfileUpdate(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		// Log::info($attributes);
		
		$response = (array)ApiCalls::post('user/edit', $attributes);
		
		// Log::info($response);
		
		return $response;
	}
	
	public function postProfileInfo(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		// $attributes['token'] = Session::get('token');
		switch (session('role')) {
			case 'PUBLISHER':
				$response = (array)ApiCalls::post('publisher/view', $attributes);
				break;
			case 'AUTHOR':
				$response = (array)ApiCalls::post('author/view', $attributes);
				break;
			case 'MOBUSER':
				$response = (array)ApiCalls::post('user/viewinfo', $attributes);
				break;
		}		
		
		return $response;
	}
	
	public function postValidate(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		
		$response = (array)ApiCalls::post('user/check', $attributes);
		
		return $response;
	}
	
	public function postValidateBook(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		
		$response = (array)ApiCalls::post('book/search', $attributes);
		
		return $response;
	}
	
	public function postUserBookSearch(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		foreach ($attributes as $key=>$attr){
			if ($attr == '') {
				unset($attributes[$key]);
			}
		}
		
		if (!isset($attributes['title'])){
			$attributes['title'] = "";
		}
		
		$response = (array)ApiCalls::post('user/books/search', $attributes);
		
		return $response;
	}
	
	public function postAdvBookSearch(Request $request)
	{
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		foreach ($attributes as $key=>$attr){
			if ($attr == '') {
				unset($attributes[$key]);
			}
		}
		
		// if (!isset($attributes['title'])) {
		// 	$attributes['title'] = '';
		// }


		
		$response = (array)ApiCalls::post('secret/books/search', $attributes);
		
		return $response;
	}
	
	public function postUpload(Request $request)
	{
		$attributes['token'] = session('token');
		$file = $request->file('file');
		
	    $url=url('/');
	    
		$mime = $file->getMimeType();
		
	    $allowedMimes = [
			'image/gif',
			'image/jpg',
			'image/jpeg',
			'image/png',
			'audio/wav',
			'audio/x-wav',
			'audio/mpeg3',
			'audio/x-mpeg-3',
			'audio/mp3',
			'audio/mpeg'
		];
    	
		if ($file) {
	    	
			if (in_array($mime, $allowedMimes)) {
				// '/assets/uploads' 
				$destinationPath = public_path() . '/assets/uploads';
				
				$filename = $file->getClientOriginalName();
				$now = new DateTime();
				$extension = pathinfo($filename, PATHINFO_EXTENSION);
				$filename = pathinfo($filename, PATHINFO_FILENAME);				
				
				if ($extension == '') {
					if ($mime == 'audio/wav' || 'audio/x-wav'){
						$extension = 'wav';
					} else if ($mime == 'audio/mpeg3' || $mime == 'audio/x-mpeg-3' || $mime == 'audio/mpeg' || $mime == 'audio/mp3') {
						$extension = 'mp3';
					}
				}
				
				if ($request->has('type')) {
					$attributes['type'] = 'PROFILE_IMAGE';
				} else if ( substr($mime, 0, 5) == 'image' && !$request->has('type')) {
					$attributes['type'] = 'SECRET_IMAGE';
				} else if ( !$request->has('type')) {
					$attributes['type'] = 'SECRET_AUDIO';
				}
				
				$filename_encoded = rawurlencode(str_random(6).$now->getTimestamp());

				
				
				$upload_success = $request->file('file')->move($destinationPath, $filename_encoded.'.'.$extension);
					 
				if ($upload_success) {
					
					$attributes['fileurl'] = $url . "/assets/uploads/" . $filename_encoded.'.'.$extension;											

					$response = (array)ApiCalls::post('file/fetch', $attributes);
					// Log::info($attributes);
					// Log::info($response);		
					
					return $response;

					if ($response['success'] == true){
						Storage::delete(public_path()."/assets/uploads/".$filename_encoded.'.'.$extension);
						
						// displaying file
						$array = [
							'filelink' => $response['data']->fileurl,
							'filename' => $response['data']->filename,
							'filecode' => $response['data']->filecode
						];
						return stripslashes(json_encode($array));
					} else {
						$error_message="Error";
						return $error_message;
					}
					
				} else {
					$error_message="Error";
					return $error_message;
				}
	    	} else {
	    		$error_message="Invalid file";
				return $error_message;
	    	}
	    } else {
			$error_message="Invalid file";
			return $error_message;
	    }
		
	}
	
	public function postCrop(Request $request)
	{		
		// $all = Input::all();
		$all = $request->all();
		$url = url('/');
		$json = $all['json'];
		$formData = json_decode($all['formData'], true);
		
		$destinationPath = $json['filelink'];
		
		$img = Image::make($destinationPath);
		
		$img->crop($formData['width'], $formData['height'], $formData['x'], $formData['y']);
		
		$img->save(public_path() . '/assets/uploads/'. $json['filename']);
		
		// ~~~~~~ upload again
		$attributes['fileurl'] = $url . "/assets/uploads/" . $json['filename'];
		$attributes['type'] = 'SECRET_IMAGE';
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('file/fetch', $attributes);
		
		if ($response['success'] == true) {
			
			// displaying file
			$array = [
				'filelink' => $response['data']->fileurl,
				'filename' => $response['data']->filename,
				'filecode' => $response['data']->filecode,
				'message'  => 'success'
			];
			
			Storage::delete( public_path()."/assets/uploads/" . $json['filename']);
			
			return stripslashes(json_encode($array));

		} else {
			Storage::delete( public_path()."/assets/uploads/" . $json['filename']);
			
			return [
				'message' => 'Error'
			];
		}
		File::delete( public_path()."/assets/uploads/" . $json['filename']);
		
		return [
			'message' => 'Error'
		];
		
	}
	
	public function postSecretBooks(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('secret/books/list', $attributes);
		
		return $response;
	}
	
	public function postForgotPassword(Request $request)
	{
		$response = (array)ApiCalls::post('user/pwd/reset', ['email'=>$request->input('email')]);
		
		return $response;
	}
	
	public function postResetPassword(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$response = (array)ApiCalls::post('user/pwd/edit', $attributes);
		
		return $response;
	}
	
	public function postClaimAccount(Request $request)
	{
		$attributes =  $request->except('id', 'type');
		
		if ($request->input('type') == 'author') {
			$attributes['authorId'] = $request->input('id');
		} else {
			$attributes['publisherId'] = $request->input('id');
		}
		$response = (array)ApiCalls::post('contact/claim', $attributes);
		
		return $response;
	}
	
	public function postBooksReport(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('reports/books/count', $attributes);
		
		return $response;
	}
	
	public function postBooksUsersReport(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('reports/books/users/count', $attributes);
		
		return $response;
	}
	
	public function postSecretsReport(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('reports/secrets/views/count', $attributes);
		
		return $response;
	}
	
	public function postSecretsStatsReport(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('reports/secrets/count', $attributes);
		
		return $response;
	}
	
	public function postPubAuthorCount(Request $request)
	{
		// $attributes = Input::all();
		$attributes = $request->all();
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('reports/publisher/author/count', $attributes);
		
		return $response;
	}
	
}