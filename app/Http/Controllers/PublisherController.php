<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use App\Classes\ApiCalls;

class PublisherController extends Controller {
	
	public function getIndex($id)
	{
		$response = (array)ApiCalls::post('publisher/view', [ 'id' => $id ]);
		$secrets = (array)ApiCalls::post('publisher/secrets/list', [ 'id' => $id ]);
		
		$name = 'Publisher';
		
		if ( $response['success'] == true ){
			if ( isset($response['data']->name) && ( $response['data']->name != '' || $response['data']->name != null ) ){
				$name = $response['data']->name;
			}
			
			View::share('id', $id);
			View::share('name', $name);
			View::share('data', $response['data']);
			
			if ( $secrets['total'] > 0 ){
				View::share('secrets', $secrets['data']);
			}
			
			if (session()->has('id')){
				if ( session('id') == $id ){
					return view('publisher.profile-edit');
				}
			}
			
			return view('publisher.profile');
			
		}
		
		return redirect('/');
		
		
	}
	
}