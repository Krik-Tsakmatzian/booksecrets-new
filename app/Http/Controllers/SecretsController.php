<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use App\Classes\ApiCalls;

class SecretsController extends Controller {
	
	public function getFeed()
	{
        View::share('name', 'Feed');
		return view('user.feeds');
	}
	
	public function getFavorites()
	{
        View::share('name', 'Favorites');
		return view('user.favorites');
	}
	
	public function getIndex($id, $secret)
	{
        $attributes['bookId'] = $id;
        $attributes['secretId'] = $secret;
        
        if (session()->has('token')){
			$attributes['token'] = session('token');
        }
		
		$response = (array)ApiCalls::post('secret/view', $attributes);
		
		View::share('bookId', $id);
		View::share('secretId', $secret);
		
		if ($response['success'] == true){
			
			$response['data']->bookID = $id;
			View::share('secret', $response['data']);
			
			View::share('name', $response['data']->title);
			
			// meta tags
			View::share('meta_desc', mb_substr(strip_tags($response['data']->text), 0, 100));
			if (isset($response['data']->book->cover)){
				View::share('meta_img', config('common.base_url').$response['data']->book->cover);
			}
						
			if ($response['data']->locked == 1 && $response['data']->userLocked == 1){
				// return Redirect::to('/');
				return view('user.secret');
			}

			return view('user.secret');

		} else if ($response['success'] == false) {
			if ($response['msg'] == 'LOGIN-REQUIRED'){
				return redirect('user/login');
			}else {
				return view('errors.404');
			}			
		} else {
			return view('errors.404');
		}
		
		return view('errors.404');
	}
	
	public function getBook($id)
	{
		if (!session()->has('token')){
			return redirect('/user/login');
		}
		
        $attributes['bookId'] = $id;
		$attributes['token'] = session('token');
		
		View::share('bookId', $id);
		
		$response = (array)ApiCalls::post('user/books/view', $attributes);
		
		if ($response['success'] == true) {
			View::share('book_secrets', $response['data']);
			
			View::share('name', $response['data']->title);
			
			return view('user.book-secrets');
		}
        
        return view('errors.404');
	}
	
	public function getAdd()
	{
		View::share('name', 'Add Secret');
		
		return view('advanced.add-secret');
	}

	public function getAddByUser($id) {

		if (!session()->has('token')){
			return redirect('/user/login');
		}

		if (session('role') != 'MOBUSER') {
			return redirect('/secrets/feed');
		}
	
        $attributes['query'] = $id;		
			
		$response = (array)ApiCalls::post('book/search', $attributes);

		
		if ($response['success'] == true) {
			View::share('book', $response['data'][0]);
			
			View::share('name', 'Add Secret');
			
			return view('advanced.add-secret');
		}
        
        return view('errors.404');

	}
	
	public function getAll()
	{
		View::share('name', 'My Secrets');
		
		return view('advanced.my-secrets');
	}
	
	public function getEdit($id, $secret)
	{
		$attributes['bookId'] = $id;
        $attributes['secretId'] = $secret;
		$attributes['token'] = session('token');
		
		$response = (array)ApiCalls::post('secret/view', $attributes);
		
		View::share('name', 'Edit Secret');
		View::share('bookId', $id);
		View::share('secretId', $secret);
		
		if ($response['success'] == true){
			
			View::share('secret', $response['data']);
			
			return view('advanced.edit-secret');
		}
		
		return view('errors.404');
	}
}