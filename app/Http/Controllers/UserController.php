<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Classes\ApiCalls;
use Artdarek\OAuth\Facade\OAuth;

class UserController extends Controller {

	public function getIndex($id) {
					
		$response = (array)ApiCalls::post('user/viewinfo', ['id' => $id ]);		
		$secrets = (array)ApiCalls::post('user/secrets/list', ['id' => $id]);

		$name = 'User';
		

		if ($response['success'] == true ){
			if ( isset($response['data']->name) && ( $response['data']->name != '' || $response['data']->name != null ) ){
				$name = $response['data']->name;
			}
			
			View::share('id', $id);
			View::share('name', $name);
			View::share('data', $response['data']);
			
			if ( $secrets['total'] > 0 ){
				View::share('secrets', $secrets['data']);
			}
			
			if (session()->has('id')){
				if ( session('id') == $id ){
					return view('mob-user.profile-edit');
				}
			}
			
			return view('mob-user.profile');
			
		}
		
		return redirect('/');
	}
	
	public function getSignUp()
	{
		if (session()->has('token')){
        	return redirect('/secrets/feed');
        }
        
		return view('user.signup');
	}
	
	public function getLogin()
	{
		if (session()->has('token')){
        	return redirect('/secrets/feed');
        }
        
		return view('user.login');
	}
	
	public function getFacebooklogin(Request $request) 
	{
		// get data from input		
		$code = $request->input('code');
		// get fb service
		$fb = OAuth::consumer('Facebook');
		
		// check if code is valid
		
		// if code is provided get user data and sign in
		if ( !empty($code) ) {
		
			// This was a callback request from facebook, get the token
			$token = $fb->requestAccessToken($code);
			
			$acctoken = $token->getAccessToken();
			
			// Send a request with it
			$result = json_decode( $fb->request('/v2.6/me?fields=id,name,email'), true );
			
			$response = (array)ApiCalls::post('user/fblogin', ['fbtoken' => $acctoken, 'fbid' => $result['id']] );
			
			if ($response['success'] == true) {
				if (isset($response['data']->username) && $response['data']->username != '') {
					$name = $response['data']->username;
				} else if (isset($response['data']->name) && $response['data']->name != '' ) {
					$name = $response['data']->name;
				} else if ($response['data']->role == 'AUTHOR' && isset($response['data']->author) && isset($response['data']->author->name)) {
					$name = $response['data']->author->name;
				} else if ($response['data']->role == 'PUBLISHER' && isset($response['data']->publisher) && isset($response['data']->publisher->name)) {
					$name = $response['data']->publisher->name;
				} else{
					$name = '';	
				}
                
                session(['name' => $name]);
                session(['token' => $response['data']->token]);
                session(['role' => $response['data']->role]);

				
				if (isset($response['data']->id)) {
                    session(['id' => $response['data']->id]);					
				} else {
                    session(['id' => $name]);
				}
				
				return redirect('/secrets/feed');
			} else {
				return redirect('/user/login')->with('error_message', 'Login failed');
			}
		}
		// if not ask for permission first
		else {
			// get fb authorization
			$url = $fb->getAuthorizationUri();
			
			// return to facebook login url
			return redirect((string)$url);
		}
	
	}
	
	public function getFacebookloginAuthor() 
	{
		// get data from input
		$code = $request->input('code');
		
		// get fb service
		$fb = OAuth::consumer( 'Facebook' );
		
		// check if code is valid
		
		// if code is provided get user data and sign in
		if (!empty( $code )) {
		
			// This was a callback request from facebook, get the token
			$token = $fb->requestAccessToken( $code );
			
			$acctoken = $token->getAccessToken();
			
			// Send a request with it
			$result = json_decode( $fb->request('/v2.6/me?fields=name,email,first_name,last_name'), true );
			
			return redirect('/#sign-up')->with('result', $result);
		}
		// if not ask for permission first
		else {
			// get fb authorization
			$url = $fb->getAuthorizationUri();
			
			// return to facebook login url
			return redirect( (string)$url );
		}
	
	}
	
	public function getLogout(Request $request)
	{
		session()->forget('token');
		session()->forget('name');
		session()->forget('role');
		session()->forget('redirect');
		
		if ( session()->has('id') ){
			session()->forget('id');
		}	
		
		if ($request->hasCookie('cookie_token')){
			$cookie = Cookie::forget('cookie_token');
			return redirect('/')->withCookie($cookie);
		}
		
		return redirect('/');
	}
	
	public function getLibrary(Request $request)
	{
		if( !session()->has('token') )
			return redirect('/user/login')->with('redirect', $request->fullUrl());
        
        View::share('name', 'Library');
		return view('user.library');
	}


	public function getBooks(Request $request)
	{
		if( !session()->has('token') )
			return redirect('/user/login')->with('redirect', $request->fullUrl());
        
        View::share('name', 'Books');
		return view('user.books');
	}

	
	public function getClaimAccount()
	{
		if ( !session()->has('token') ){
	        View::share('name', 'Claim Account');
			return view('user.claim_account');
		}
		
		return redirect('/');
	}
	
	public function getForgotPassword(Request $request)
	{
		if ( !session()->has('token') ){
			View::share('name', 'Forgot Password');
			
			if ($request->has('reset')){
				return view('user.reset_password');
			}
			
			return view('user.forgot_password');
		}
		
		return redirect('/');
	}
	
	public function getStatistics(Request $request)
	{
		if( !session()->has('token') ){
			return redirect('/user/login')->with('redirect', $request->fullUrl());
		}
		else if ( session()->has('token') && session()->get('role') == 'MOBUSER' ){
			return view('errors.404');
		}
			
		View::share('name', 'Statistics');
		
		return view('advanced.statistics');
	}
}