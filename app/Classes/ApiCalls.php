<?php 

namespace App\Classes;

use GuzzleHttp\Client;

class ApiCalls {


	public static function get($url, $data = [], $auth = false)
	{
		$client = new Client();
        $response = $client->request('GET', config('common.api_url').$url, [
            'form_params' => $data
        ]);

        return json_decode($response->getBody());
	}



	public static function post($url, $data = [], $auth = false)
	{
		$client = new Client();
        $response = $client->request('POST', config('common.api_url').$url, [
            'form_params' => $data
        ]); 

        return json_decode($response->getBody());
	}
}
