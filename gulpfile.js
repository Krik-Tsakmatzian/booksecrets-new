var gulp = require('gulp'),
    cleanCSS = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    sass = require("gulp-sass"),
    hash = require('gulp-hash'),
    imagemin = require('gulp-imagemin'),
    // cache = require('gulp-cache'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    browserify = require('browserify'),
    ngAnnotate = require('gulp-ng-annotate'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer');
    
gulp.task('styles', function(){
    return gulp.src('./public/assets/scss/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./public/dist/css'));
});

gulp.task('icons', function(){
    return gulp.src('./public/assets/css/icons.css')
        .pipe(cleanCSS())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./public/dist/css'));
});

gulp.task('advCss', function(){
    return gulp.src([
            './public/assets/bower_components/cropper/dist/cropper.min.css',
            './public/assets/redactor/redactor.css',
            './public/assets/bower_components/angular-chart.js/dist/angular-chart.min.css'
        ])
        .pipe(concat('advanced.css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./public/dist/css'));
});

gulp.task('vendorCss', function(){
    return gulp.src([
            './public/assets/bower_components/fontawesome/css/font-awesome.min.css',
            './public/assets/bower_components/OwlCarouselBower/owl-carousel/owl.carousel.css',
            './public/assets/bower_components/angular-toastr/dist/angular-toastr.min.css',
            './node_modules/angular-switcher/dist/angular-switcher.min.css'
        ])
        .pipe(concat('vendor.css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./public/dist/css'));
});

gulp.task('scripts', function() {
    return gulp.src([
            './public/assets/bower_components/jquery/dist/jquery.min.js',
            './public/assets/bower_components/OwlCarouselBower/owl-carousel/owl.carousel.min.js',
            './public/assets/bower_components/history.js/scripts/bundled/html4+html5/jquery.history.js'
        ])
        // .pipe(cache.clear())
        .pipe(concat('vendor.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('./public/dist/js'));
});

gulp.task('advJs', function() {
    return gulp.src([
            './public/assets/redactor/plugins/imagemanager.js',
            './public/assets/redactor/plugins/custom-video.js',
            './public/assets/redactor/plugins/custom-audio.js',
            './public/assets/bower_components/cropper/dist/cropper.js',
            // './public/assets/redactor/plugins/imagecropper.js',
            './public/assets/redactor/plugins/recorder/recorder.js',
            './public/assets/redactor/plugins/recorder/Fr.voice.js',
            './public/assets/redactor/plugins/recorder/record.js',
            './public/assets/bower_components/moment/min/moment.min.js',
            './node_modules/chart.js/Chart.min.js'
        ])
        .pipe(concat('advanced.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('./public/dist/js'));
});

gulp.task('images', function() {
    return gulp.src([
            './public/assets/img/**/*'
        ])
        // .pipe(cache.clear())
        // .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
        .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
        .pipe(gulp.dest('./public/dist/img'));
});


// Fonts
gulp.task('fonts', function() {
    return gulp.src([
            './public/assets/bower_components/fontawesome/fonts/fontawesome-webfont.*',
            './public/assets/fonts/*'
        ])
        .pipe(gulp.dest('./public/dist/fonts/'));
});

gulp.task('browserify', function() {
    // Grabs the app.js file
    return browserify('./public/assets/js/app.js')
        // bundles it and creates a file called app.js
        .bundle()        
        .pipe(source('app.js'))
        .pipe(buffer())
        .pipe(ngAnnotate())
        .pipe(uglify())        
        .pipe(gulp.dest('./public/dist/js'));
});

gulp.task('browserifyDev', function() {
    // Grabs the app.js file
    return browserify('./public/assets/js/appDev.js')
        // bundles it and creates a file called app.js
        .bundle()        
        .pipe(source('appDev.js'))     
        .pipe(buffer())
        .pipe(ngAnnotate())
        // .pipe(uglify())        
        .pipe(gulp.dest('./public/dist/js'));
});

gulp.task('watch', function () {
    gulp.watch('./public/assets/scss/**/*.scss', ['styles']);
    gulp.watch('./public/assets/js/**/*.js', ['scripts', 'browserify', 'browserifyDev']);
    gulp.watch('./public/assets/img/**/*', ['images']);
    gulp.watch('./public/assets/fonts/**/*', ['fonts']);
    gulp.watch('./public/assets/css/**/*', ['icons']);
});


gulp.task('default', ['styles', 'scripts', 'watch', 'images', 'browserify', 'browserifyDev', 'fonts', 'icons', 'advCss', 'vendorCss', 'advJs']);