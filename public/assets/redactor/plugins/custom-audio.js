$.Redactor.prototype.audio = function()
{
    return {
        getTemplate: function()
        {
            return String()
            + '<div class="content">'
            + '<form name="mp3_form" class="hidden">'  
            + '<input type="file" name="file" id="mp3_file" accept=".mp3">'
            + '<input type="submit" value="Submit">'
            + '</form>'
            + '<p>Πατήστε record για να ξεκινήσει η ηχογράφηση ή επιλέξτε ένα αρχείο mp3 από τον υπολογιστή σας.</p>'
            + '<div>'
            + '<audio controls src="" id="audio"></audio>'
            + '</div>'
            + '<div class="hidden mp3_load">'
            + '<span class="mp3_name pull-left"></span> <span class="error_msg pull-left"></span>'
            + '<span class="loader pull-left"><div class="loader-inner ball-clip-rotate"><div></div></div></span>'
            + '</div>'
            + '<div class="clearfix"></div>'
            + '<div style="margin:10px 0;">'
            + '<a class="button" id="record">Εγγραφή</a>'
            + '<a class="button disabled one" id="pause">Παύση</a>'
            + '<a class="button disabled one" id="play">Αναπαραγωγή</a>'
            + '<a class="button disabled one" id="stop">Εγγραφή νέου</a>'
            + '<a class="button one" id="upload_mp3">Επιλογή Αρχείου</a>'
            // + '<a class="button disabled one" id="download">Download</a>'
            // + '<a class="button disabled one" id="base64">Base64 URL</a>'
            // + '<a class="button disabled one" id="mp3">MP3 URL</a>'
            + '<a class="button disabled one hidden" id="save"><span>Upload στον Server</span>'
            +    '<div class="plotlybars-wrapper">'
            +      '<div class="plotlybars">'
            +        '<div class="plotlybars-bar b1"></div>'
            +        '<div class="plotlybars-bar b2"></div>'
            +        '<div class="plotlybars-bar b3"></div>'
            +        '<div class="plotlybars-bar b4"></div>'
            +        '<div class="plotlybars-bar b5"></div>'
            +        '<div class="plotlybars-bar b6"></div>'
            +        '<div class="plotlybars-bar b7"></div>'
            +      '</div>'
            +    '</div>'
            + '</a>'
            + '</div>'
            + '</div>';
        },
        init: function ()
        {
            var button = this.button.add('audio', 'Εισαγωγή ηχητικού');
            this.button.addCallback(button, this.audio.show);
 
            // make your added button as Font Awesome's icon
            this.button.setAwesome('audio', 'fa-file-audio-o');
        },
        show: function()
        {
            if ($('#textaa').val().indexOf('<audio') != -1){
                alert('Μπορείτε να εισάγετε μόνο ένα ηχητικό');
            }
            else{
                this.modal.addTemplate('audio', this.audio.getTemplate());
 
                this.modal.load('audio', 'Εισαγωγή ηχητικού', 700);
     
                this.modal.createCancelButton();
     
                var button = this.modal.createActionButton('Εισαγωγή');
                button.on('click', this.audio.insert);
     
                this.selection.save();
                this.modal.show();
            }
        },
        insert: function()
        {
            var audio = $('#audio').attr('src');
            var code = $('#audio').attr('filecode');
            var block = this;
            var sound;
            
            if ( (!audio || audio == '') && !$('#record').hasClass('disabled') && $('#play').hasClass('disabled') ){
                alert('Record somethin first!');
            }
            else if ( $('#record').hasClass('disabled') && !$('#play').hasClass('disabled') && !$('.redactor-modal-btn.redactor-modal-action-btn').hasClass('recorded')){
                $('.redactor-modal-btn.redactor-modal-action-btn').prop( "disabled", true );
                $('#pause').trigger('click');
                $('#save').trigger('click');
            }else if ($('.redactor-modal-btn.redactor-modal-action-btn').hasClass('recorded')){
                block.selection.restore();
                block.modal.close();
                
                var current = block.selection.getBlock() || block.selection.getCurrent();
                sound = '<audio controls preload="auto" id="audio" filecode="'+code+'" src="'+audio+'">';
                
                if (current) {
                    if ( $('#textaa').val() === '' ){
                        angular.element('.redactor-editor').removeClass('redactor-placeholder');
                        angular.element('.redactor-editor').empty().append('<p>&nbsp;</p><p>'+sound+'</p><p>&nbsp;</p>');
                    } else {
                        $(current).after('<p>&nbsp;</p><p>' + sound + '</p><p>&nbsp;</p>');
                    }
                } else {
                    block.insert.html('<p>&nbsp;</p><p>' + sound + '</p><p>&nbsp;</p>');
                }
                
                angular.element('.redactor-editor').find('audio').parent().addClass('audio-container');
                
                block.code.sync();    
                
                $( "#textaa" ).focus();
            }
           
            
        }
    };
};