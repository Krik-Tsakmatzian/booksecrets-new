function restore(){
  $("#record, #live").removeClass("disabled");
  $("#pause").replaceWith('<a class="button one" id="pause">Pause</a>');
  $(".one").addClass("disabled");
  Fr.voice.stop();
}
$(document).ready(function(){
  $(document).on("click", "#record:not(.disabled)", function(){
    $('.error_msg').empty();
    $("#audio").attr("src", '');
    $("#audio").attr("filecode", '');
    $('.mp3_load .loader').addClass('hidden');
    $('.mp3_load').addClass('hidden');
    
    elem = $(this);
    Fr.voice.record($("#live").is(":checked"), function(){
      elem.addClass("disabled");
      $("#live").addClass("disabled");
      $(".one").removeClass("disabled");
    });
  });
  
  $(document).on("click", "#pause:not(.disabled)", function(){
    if($(this).hasClass("resume")){
      Fr.voice.resume();
      $(this).replaceWith('<a class="button one" id="pause">Pause</a>');
    }else{
      Fr.voice.pause();
      $(this).replaceWith('<a class="button one resume" id="pause">Resume</a>');
    }
  });
  
  $(document).on("click", "#stop:not(.disabled)", function(){
    restore();
  });
  
  $(document).on("click", "#play:not(.disabled)", function(){
    Fr.voice.export(function(url){
      $("#audio").attr("src", url);
      $("#audio")[0].play();
    }, "URL");
    // restore();
  });
  
  $(document).on("click", "#download:not(.disabled)", function(){
    Fr.voice.export(function(url){
      $("<a href='"+url+"' download='MyRecording.wav'></a>")[0].click();
    }, "URL");
    restore();
  });
  
  $(document).on("click", "#base64:not(.disabled)", function(){
    Fr.voice.export(function(url){
      alert("Check the web console for the URL");
      
      $("<a href='"+ url +"' target='_blank'></a>")[0].click();
    }, "base64");
    restore();
  });
  
  $(document).on("click", "#mp3:not(.disabled)", function(){
    alert("The conversion to MP3 will take some time (even 10 minutes), so please wait....");
    Fr.voice.export(function(url){
      alert("Check the web console for the URL");
      
      $("<a href='"+ url +"' target='_blank'></a>")[0].click();
    }, "mp3");
    restore();
  });
  
  $(document).on("click", "#save:not(.disabled)", function(){
    var $button = $(this);
    $('.redactor-modal-btn.redactor-modal-action-btn').text('Please wait...');
    
    Fr.voice.export(function(blob){
      var formData = new FormData();
      
      if (!blob){
         alert('Record somethin first!');
      }else{
        formData.append('file', blob);
        
        $.ajax({
          url: window.location.origin+'/api/upload',
          type: 'POST',
          data: formData,
          contentType: false,
          processData: false,
          success: function(url) {
            if ( url == 'Error' || url == 'Invalid file' ){
              $('.error_msg').text('Μη αποδεκτό αρχείο')
            }else {
              $('.error_msg').empty();
              url = JSON.parse(url);
              $("#audio").attr("src", url.filelink).attr('filecode', url.filecode);
              $('.redactor-modal-btn.redactor-modal-action-btn').prop( "disabled", false ).addClass('recorded');
              $('.redactor-modal-btn.redactor-modal-action-btn').trigger('click');
            }
          }
        });
      }
      
    }, "blob");
    restore();
  });
  
  $(document).on('click', '#upload_mp3', function() {
     $('#mp3_file').trigger('click');
  });
  
  var files;
  $(document).on('change', '#mp3_file', function(event){
    var file = $(this).val();
    var name = $(this)[0].files[0].name;
    
    $('.mp3_load').removeClass('hidden');
    $('.mp3_load .loader').removeClass('hidden');
    $('.mp3_name').text(name);
    
    files = event.target.files;
        
    var data = new FormData();
    data.append('file', files[0]);
    
    $.ajax({
      url: window.location.origin+'/api/upload',
      type: 'POST',
      data: data,
      contentType: false,
      processData: false,
      success: function(url) {
        if ( url == 'Error' || url == 'Invalid file' ){
          $('.error_msg').text('Μη αποδεκτό αρχείο')
        }else {
          $('.error_msg').empty();
          url = JSON.parse(url);
          $("#audio").attr("src", url.filelink).attr('filecode', url.filecode);
          $('.redactor-modal-btn.redactor-modal-action-btn').prop( "disabled", false ).addClass('recorded');
          $('.redactor-modal-btn.redactor-modal-action-btn').trigger('click');
        }
        
        $('.mp3_load .loader').addClass('hidden');
      }
    });
  });
});
