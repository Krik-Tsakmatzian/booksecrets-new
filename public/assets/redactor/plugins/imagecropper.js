$.Redactor.prototype.imagecropper = function()
{
	return {
		//default settings
		cropped: false,
		modalWidth: 700,
		buttonCropText: 'Περικοπή',
		buttonSaveText: 'Αποθήκεύση χωρίς περικοπή',

		init: function() {
			var button = this.button.add('imagecropper', 'Εισαγωγή εικόνας');
			this.button.setAwesome('imagecropper', 'fa-picture-o');
			this.button.addCallback(button, this.imagecropper.showModal);
			this.opts.imagecropper.croppingOptions = this.opts.imagecropper.croppingOptions || {};
		},
		showModal: function() {
			if ($('#textaa').val().match(/<img/g) && $('#textaa').val().match(/<img/g).length > 19){
				alert('Μπορείτε να εισάγετε μέχρι 20 εικόνες');
			}
			else{
				this.opts.imagecropper.modalWidth = this.opts.imagecropper.modalWidth || this.imagecropper.modalWidth;
				this.opts.imagecropper.buttonCropText = this.opts.imagecropper.buttonCropText || this.imagecropper.buttonCropText;
				this.opts.imagecropper.buttonSaveText = this.opts.imagecropper.buttonSaveText || this.imagecropper.buttonSaveText;
	
				this.modal.load('image', this.lang.get('image'), this.opts.imagecropper.modalWidth);
				this.upload.init('#redactor-modal-image-droparea', this.opts.imagecropper.imageUpload, this.imagecropper.crop);
				this.selection.save();
				this.modal.show();
			}
		
		},
		crop: function(json, direct, e) {
			// error callback
			if (typeof json.error != 'undefined') {
				this.modal.close();
				this.selection.restore();
				this.core.setCallback('fileUploadError', json);
				return;
			}
			if (!this.imagecropper.cropped) {
				var self = this,
					template =
						'<form class="js-crop-form" style="margin:2rem;" ng-controller="AdvancedController">' +
							'<div class="img-container">' +
								'<button class="js-crop button" style="margin: 0.5rem 0.5rem 0.5rem 0" >' +
									'<i class="fa fa-crop fa-lg"></i> ' + this.opts.imagecropper.buttonCropText  +
								'</button>' +
								'<button class="js-save button" style="margin:0.5rem">' +
									'<i class="fa fa-floppy-o fa-lg"></i> ' + this.opts.imagecropper.buttonSaveText +
								'</button>' +
								'<div style="max-height: 400px;"><img id="image" class="js-image" src="' + json.filelink + '"></div>' +
								'<input class="js-crop-data" name="data" type="hidden">' +
								'<input class="js-crop-src" name="src" value="' + json.filelink + '" type="hidden">' +
							'</div>' +
						'</form>';

				this.upload.$droparea.find('.js-crop-form').remove();
				this.upload.$droparea.append(template);
				this.upload.$droparea.css('padding', '0');

				this.opts.imagecropper.croppingOptions.built = function () {
					$(this).cropper("zoom", 0.5);
				};

				this.opts.imagecropper.croppingOptions.done = function (data) {
					//prepare cropping data for server
					var cropData = {
						x: data.x,
						y: data.y,
						height: data.height.toFixed(0),
						width: data.width.toFixed(0)
					};
					$('.js-crop-data').val(JSON.stringify(cropData));
				};

				$('.js-image').cropper(this.opts.imagecropper.croppingOptions);
				
				this.upload.$droparea.on('click', '.js-crop', function (e) {
					e.preventDefault();
					var data = $('.js-image').cropper('getData'),
						cropData = {
							x: data.x.toFixed(0),
							y: data.y.toFixed(0),
							height: data.height.toFixed(0),
							width: data.width.toFixed(0)
						};
					
					$('.js-crop-data').val(JSON.stringify(cropData));

					if (self.imagecropper.imagecropped()) {
				
				        var formData = $('.js-crop-form .js-crop-data').val();
				        
						$.ajax({
                            url: window.location.origin+'/api/crop',
                            type: 'POST',
                            data: { json, formData },
                            success: function(data) {
                                data = JSON.parse(data);
                                self.imagecropper.cropped = false;
                                // json.filelink = data.filelink;
                                self.imagecropper.insertImage(data);
                            }
                          });
					} else {
						self.imagecropper.insertImage(json);
					}
				});

				this.upload.$droparea.on('click', '.js-save', function (e) {
					e.preventDefault();
					self.imagecropper.insertImage(json);
				});
			} else {
				this.imagecropper.insertImage(json);
			}
		},
		imagecropped: function() {
			var cropData = JSON.parse($('.js-crop-data').val()),
				imageHeight = $('.js-image').cropper('getImageData').naturalHeight.toFixed(0),
				imageWidth = $('.js-image').cropper('getImageData').naturalWidth.toFixed(0);
			if (cropData.height == imageHeight && cropData.width == imageWidth) {
				return false;
			} else {
				return true;
			}
		},
		insertImage: function(im) {
			var $img = $('<img>');
			var block = this;
			
			this.imagecropper.cropped = false;
			$img.attr('src', im.filelink).attr('data-redactor-inserted-image', 'true').attr('filecode', im.filecode);
			this.modal.close();
			$('.js-image').cropper('destroy');
			
			this.selection.restore();
			
			this.insert.html(this.utils.getOuterHtml($img), false);
		}
	};
};
