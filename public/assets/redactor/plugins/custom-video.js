$.Redactor.prototype.record = function()
{
    return {
        getTemplate: function()
        {
            // insert video options
            return String()
            + '<div class="content">'
            + '<p>Εισάγετε το url του βίντεο:</p>'
            + '<form name="testForm" ng-controller="AdvancedController">'
            + '<input type="text" class="form-control" id="video_url" placeholder="Βίντεο URL">'
            + '<div class="error-log"> </div>'
            + '</form>'            
            + '</div>';
        },
        init: function () {
            var button = this.button.add('record', 'Εισάγετε Video');
            this.button.addCallback(button, this.record.show);
 
            // make your added button as Font Awesome's icon
            this.button.setAwesome('record', 'fa-video-camera');
        },
        show: function() {
            if (($('#textaa').val().match(/<iframe/g) && $('#textaa').val().match(/<iframe/g).length > 9) ||( $('#textaa').val().match(/<video/g) && $('#textaa').val().match(/<video/g).length > 9)) {
                alert('Μπορείτε να εισάγεται μέχρι 10 videos');
            } else{
                this.modal.addTemplate('record', this.record.getTemplate());
 
                this.modal.load('record', 'Εισάγετε Video', 700);
     
                this.modal.createCancelButton();
     
                var button = this.modal.createActionButton('Εισαγωγή');
                button.on('click', this.record.insert);
                
                this.selection.save();
                this.modal.show();
            }
        },
        insert: function() {
            var success;
            var iframe;
            var provider;
            var block = this;
            var token = '';
            var v = $('#video_url').val();
            
            if ( video != '' && v == '' ) {
                v = video;
            }
            
            if ( v == '' ){
                alert('Insert a url or record a video')
            } else {
                $('.redactor-modal-btn.redactor-modal-action-btn').text('Please wait...');
                $('.redactor-modal-btn.redactor-modal-action-btn').prop( "disabled", true );
                
                $.ajax({
                    type: 'POST',
                    url: "/api/embed",
                    data: { video: v },
                    asunc: false
                }).done(function(data) {
                    if ( data.success == false ){
                        $('.error-log').empty().text('Video url not valid. Please try with another one.');
                        success = false;
                        alert('Not valid');
                        $('.redactor-modal-btn.redactor-modal-action-btn').text('Insert');
                        $('.redactor-modal-btn.redactor-modal-action-btn').prop( "disabled", false );
                    }else{
                        success = true;
                        iframe = data.data;
                        provider = data.provider;
                        foo();
                    }
                    
                });
            }
            
            function foo() {
                if ( success == true ){
                    block.selection.restore();
                    block.modal.close();
                    
                    var current = block.selection.getBlock() || block.selection.getCurrent();
                    
                    if (current) {
                        if ( $('#textaa').val() == '' ){
                            angular.element('.redactor-editor').removeClass('redactor-placeholder');
                            angular.element('.redactor-editor').empty().append('<p>'+iframe+'</p>');
                        }else{
                            $(current).after('<p>'+iframe+'</p>');
                        }
                    }
                    else
                    	block.insert.html('<p>'+iframe+'</p>');
                    	
                    if ( !angular.element('.redactor-editor').find('iframe').length && angular.element('.redactor-editor').find('video').length ){
                        angular.element('.redactor-editor').find('video').attr('provider', provider).addClass('secretVideo').parent().addClass('video-container');
                        
                        if ( $('#textaa').val() == '' ){
                            angular.element('.redactor-editor').find('.video-container').after('<p><br></p>');
                        }else{
                            angular.element('.redactor-editor').find('.video-container').before('<p><br></p>');
                            angular.element('.redactor-editor').find('.video-container').after('<p><br></p>');
                        }
                    }else{
                        angular.element('.redactor-editor').find('iframe').attr('provider', provider).addClass('secretVideo').parent().addClass('video-container');
                    }	
                    
                    block.code.sync();          
                }
            }
            
        }
    };
};