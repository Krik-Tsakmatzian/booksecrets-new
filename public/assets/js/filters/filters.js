'use strict';

/* Filters */


angular.module('filters', [])
    .filter('orderObjectBy', function() {
        return function(items, field, reverse) {
            var filtered = [];
            
            angular.forEach(items, function(item) {
                filtered.push(item);
            });
            
            filtered.sort(function (a, b) {

                if (typeof a[field] === 'string' && typeof b[field] === 'string') {
                    
                    var nameA = a[field].toLowerCase(), nameB = b[field].toLowerCase();
                    
                    if (nameA < nameB) //sort string ascending
                        return -1 ;
                    
                    if (nameA > nameB)
                        return 1;
                    
                    return 0; //default return value (no sorting)
                    
                } else {
                    return (a[field] > b[field] ? 1 : -1);
                }
                
            });
            
            if (reverse) {
                filtered.reverse()
            };
            
            return filtered;
        };
    })
    .filter('htmlToPlaintext', function() {
        return function(text) {
          return text ? String(text).replace(/<[^>]+>/gm, '') : '';
        };
    });