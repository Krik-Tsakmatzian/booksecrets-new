'use strict';

var StatisticsController = /*@ngInject*/ function($scope, StatisticsService, toastr, $rootScope, $http) {

	$scope.series = ['Τίτλοι', 'Αντίτυπα'];
    
    $scope.labels = [];
	$scope.data = [];
	$scope.period = 'monthly';
		
	$scope.books = [];
	$scope.ids = [];
	$scope.userscount = [];
	$scope.pagination = {
	    currentPage:  1
	};
	
	$scope.stats = {};
	$scope.stats.book = 'total';
	$scope.stats.book_table = 'books';
	
	$scope.secret_series = ['Μοναδικές θεάσεις', 'Συνολικές θεάσεις'];
    
    $scope.secret_labels = [];
	$scope.secret_data = [];
		
	$scope.secrets = [];
	$scope.secret_ids = [];
	$scope.secrets_views = [];
	$scope.secrets_uniqueViews = [];	
	
	$scope.secret_pagination = {
	    currentPage:  1
	};
	
	var year = moment().format('YYYY');
	
	var booksCount;
	(booksCount = function(params){
    	StatisticsService.booksCount(params)
			.success(function(data) {
				if (data.successtrue) {
					$scope.data.splice(0, $scope.data.length);
					$scope.labels = data.data.labels;
					
					if ($scope.stats.book === 'total') {
						$scope.data.push(data.data.sumUniqueBookCount);	
						$scope.data.push(data.data.sumBookCount);
					} else {
						$scope.data = [];
						$scope.data.push(data.data.uniqueBookCount);
						$scope.data.push(data.data.bookCount);
					}
				}
			})
			.error(function(err) {
				toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
			});
	})({ fromDate: year + '-01-01', period: $scope.period });
	
	var usersbooksCount;
	(usersbooksCount = function(params){
    	StatisticsService.booksUsersCount(params)
			.success(function(data) {
				if (data.success) {
					$scope.books = data.data.titles;
					$scope.ids = data.data.ids;
					$scope.userscount = data.data.uniqueUserCount;
					
					$scope.total = data.total;
				}
			})
			.error(function(err) {
				toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
			});
	})({page: $scope.pagination.currentPage, limit: 6});
	
	var authorsCount;
	authorsCount = function(params){
    	StatisticsService.pubAuthorsCount(params)
			.success(function(data) {
				if (data.success) {
					$scope.authors = data.data.names;
					$scope.authors_ids = data.data.ids;
					$scope.userscount = data.data.uniqueUserBookCount;					
					$scope.total = data.total;
				}
			})
			.error(function(err) {
				toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
			});
	};
	
	$scope.updateBookStats = function() {
		booksCount({ fromDate: year + '-01-01', period: $scope.period });
	};
	
	$scope.pageChanged = function() {
		usersbooksCount({ page: $scope.pagination.currentPage, limit: 6 });
	};
	
	$scope.bookTableUpdate = function() {
		$scope.pagination.currentPage = 1;
		if ($scope.stats.book_table === 'writers') {
			authorsCount({ page: $scope.pagination.currentPage, limit: 6 });
		} else {
			usersbooksCount({ page: $scope.pagination.currentPage, limit: 6 });
		}
	};
	
	var secetsReporst;
	(secetsReporst = function(params) {
    	StatisticsService.secretsReportsStats(params)
			.success(function(data) {
				if (data.success) {
					$scope.secret_data.splice(0, $scope.secret_data.length);
					$scope.secret_labels = data.data.labels;
					
					$scope.secret_data.push(data.data.sumUniqueSecretCount);	
					$scope.secret_data.push(data.data.sumSecretCount);
				}
			})
			.error(function(err) {
				toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
			});
	})({ fromDate: year + '-01-01', period: $scope.period });
	
	var secetsCount;
	(secetsCount = function(params){
    	StatisticsService.secretsCount(params)
			.success(function(data) {
				if (data.success) {
					$scope.secrets = data.data.titles;
					$scope.secrets_views = data.data.secretViews;
					$scope.secrets_uniqueViews = data.data.uniqueSecretViews;
					$scope.secret_ids = data.data.ids;
					$scope.secret_bookIds = data.data.bookIds;
					$scope.secret_total = data.total;
				}
			})
			.error(function(err) {
				toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
			});
	})({ page: $scope.secret_pagination.currentPage, limit: 6 });
	
	$scope.secretpageChanged = function(){
		secetsCount({ page: $scope.secret_pagination.currentPage, limit: 6 });
	};
	
};

module.exports = StatisticsController;