'use strict';

var FavoritesController = /*@ngInject*/ function($scope, DashboardService, SecretsService, HelpService, toastr, $rootScope, $http) {
		$scope.overStar = 0;
        var	History = window.History;
		$scope.favorites = null;
		$scope.total = null;
		$scope.loading = true;
        
		var limit = HelpService.param('limit');
		if ( !limit ){
			limit = 6;
			$scope.moreFeeds = 0;
		}else{
			$scope.moreFeeds = limit;
		}
		
		var filter = { page: 1, limit: parseInt(limit) };
		var type = HelpService.param('type');
		$scope.chosenType = 'all';
		if ( type && type == 'booksecrets' ){
			filter.type = 'BOOKSECRETS'; 
			$scope.chosenType = 'booksecrets';
		}
		else if ( type && type == 'author' ){
			filter.type = 'AUTHOR';
			$scope.chosenType = 'author';
		}
		else if ( type && type == 'publisher' ){
			filter.type = 'PUBLISHER';
			$scope.chosenType = 'publisher';
		}
		
		$scope.order_item = 'date';
		var order = HelpService.param('order');
		switch (order) {
			case 'asc':
				$scope.order = 'asc';
				filter.orderDir = 'asc';
				break; 
			default: 
				$scope.order = 'desc';
				filter.orderDir = 'desc';
		}
		
		var favorites;
		(favorites = function(params){
	    	DashboardService.favorites(params)
				.success(function(data) {
					if ( data.success == true ){
						$scope.favorites = data.data;
						$scope.total = data.total;
						
						if ( $scope.total <= 6){
							$scope.moreFeeds = $scope.total;
						}
					}
					
					$scope.loading = false;
				})
				.error(function(data) {
					toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
			});
		})(filter);
		
		$scope.dynamicPopover = {
			templateUrl: '/assets/templates/infoPopoverFavorites.html'
		};
		
		$scope.hoveringOver = function(value) {
			$scope.overStar = value;
			$scope.rating = value;
			$scope.percent = 100 * (value / 5);
		};
		
		$scope.loadMore = function () {
			$scope.loading = true;
			$scope.moreFeeds = SecretsService.moreFeeds($scope.moreFeeds, $scope.total);
			
			var params = { page: 1, limit: $scope.moreFeeds };
		
			if ( $scope.chosenType && $scope.chosenType == 'booksecrets' ){
				params.type = 'BOOKSECRETS'; 
			}
			else if ( $scope.chosenType && $scope.chosenType == 'authors' ){
				params.type = 'AUTHOR';
			}
			else if ( $scope.chosenType && $scope.chosenType == 'publishers' ){
				params.type = 'PUBLISHER';
			}
			
			if ( $scope.order == 'asc' ){
				params.orderDir = 'asc';
			}else{
				params.orderDir = 'desc';
			}
			
			var url = '?limit='+$scope.moreFeeds+'&order='+$scope.order+'&type='+$scope.chosenType+'';
			History.pushState({timestamp: (new Date().getTime())}, 'Favorites | booksecrets', window.location.pathname+url);
			favorites(params);
		};
		
		$scope.sort = function(){
			if ($scope.moreFeeds==0)
				$scope.moreFeeds=6;
			else if ($scope.moreFeeds<6)
				$scope.moreFeeds=6;
				
			var filter = { page: 1, limit: parseInt($scope.moreFeeds) };
			
			if ( $scope.order == 'asc' ){
				$scope.order = 'desc';
				filter.orderDir = 'desc';
			}else{
				$scope.order = 'asc';
				filter.orderDir = 'asc';
			}
			
			if ( $scope.chosenType && $scope.chosenType == 'booksecrets' ){
				filter.type = 'BOOKSECRETS'; 
			}
			else if ( $scope.chosenType && $scope.chosenType == 'authors' ){
				filter.type = 'AUTHOR';
			}
			else if ( $scope.chosenType && $scope.chosenType == 'publishers' ){
				filter.type = 'PUBLISHER';
			}
			
			$scope.favorites = null;
			$scope.total = null;
			$scope.loading = true;
				
			var url = '?limit='+$scope.moreFeeds+'&order='+$scope.order+'&type='+$scope.chosenType+'';
			History.pushState({timestamp: (new Date().getTime())}, 'Favorites | booksecrets', window.location.pathname+url);
			favorites(filter);
		};
		
		$scope.showType = function(type){
			if ($scope.moreFeeds==0)
				$scope.moreFeeds=6;
			else if ($scope.moreFeeds<6)
				$scope.moreFeeds=6;
				
			var filter = { page: 1, limit: parseInt($scope.moreFeeds) };
			
			if ( type && type == 'booksecrets' ){
				filter.type = 'BOOKSECRETS'; 
				$scope.chosenType = 'booksecrets';
			}
			else if ( type && type == 'authors' ){
				filter.type = 'AUTHOR';
				$scope.chosenType = 'authors';
			}
			else if ( type && type == 'publishers' ){
				filter.type = 'PUBLISHER';
				$scope.chosenType = 'publishers';
			}else {
				$scope.chosenType = 'all';
			}
			
			$scope.favorites = null;
			$scope.total = null;
			$scope.loading = true;
			
			var url = '?limit='+$scope.moreFeeds+'&order='+$scope.order+'&type='+type+'';
			History.pushState({timestamp: (new Date().getTime())}, 'Favorites | booksecrets', window.location.pathname+url);
			favorites(filter);
		};
		
		
		$scope.editFavoriteCtrl = function(secret){
			var params = { 'secretId': secret.id, 'favorite': secret.userFavorite, 'bookId': secret.book.id };
			params.favorite = params.favorite === 0 ? 1 : 0;		
			
			SecretsService.favorite(params)
				.success(function(data) {
					if (data.success){
						secret.userFavorite = params.favorite;
						$scope.total--;
					}
				})
				.error(function(data) {
					toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
			});
		};
		
		$scope.readMore = function(secret, iscollapsed){
			var res;
			if (iscollapsed) {
				SecretsService.secretView({secretId: secret.id, bookId:secret.book.id})
					.success(function(data) {
						res = data.success ? false : true;						
					})
					.error(function(data) {
						toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
					});				
			} else {
				res = true;
			}
			
			return res;
		};
};

module.exports = FavoritesController;