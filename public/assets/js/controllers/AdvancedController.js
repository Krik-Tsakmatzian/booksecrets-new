'use strict';

var AdvancedController = /*@ngInject*/ function($scope, DashboardService, SecretsService, HelpService, toastr, $rootScope, $http) {

		$scope.text = angular.element('#textaa').val();
		$scope.title = '';
		$scope.locked = ( angular.element('input[name="locked"]').val() == 0 ? false : true );
		$scope.item = false;
        $scope.booksSecret = [];
		$scope.selectedBooks = [];
		$scope.userSelectedBook = {};
		$scope.search = {};
		$scope.status = false;
		$scope.totalBooks = 9;
		$scope.curPage = 1;
		$scope.loadingMore = false;
		var limit = 0;
		var savedStatus = false;
		var oldvalSec = {};
		
		$scope.showFilters = function(){
			$scope.filters = !$scope.filters;			
		};
		
		$scope.isOpen = false;
	
	    $scope.openCalendar = function(e, prop, button) {
			e.preventDefault();
			e.stopPropagation();
			
			if ($(".datetimepicker-wrapper input").is(":focus") || 
				typeof button !== 'undefined' && button === 'btn') {
				$scope.isOpen = true;
				$scope.schedule = 'later';			
			} else {
				$scope.isOpen = false;
				$scope.schedule == 'now';
			}
	    };
	    
	    $scope.myDate = {};
	    
	    $scope.dateOptions = {
	    	showWeeks: false
	    };
	    
	    $scope.setDate = function(secretdate) {
	    	$scope.myDate.date = new Date(secretdate);
	    }

		ZiggeoApi.Events.on("submitted", function (data) {
            var token = data.video.token;
            video = ZiggeoApi.Videos.source(token);
        });
        
        $scope.onChange = function (newValue, oldValue) {
		};
        
        $scope.postSecret = function(){
        	var date = $scope.myDate.date;
        	
			var secret = { 
				title: $scope.title, 
				text: $scope.text, 
				locked: ( typeof $scope.locked == 'undefined' || $scope.locked == false ? false : true ) ,
				bookId: $scope.booksSecret,
				status: ( $scope.status == true ? 1 : 0 )
			};
			
			if ( $scope.schedule === 'later' && $scope.status) {
				if (typeof date !== 'undefined' && date !== '' && date !== null && date !== 'undefined') {
					var post_date = date;
					post_date = post_date.toString();
					post_date = post_date.replace(/\([A-Z]+\)$/,"");
					
					secret.startDate = post_date;
					secret.status = 2;
				} else {
					secret.status = ( $scope.status == true ? 1 : 0 );
				}
			} else {
				secret.status = ( $scope.status == true ? 1 : 0 );
			}
			
			if ( $scope.item != false ){
				secret.id = $scope.item;
			}
			
			if ( $scope.text != '' && typeof $scope.text != 'undefined' ){
				var img = $scope.text.indexOf('<img');
				if ( img != -1 ){
					secret.imagefilecode = angular.element('.redactor-editor').find('img').attr('filecode');
				}
				
				var iframe = $scope.text.indexOf('<iframe');
				var video = $scope.text.indexOf('<video');
				if ( iframe != -1 && video == -1 ){
					secret.videourl = angular.element('.redactor-editor').find('iframe').attr('src');
					secret.videoprovider = angular.element('.redactor-editor').find('iframe').attr('provider');
				}else if ( iframe == -1 && video != -1 ){
					secret.videourl = angular.element('.redactor-editor').find('video').attr('src');
					secret.videoprovider = angular.element('.redactor-editor').find('video').attr('provider');
				}
				
				var audio = $scope.text.indexOf('<audio');
				if ( audio != -1 ){
					secret.audiofilecode = angular.element('.redactor-editor').find('audio').attr('filecode');
				}
			}
			
			if ( $scope.booksSecret.length > 0 && $scope.title != '' && $scope.text != '' && typeof $scope.title != 'undefined' && typeof $scope.text != 'undefined'){
				SecretsService.postSecret(secret) 
					.success(function(data) {
						if (data.success){
							savedStatus = true;
							window.location.replace('/secrets/all');
						} else{
							savedStatus = false;
						}
					})
					.error(function(data) {
						toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
					});
        	}
        	else if ($scope.booksSecret.length <= 0) {
        		toastr.error('Παρακαλούμε επιλέξτε τουλάχιστον ένα βιβλίο για το μυστικό!');
        	}
        	else if (!$scope.title || $scope.title == '') {
        		toastr.error('Παρακαλούμε εισάγετε έναν τίτλο');
        	}
        	else if (!$scope.text || $scope.text == '') {
        		toastr.error('Το περιεχόμενο του μυστικού δεν μπορεί να είναι κένο');
        	}else{
        		toastr.error('Συμπληρώστε όλα τα πεδία');
        	}
        };
        
        
        $scope.selectBook = function(id, $index, book){
			if ( $scope.booksSecret.indexOf(id) != -1 ){
				$scope.booksSecret.splice($scope.booksSecret.indexOf(id), 1);
				
				$scope.selectedBooks.splice($scope.selectedBooks.indexOf(book), 1);
			}else{
				$scope.booksSecret.push(id);
				
				$scope.selectedBooks.push(book);
			}
		};
		
		var secretbooks;
		secretbooks = function(params){
	    	SecretsService.bookSecrets(params)
				.success(function(data) {
					$scope.booksLoading = false;
					if ( data.success == true ){
						$scope.selectedBooks = data.data;
						
						angular.forEach(data.data, function(value, key) {
						  this.push(data.data[key].id);
						}, $scope.booksSecret);
					}
				})
				.error(function(data) {
					toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
			});
		};
		
		var getBooks;
		getBooks = function(params){
			$scope.search.limit = params.limit;
			$scope.search.page = params.page;
			
			DashboardService.advBookSearch($scope.search)
			.success(function(data) {
				$scope.booksLoading = false;
				$scope.search_loading = false;
				limit = 0;
				
				if ( data.success == true ){
					if ( data.data.length > 0 ){
						if ( params.page != 1 ){
							angular.forEach(data.data,function(value,index){
								if ( $.inArray( value, $scope.booksFound) == -1 ){ 
									$scope.booksFound.push(value);
								}
				            });
				            $scope.loadingMore = false;
						}else{
							$scope.booksFound = data.data;
						}
						$scope.totalBooks = data.total;
						$scope.filters = false;
						$scope.curPage = params.page;
						if ( $('#selectAll').is(':checked') ){ 
							$('#selectAll').prop('checked', false);
						}
					}else{
						$scope.booksFound = null;
						toastr.error('Δεν βρέθηκε αποτέλεσμα!');
					}
				}else{
					$scope.booksFound = null;
					toastr.error('Δεν βρέθηκε αποτέλεσμα!');
				}
			})
			.error(function(data) {
				$scope.booksLoading = false;
				toastr.error('Δεν βρέθηκε αποτέλεσμα!');
			});	
					
		};
		
		$scope.$watch('item', function () {
			if ($scope.item != false){
				$scope.booksLoading = true;
				oldvalSec.title = $scope.title;
				oldvalSec.text = $scope.text;
				secretbooks( {'secretId':$scope.item, 'page': 1, 'limit': 100} );
			}
		});
		
		$scope.$watch('role', function () {
			if ($scope.role == 'author'){
				$scope.booksLoading = true;
				getBooks({limit: 10000, page: 1});
			}
			
		});

		$scope.$watch('book', function () {
			if ($scope.book){
				var book = $scope.book;
				$scope.booksSecret.push(book.id);				
				$scope.selectedBooks.push(book);
			}
			
		});
		
		$scope.bookSearch = function () {
			var pass = false;
			angular.forEach($scope.search,function(value,index){
                if ( index != 'limit' && index != 'page' ){
                	if ( value != '' && value != ' ' && typeof value != "undefined" && value != null ){
                		pass = true;
                	}
                }
            });
			
			if ( pass == false ){
				toastr.error('Παρακαλώ κάντε αναζήτηση');
			}else{
				$scope.search_loading = true;
				getBooks({limit: 12, page: 1});
			}
		};
		
		$scope.secretDelete = function(id){
			if (confirm("Ειστε σίγουροι οτι θέλετε να διαγράψετε το μυστικό;")) {
				
		        SecretsService.deleteSecret({secretId:id}) 
				.success(function(data) {
					if ( data.success == true){
						toastr.success('Διαγραφτηκε επιτυχώς');
						window.location.replace('/secrets/all');
					} else{
						toastr.error(data.msg);
					}
				}).error(function(data) {
					toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
				});
				
		    }
		};
		
		$scope.selectAll = function(){
			if ( $('#selectAll').is(':checked') ){
				angular.forEach($scope.booksFound,function(value,index){
					if ( $.inArray( value.id, $scope.booksSecret) == -1 ){
						$scope.booksSecret.push(value.id);
						$scope.selectedBooks.push(value);
					}
	            });
			}else{
				angular.forEach($scope.booksFound,function(value,index){
					if ( $.inArray( value.id, $scope.booksSecret) != -1 ){
						$scope.selectedBooks.splice($scope.selectedBooks.indexOf(value), 1);
						$scope.booksSecret.splice($scope.booksSecret.indexOf(value.id), 1);
					}
	            });
			}
		};
		
		$scope.unselectAll = function(){
			angular.forEach($scope.selectedBooks,function(value,index){
				if ( $.inArray( value.id, $scope.booksSecret) != -1 ){
					$scope.booksSecret.splice($scope.booksSecret.indexOf(value.id), 1);
				}
            });
			$scope.selectedBooks = [];
			
			if ( $('#selectAll').is(':checked') ){ 
				$('#selectAll').prop('checked', false);
			}
		};
		
		$scope.pubLoadMore = function(){
			var pages = $scope.totalBooks/12;
			if ( Math.ceil(parseFloat(pages)) != pages ){
				pages = parseInt(pages) + 1;
			}
			
			if ( $scope.curPage + 1 <= pages ){
				$scope.curPage++;
				
				if ( !$scope.loadingMore && $scope.booksFound.length < $scope.totalBooks ) {
					getBooks({limit: 12, page: $scope.curPage});
				}
				
				$scope.loadingMore = true;
			}
		};
		
		window.onbeforeunload = function() {
		    if ( ($scope.booksSecret.length > 0 || $scope.title != '' || $scope.text != '') && savedStatus == false ) {
		    	if ( oldvalSec ){
		    		if ( oldvalSec.title != $scope.title || oldvalSec.text != $scope.text ){
		    			return 'All your changes will be lost.';
		    		}
		    	}else {
		    		return 'All your changes will be lost.';
		    	}
			}
		};
};

module.exports = AdvancedController;