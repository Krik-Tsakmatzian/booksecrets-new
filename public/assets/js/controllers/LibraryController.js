'use strict';

var LibraryController = /*@ngInject*/ function($scope, DashboardService, SecretsService, HelpService, toastr, $rootScope) {
		$scope.search_loading = false;
		$scope.isCollapsed = true;
		$scope.search = null;
		$scope.searchlist = null;
		$scope.filterTab = 'data';
		$scope.moreSearch = 6;
		$scope.totalSearch = 7;
		$scope.loading = true;
		$scope.bookslist = null;
		
		var limit = HelpService.param('limit');
		if (!limit) {
			limit = 6;
			$scope.moreFeeds = 0;
		} else {
			$scope.moreFeeds = limit;
		}

	  	$scope.showFilters = function() {
			$scope.filters = !$scope.filters;			
		};
		
		var order_by = HelpService.param('order_by');
		var order = HelpService.param('order');
		
		var bookslist;
		(bookslist = function(params){
	    	DashboardService.bookslist(params)
				.success(function(data) {
					if (data.success){
						$scope.bookslist = data.data;
						$scope.total = data.total;
						
						if ($scope.total <= 6) {
							$scope.moreFeeds = $scope.total;
						}
					}
					$scope.loading = false;
				})
				.error(function(err) {
					toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
				});
		})({ page : 1, limit: parseInt(limit) });
		

		$scope.pageChanged = function() {
			bookslist({ page: $scope.currentPage, limit: 6 });
		};
		

		$scope.loadMore = function (type) {
			$scope.loading = true;
			
			if (type === 'books') {
				$scope.moreFeeds = SecretsService.moreFeeds($scope.moreFeeds, $scope.total);
			
				HelpService.changeState('Library | Booksecrets', $scope.moreFeeds);
				
				bookslist({ page: 1, limit: $scope.moreFeeds });
			} else {
				if ($scope.moreSearch <= $scope.totalSearch){
					$scope.moreSearch = $scope.moreSearch + 6;
					$scope.bookSearch();
				}
			}
			
		};
		

		$scope.viewChange = function(view){
			$scope.visibleView = view;
		};
		

		$scope.changeFilterTab = function(tab){
			if ( tab === 'data' ){
				var limit = HelpService.param('limit');
				if (!limit){
					limit = 6;
				}
				bookslist({ page: 1, limit: parseInt(limit) });
				$scope.filterTab = 'data';
			} else if ( tab === 'search' && $scope.searchlist) {
				$scope.filterTab = 'search';
			}
		};
		
		$scope.bookSearch = function () {
			
			var pass = false;
			angular.forEach($scope.search,function(value,index){
                if ( index != 'limit' && index != 'page' ){
                	if ( value != '' && value != ' ' && typeof value != "undefined" && value != null ){
                		pass = true;
                	}
                }
            })
			
			if (!pass) {
				toastr.error('Παρακαλούμε είσαγεται κάτι.');
			} else {
				if ( $scope.moreSearch <= $scope.totalSearch ){
					$scope.search_loading = true;
					$scope.search.limit = $scope.moreSearch;
					$scope.search.page = 1;
					
					DashboardService.userBookSearch($scope.search)
						.success(function(data) {
							$scope.search_loading = false;
							$scope.loading = false;
							
							if (data.success && data.data.length > 0 ){								
								$scope.filterTab = 'search';
								$scope.searchlist = data.data;
								$scope.filters = false;
								$scope.totalSearch = data.total;								
							} else {
								$scope.searchlist = null;
								toastr.error('Δεν βρέθηκε αποτέλεσμα!');
							}
						})
						.error(function(data) {
							$scope.loading = false;
							$scope.search_loading = false;
							toastr.error('Δεν βρέθηκε αποτέλεσμα!');
						});	
				}
				
			}
			
		};
		
		$scope.libraryBookCtrl = function(book) {			
						

			if (book.userBookStatus === -1) {
				SecretsService.addBook({ 'bookId': book.id, 'userBookStatus': 0 })
					.success(function(data) {										
						book.userBookStatus = 0;
						if ($scope.searchlist !== null) {
							for (var i = 0; i < $scope.searchlist.length; i++) {
								if ($scope.searchlist[i].id === book.id) {
									$scope.searchlist[i].userBookStatus = 0;
								}
							}
						}	
						$scope.total++;
					})
					.error(function(err) {
						toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
					});
			} else {			
				SecretsService.removeBook({ 'bookId': book.id }) 
					.success(function(data) {					
						book.userBookStatus = -1;																	
						$scope.total--;						
					}).error(function(err) {
						toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
					});	
			}		
		};

	
};

module.exports = LibraryController;