'use strict';

var FooterController = /*@ngInject*/ function($scope, $http, $interval, toastr, $uibModal) {

        var modalInstance;
		$scope.open = function () {
			modalInstance = $uibModal.open({
				animation: true,
				size: 'lg',
				templateUrl: '/assets/templates/contactModal.html',
				controller:function($uibModalInstance ,$scope){
					$scope.cancel = function () {
						$uibModalInstance.dismiss('cancel');
					};
				}
			});
		};
		
		$scope.contact = {};
		$scope.message = function(){
			$http({
				method: 'POST',
				url: window.location.origin+'/contact-form',
				data: $.param($scope.contact),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			}).then(function successCallback(response) {
				if (response.data.success == true){
					$('#contactModal button[type="submit"]').after( "<p>Το μήνυμα σας στάλθηκε επιτυχώς!</p>" );
				}else{
					$('#contactModal button[type="submit"]').after( "<p>Κάποιο πρόβλημα παρουσιάστηκε. Παρακαλούμε δοκιμάστε αργότερα.</p>" );
				}
			}, function errorCallback(response) {
				$('#contactModal button[type="submit"]').after( "<p>Κάποιο πρόβλημα παρουσιάστηκε. Παρακαλούμε δοκιμάστε αργότερα.</p>" );
			});
		};
};

module.exports = FooterController;