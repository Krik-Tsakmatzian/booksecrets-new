'use strict';

var BookSecretsController = /*@ngInject*/ function($scope, DashboardService, SecretsService, toastr, $rootScope, $http,  $uibModal, $window) {

        var height = window.innerHeight
		|| document.documentElement.clientHeight
		|| document.body.clientHeight;
		
		$scope.height = height/2+'px';
		$scope.loading = false;
		
		var secrets = function(params) {
			$scope.loading = true;
	    	DashboardService.revealSecrets(params)
				.success(function(data) {
					if (data.success) {
						$scope.loading = false;
						$scope.bookInfo = data.data;
						$scope.secrets = data.data.secrets;
					}
				})
				.error(function(err) {
					toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
			});
		};

		$scope.dynamicPopover = {
			templateUrl: '/assets/templates/infoPopoverSingleSecret.html'
		};
		
		$scope.redirectToSecret = function(secret, iscollapsed) {
			if (secret.userLocked === 1) {
				return !iscollapsed;				
			}
			$window.location.href = '/secrets/' + secret.book.id + '/' + secret.id;
		}

		$scope.$watch('item', function() {			
		    secrets({ 'bookId': $scope.item });
		});
		
		$scope.hoveringOver = function(value) {
			$scope.overStar = value;
			$scope.percent = 100 * (value / 5);
		};
};

module.exports = BookSecretsController;