'use strict';

var SecretController = /*@ngInject*/ function($scope, DashboardService, SecretsService, toastr, $rootScope, $http) {
		$scope.overStar = 0;
        var height = window.innerHeight
		|| document.documentElement.clientHeight
		|| document.body.clientHeight;
		
		$scope.height = height/2+'px';
		
		var feedbooks;
		feedbooks = function(params){
	    	DashboardService.feedbook(params)
				.success(function(data) {
					if (data.success) {						
						$scope.feedbooks = data.data;					
					}
				})
				.error(function(err) {
					toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
				});
		};
		
		$scope.dynamicPopover = {
			templateUrl: '/assets/templates/infoPopoverSingleSecret.html'
		};
		
		$scope.isbn = [];
		$scope.$watch('item', function () {
		    if ($scope.item) {
				feedbooks({ 'itemId': $scope.item, 'page': 1, 'limit': 10  });
		    }
		});
		
		$scope.$watch('secret', function () {
		    if ($scope.secret) {				
				// FOO
		    }
		});

		$scope.hoveringOver = function(value) {
			$scope.overStar = value;
			$scope.percent = 100 * (value / 5);
		};

		$scope.report = function(secret) {
			SecretsService.report({ secretId: secret.id, flag: secret.userFlag })
				.success(function(data) {
					if (data.success) {
						toastr.success('H αναφορά έγινε με επιτυχία')					
					}
				})
				.error(function(err) {
					toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
				});
		}


		$scope.secretRate = function(id, overStar) {
			SecretsService.rate({ secretId: id, rating: overStar })
				.success(function(data) {
					if (data.success) {
						toastr.success('H Αξιολόγηση έγινε με επιτυχία')					
					}
				})
				.error(function(err) {
					toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
				});
		}
		
		
		$scope.unlockSecret = function() {
			var isbn = 	$scope.isbn[$scope.item];
			
			var found = false;
			var bookId = '';
			angular.forEach($scope.feedbooks, function(value, index) {
				if ((isbn === value.isbn10 || isbn === value.isbn13) && !found) {
					found = true;	
					bookId = value.id;
				}
            });
			
			if (found && bookId !== '') {
				var params = { 'bookId': bookId, 'userBookStatus': 0 };
				SecretsService.addBook(params) 
					.success(function(data) {
						if (data.success) {
							params.userBookStatus = 1;
							
							SecretsService.updateBook(params) 
								.success(function(data) {
									if (data.success) {
										toastr.success('Επιτυχία!');
										location.reload();
									} else {
										toastr.error(data.msg);
									}
									
								})
								.error(function(err) {
									toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
								});
						} else {
							if (data.msg === 'EXISTING-BOOK') {
								params.userBookStatus = 1;
								SecretsService.updateBook(params) 
									.success(function(data) {
										if (data.success) {
											toastr.success('Επιτυχία!');
											location.reload();
										} else {
											toastr.error(data.msg);
										}
										
									}).error(function(err) {
										toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
									});
							} else {
								toastr.error(data.msg);
							}
						}
						
					})
					.error(function(err) {
						toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
					});
			}else {
				toastr.error('Μη έγκυρο ISBN. Παρακαλώ βάλτε ένα νέο.');
			}
	    };
	    
	    $scope.isbnValidatorSecret = function(num) {
			if (!num) {
				return;
			}

			if (num.length === 10 || num.length === 13) {
				return true;
			}

			return "Μη έγκυρο ISBN. Το κωδικός ISBN πρέπει να είναι είτε 10 είτε 13 ψηφία";			
		};

		$scope.secretDelete = function(secret){
			if (confirm("Είστε σίγουροι οτι θέλετε να σβήσετε αυτό το μυστικό;")) {
				
		        SecretsService.deleteSecret({ secretId: secret.id }) 
					.success(function(data) {
						if (data.success){
							toastr.success('Διαγράφτηκε επιτυχώς');
							secret.status = 'deleted';
							window.location('/secrets/all');
						} else {
							if (data.msg === 'ACTIVE-SECRET') {
								toastr.error('Μόνο μυστικά με status "Προχειρο" μπορούν να διαγραφούν. Αν θες να διαγράψεις το μυστικό, πάτα επεξεργασία και τροποποίησε την κατάσταση του μυστικόυ σε "Πρόχειρο"');
								return;
							}
							toastr.error(data.msg);
						}
					}).error(function(err) {
						toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
					});
				
		    }
		};
};

module.exports = SecretController;