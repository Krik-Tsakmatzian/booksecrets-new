'use strict';


var mainController = /*@ngInject*/ function($scope, $http, $interval, AuthService, toastr, $rootScope, $uibModal, $timeout) {
        $scope.time = new Date();
		$scope.loginvar = false;
		$scope.loginUser = {};
		$scope.loginUser.remember = false;
		$rootScope.localstorageSup = false;
		$rootScope.cookieSup = false;
		var slides = [
			"/dist/img/home-slides/1.jpg",
			"/dist/img/home-slides/2.jpg",
			"/dist/img/home-slides/3.jpg",
			"/dist/img/home-slides/4.jpg"
		];
		$scope.background = slides[0];
		$scope.tabs = [
			{ id: 1, title: 'Αναγνώστης' },
			{ id: 2, title: 'Συγγραφέας' },
			{ id: 3, title: 'Εκδότης' },
			{ id: 4, title: 'Login' }
		];
		$scope.status = {
			isopen: false
		};
		$scope.stepAuthor = 1;
		$scope.stepPublisher = 1;
		$scope.credentials = {};
		$scope.author = {};
		$scope.publisher = {};
		$scope.author.bookAccept = 0;
		$scope.publisher.bookAccept = 0;

		$scope.getRes = function(res){
			$scope.result = res;
			$scope.stepAuthor = 3;
		};
		
		if ( $('#player1').length ){
			var iframe = $('#player1')[0];
			var player = $f(iframe);
	    	
			player.addEvent('ready', function() {
				player.addEvent('finish', onFinish);
			});
		}
		if ( $('#player2').length ){
			var iframewho = $('#player2')[0];
			var playerwho = $f(iframewho);
	    	
			playerwho.addEvent('ready', function() {
				playerwho.addEvent('finish', onFinishWho);
			});
		}

		$scope.play = function(){
	    	$scope.player = true;
			player.api('play');
	    };
	    
	    function onFinish(id) {
			$scope.player = false;
		}
		
		$scope.playwho = function(){
	    	$scope.playerwho = true;
			playerwho.api('play');
	    };
		
		function onFinishWho(id) {
			$scope.playerwho = false;
		}
		
		var slideMove = function(){
	    	var pos = slides.indexOf($scope.background);
			if ( $scope.background == slides[3] ){
			  	$scope.background = slides[0];
			}
			else{
			  	$scope.background = slides[pos + 1];
			}
			$scope.$apply();
		};
		
		$("#owl-news").owlCarousel({
			navigation : true,
			pagination: true,
			slideSpeed : 300,
			paginationSpeed : 400,
			autoPlay: 8000,
			singleItem:true,
			afterMove: function(){
				slideMove();
			}
		});
		

		$scope.login = function () {
			var user = {
				email: $scope.loginUser.email,
				pwd: $scope.loginUser.pwd,
				remember: $scope.loginUser.remember
			};
			
			AuthService.login(user)
				.success(function(data) {
					if (data.success){
						window.location.replace(data.data.role === 'MOBUSER' ? '/user/books' : '/secrets/feed');
					} else {
						toastr.error('Η σύνδεση απέτυχε');
					}
				})
				.error(function(err) {
					toastr.error('Η σύνδεση απέτυχε');
				});
	    };
		
		

		$scope.register = function () {
			var user = {
				name: $scope.credentials.name,
				email: $scope.credentials.email,
				pwd: $scope.credentials.pwd
			};
			
			AuthService.register(user)
				.success(function(data) {
					if (data.success){
						window.location.replace('/secrets/feed');
					} else {
						toastr.error(data.msg);
					}
				})
				.error(function(err) {
					toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
				});
				
	    };
	    
    	$scope.passwordValidator = function(password) {
			if (!password) { 
				return false;
			}
			
			if (password.length < 6) {
				return "Ο κωδικός πρέπει να είναι τουλάχιστον " + 6 + " χαρακτήρες";
			}

			if (!password.match(/[A-Z]/)) {
				return "Ο κωδικός πρέπει να έχει τουλάχιστον ένα κεφαλαίο γράμμα";
			}

			if (!password.match(/[0-9]/)) {
				return "Ο κωδικός πρέπει να έχει τουλάχιστον ένα νούμερο";
			}
			
			return true;
		};
		

		$scope.registerAdv = function (type) {
			
			if ( type == 'author' ){
				var user = {
					username: $scope.author.name,
					email: $scope.author.email,
					pwd: $scope.author.pwd,
					firstName: $scope.author.first_name,
					lastName: $scope.author.last_name,
					authorId: $scope.author.bookId
				};
			}else{
				var user = {
					username: $scope.publisher.username,
					email: $scope.publisher.email,
					pwd: $scope.publisher.pwd,
					name: $scope.publisher.name,
					contactPerson: $scope.publisher.contactPerson,
					publisherId: $scope.publisher.bookId
				};
			}
			
			if ( $('input[name="bookSelect"]').val() != 0 ){
				
				if ( type === 'author' && (!$scope.author.bookId || $scope.author.bookId == false || $scope.author.bookId==null) ){
					toastr.error('Παρακαλώ επιλέξτε βιβλίο.');
				} else if ( type === 'publisher' && (!$scope.publisher.bookId || $scope.publisher.bookId == false || $scope.publisher.bookId==null) ){
					toastr.error('Παρακαλώ επιλέξτε βιβλίο.');
				} else {
					AuthService.register(user)
						.success(function(data) {
						if (data.success){
							window.location.replace('/secrets/feed');
						} else {
							var id = type === 'author' ? user.authorId : user.publisherId;						
							if (data.msg === 'EXISTING-AUTHOR' || data.msg === 'EXISTING-PUBLISHER'){
								toastr.info('Το συγκεκριμένο βιβλιο, σχετίζεται ήδη με ένα λογαριασμό χρήστη. Αν θέλετε, να διεκδικήσετε τον συγκεκριμένο λογαριασμό, ενημερώστε μας \
									<a href="' + window.location.origin + '/user/claim-account?id=' + id + '&type=' + type + '' + '" target="_self" \
									style="color: #fff; font-weight: 600; border-bottom: 1px solid #fff;">εδώ</a>.', {
										allowHtml: true,
										autoDismiss: false,
										closeButton: true,
										timeOut: 0,
										extendedTimeOut: 0,
										tapToDismiss: false,
										toastClass: 'toast-top-center',
										closeHtml: '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
									});
							} else {
								toastr.error(data.msg);
							}
						}
					})
					.error(function(err) {
						toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
					});
				}
			}

	    };
	    
		$scope.nextStep = function(type){
			var step;
			var mail;
			if ( type === 'author' ){
				step = $scope.stepAuthor;
				mail = { email : $scope.author.email };
			} else {
				step = $scope.stepPublisher;	
				mail = { email : $scope.publisher.email };
			}
			
			if (step === 1) {
				
				AuthService.validateEmail(mail)
					.success(function(data) {
						if (data.success) {
							if (type === 'author') {
								$scope.stepAuthor++;
							} else {
								$scope.stepPublisher++;	
							}
						} else {
							toastr.error('Το συγκεκριμένο Email χρησιμοποιείται.');
						}
					})
					.error(function(err) {
						toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
					});
			} else if ( step < 4 ) {
				if ( type === 'author' ){
					$scope.stepAuthor++;
				}else{
					$scope.stepPublisher++;	
				}
			}
		};
		
		var validateBook = function(type, query){
			if (type === 'author') {
				$scope.loading = true;
				$scope.error = false;
				$scope.resultBooklist = false;
				
				AuthService.validateBook(query)
					.success(function(data) {
						if (data.success) {
							$scope.loading = false;
							
							if ( $scope.stepAuthor === 3 ){
								$scope.stepAuthor++;
							}
							
							if ( data.total === 0 ) {
								$scope.error = true;
								$scope.resultBooklist = false;
							} else {
								$scope.error = false;
								$scope.resultBooklist = data.data;	
							}
						} else {
							toastr.error('Δεν βρέθηκε αποτέλεσμα!');
							$scope.loading = false;
						}
						
					})
					.error(function(err) {
						// FOO
					});	
			} else {
				$scope.loadingPub = true;
				$scope.errorPub = false;
				$scope.resultBooklistPub = false;
				
				AuthService.validateBook(query)
					.success(function(data) {
						if (data.success) {
							$scope.loadingPub = false;
							
							if ($scope.stepPublisher === 3) {
								$scope.stepPublisher++;
							}
							
							if (data.total === 0) {
								$scope.errorPub = true;
								$scope.resultBooklistPub = false;
							} else {
								$scope.error = false;
								$scope.resultBooklistPub = data.data;	
							}
						} else {
							toastr.error('Δεν βρέθηκε αποτέλεσμα!');
							$scope.loadingPub = false;
						}
					})
					.error(function(err) {
						// FOO
					});	
			}
		    
		};
		
		$scope.bookSearch = function(type){
			$scope.selectedIndex = null;
			
			if (type === 'author') {
				$scope.author.bookId = false;
				angular.element('#bookAuthorID').val('');
				var query = { query : $scope.author.search };
			
				if (query && typeof $scope.author.search != 'undefined'){
					validateBook('author', query);
				}
			} else {
				$scope.publisher.bookId = false;
				angular.element('#bookPublisherID').val('');
				var query = { query : $scope.publisher.search };
				
				if (query && typeof $scope.publisher.search != 'undefined'){
					validateBook('publisher', query);
				}
			}
			
		};
		
		$scope.selectBook = function(id, $index, type){
			
			if (type === 'author'){
				if (id && id !== ''){
					$scope.author.bookId = id;
					$scope.selectedIndex = $index;
					angular.element('#bookAuthorID').val(id);
				} else {
					toastr.error('Το id του Συγγραφέα δεν είναι διαθέσιμο!');
				}
			} else {
				if (id && id !== '') {
					$scope.publisher.bookId = id;
					$scope.selectedIndexPub = $index;
					angular.element('#bookPublisherID').val(id);
				} else {
					toastr.error('Το id του Εκδότη δεν είναι διαθέσιμο!');
				}
			}
		};
		
		$scope.prevStep = function(type){
			if (type === 'author') {
				$scope.stepAuthor--;
			} else {
				$scope.stepPublisher--;	
			}
		};
		
		$scope.stepValidate = function(id, type){
			if (type === 'author') {
				if (angular.element('#step' + id).hasClass('has-error') || angular.element('#step' + id).children().hasClass('ng-invalid-required')) {
					return true;
				}
				return false;
			} else {
				if ( angular.element('#stepPub' + id).hasClass('has-error') || angular.element('#stepPub' + id).children().hasClass('ng-invalid-required')) {
					return true;
				}
				return false;
			}
		};
		
		var modalInstance;
		$scope.openTerms = function (type, $event) {
			$event.preventDefault();

			modalInstance = $uibModal.open({
				animation: true,
				size: 'md',
				templateUrl: '/assets/templates/terms'+type+'Modal.html',
				controller:function($uibModalInstance ,$scope){
					$scope.cancel = function () {
						$uibModalInstance.dismiss('cancel');
					};
					
					$scope.disagree = function() {
						$uibModalInstance.dismiss('cancel');
						angular.element($('input[name="bookSelect"]')).prop( "checked", false ).val(0).triggerHandler('input');
					};
					
					$scope.agree = function() {
						$uibModalInstance.dismiss('cancel');
						angular.element($('input[name="bookSelect"]')).prop( "checked", true ).val(1).triggerHandler('input');
					};
				}
			});
		};
};

module.exports = mainController;