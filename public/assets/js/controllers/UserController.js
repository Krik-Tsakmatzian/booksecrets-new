'use strict';

var UserController = /*@ngInject*/ function($scope, toastr, $http, HelpService) {
	$scope.forgotUser = {};
	$scope.reset = {};
	
	$scope.forgotAccount = function() {
		var params = { email: $scope.forgotUser.email };
		
		$http({
			method: 'POST',
			url: window.location.origin + '/api/forgot-password',
			data: $.param(params),
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
		})
		.then(
			function successCallback(response) {
				if (response.data.success) {
					toastr.success('Παρακαλώ ελέγξτε τα emails για να συνεχίσετε.');
				} else {
					if (response.data.msg === 'FB-LOGIN'){
						toastr.info('Χρησιμοποιήσατε τον λογαριασμόυ του facebook σας για να συνδεθείτε.', { timeOut: 8000, extendedTimeOut: 5000 });
					} else if (response.data.msg === 'UNKNOWN-EMAIL'){
						toastr.error('Άγνωστος χρήστης.', { timeOut: 8000, extendedTimeOut: 5000 });
					} else {
						toastr.error(response.data.msg);
					}
				}
			}, function errorCallback(response) {
				toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
			}
		);
	};
	
	$scope.resetPassword = function() {
		var reset = HelpService.param('reset');
		var params = { pwd: $scope.reset.pwd, reset: reset };
		
		$http({
			method: 'POST',
			url: window.location.origin + '/api/reset-password',
			data: $.param(params),
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
		})
		.then(
			function successCallback(response) {
				if (response.data.success) {
					toastr.success('Ο κωδικός σας έχει αλλαχτεί.');
					setTimeout(function(){ window.location.replace('/user/login'); }, 2000);
				} else {
					if (response.data.msg === 'RESET-INVALID'){
						toastr.error('Invalid reset token. Please repeat the process.', { timeOut: 8000, extendedTimeOut: 5000 });
					} else {
						toastr.error(response.data.msg);
					}
				}
			}, 
			function errorCallback(response) {
				toastr.error('Error. Please try again later.');
			}
		);
	};
	
	$scope.claimAccount = function(){
		var id = HelpService.param('id');
		var type = HelpService.param('type');
		var params = $scope.claimUser;
		params.id = id;
		params.type = type;
		
		$http({
			method: 'POST',
			url: window.location.origin + '/api/claim-account',
			data: $.param(params),
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
		})
		.then(
			function successCallback(response) {
				if (response.data.success) {
					toastr.success('To μηνυμά σας έχει σταλεί επιτυχώς. Θα επικοινωνίσουμε μαζί σας σύντομα.');
				} else {
					toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
				}
			}, 
			function errorCallback(response) {
				toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
			}
		);
	};
	
	$scope.passwordValidator = function(password) {
		if (!password) { 
			return false;
		}
		
		if (password.length < 6) {
			return "Ο κωδικός θα πρέπει να έχει μέγεθος τουλάχιστον " + 6 + " χαρακτήρων";
		}
		if (!password.match(/[A-Z]/)) {
			 return "Ο κωδικός θα πρέπει να έχει τουλάχιστον ένα κεφαλαίο γράμμα";
		}
		if (!password.match(/[0-9]/)) {
			 return "Ο κωδικός θα πρέπει να έχει τουλάχιστον ένα νούμερο";
		}
		
		return true;
	};
	
};

module.exports = UserController;