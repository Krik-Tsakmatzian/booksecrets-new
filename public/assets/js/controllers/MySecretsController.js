'use strict';

var MySecretsController = /*@ngInject*/ function($scope, DashboardService, SecretsService, HelpService, toastr, $rootScope, $http) {
		$scope.isCollapsed = true;
		$scope.booksSecret = [];
		var	History = window.History;
		$scope.secretslist = null;
		$scope.loading = true;
		
		var limit = HelpService.param('limit');
		if (!limit) {
			limit = 6;
			$scope.moreFeeds = 0;
		} else {
			$scope.moreFeeds = limit;
		}
			
		var filter = { page: 1, limit: parseInt(limit) };
		var type = HelpService.param('type');
		$scope.chosenType = 'all';
		
		if ( type && type === 'published' ){
			filter.status = 1; 
			$scope.chosenType = 'published';
		} else if ( type && type === 'draft' ){
			filter.status = 0;
			$scope.chosenType = 'draft';
		}
		
		$scope.order_item = 'date';
		var order = HelpService.param('order');
		switch (order) {
			case 'asc':
				$scope.order = 'asc';
				filter.orderDir = 'asc';
				break; 
			default: 
				$scope.order = 'desc';
				filter.orderDir = 'desc';
		}
			
		var secretslist;
		(secretslist = function(params){
	    	SecretsService.mySecrets(params)
				.success(function(data) {
					if (data.success) {
						$scope.secretslist = data.data;
						$scope.total = data.total;
						if ( $scope.total <= 6){
							$scope.moreFeeds = $scope.total;
						}
					}
					
					$scope.loading = false;
				})
				.error(function(data) {
					toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
				});
		})( filter );
		
		$scope.loadMore = function () {
			$scope.loading = true;
			$scope.moreFeeds = SecretsService.moreFeeds($scope.moreFeeds, $scope.total);
			
			var params = { page: 1, limit: $scope.moreFeeds };
			
			if ( $scope.chosenType === 'published' ) {
				params.status = 1; 
			} else if ( $scope.chosenType === 'draft' ) {
				params.status = 0;
			}
			
			params.orderDir = $scope.order == 'asc' ? 'asc' : 'desc';		
			
			var url = '?limit=' + $scope.moreFeeds + '&order=' + $scope.order + '&type=' + $scope.chosenType + '';
			History.pushState({ timestamp: (new Date().getTime()) }, 'My secrets | booksecrets', window.location.pathname + url);
			secretslist(params);
		};

		$scope.redirectToSecret = function(secret, iscollapsed) {
			if (secret.locked === 1) {
				return !iscollapsed;				
			}
			window.location.href = '/secrets/' + secret.book.id + '/' + secret.id;
		}
		
		$scope.sort = function(){
			if ($scope.moreFeeds === 0 || $scope.moreFeeds < 6) {
				$scope.moreFeeds = 6;
			}			
				
			var filter = { page: 1, limit: parseInt($scope.moreFeeds) };
			
			if ($scope.order == 'asc') {
				$scope.order = 'desc';
				filter.orderDir = 'desc';
			} else {
				$scope.order = 'asc';
				filter.orderDir = 'asc';
			}
			
			if ( $scope.chosenType && $scope.chosenType === 'published') {
				filter.status = 1; 
			} else if ($scope.chosenType && $scope.chosenType === 'draft') {
				filter.status = 0;
			}
			
			$scope.secretslist = null;
			$scope.total = null;
			$scope.loading = true;
				
			var url = '?limit=' + $scope.moreFeeds + '&order=' + $scope.order + '&type=' + $scope.chosenType + '';
			History.pushState({ timestamp: (new Date().getTime()) }, 'My secrets | booksecrets', window.location.pathname + url);
			secretslist(filter);
		};
		
		$scope.showType = function(type){
			$scope.chosenType = type;

			if ($scope.moreFeeds === 0 || $scope.moreFeeds < 6) {
				$scope.moreFeeds = 6;
			}				
			
			var filter = { page: 1, limit: parseInt($scope.moreFeeds) };
			
			if (type && type === 'published') {
				filter.status = 1; 
				$scope.chosenType = 'published';
			} else if (type && type === 'draft') {
				filter.status = 0;
				$scope.chosenType == 'draft';
			} else {
				$scope.chosenType == 'all';
			}
			
			$scope.secretslist = null;
			$scope.total = null;
			$scope.loading = true;
			
			secretslist(filter);
			var url = '?limit=' + $scope.moreFeeds + '&order=' + $scope.order + '&type=' + $scope.chosenType + '';
			History.pushState({timestamp: (new Date().getTime())}, 'My secrets | booksecrets', window.location.pathname+url);
		};
		
		$scope.secretDelete = function(secret){
			if (confirm("Είστε σίγουροι οτι θέλετε να σβήσετε αυτό το μυστικό;")) {
				
		        SecretsService.deleteSecret({secretId:secret.id}) 
					.success(function(data) {
						if (data.success){
							toastr.success('Διαγράφτηκε επιτυχώς');
							secret.status = 'deleted';
						} else {
							toastr.error(data.msg);
						}
					}).error(function(err) {
						toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
					});
				
		    }
		};
};

module.exports = MySecretsController;