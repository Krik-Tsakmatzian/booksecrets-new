'use strict';

var MobUserController = /*@ngInject*/ function($scope, DashboardService, Upload, $timeout, $rootScope) {

        $scope.edit = false;
        $scope.editSocial = false;
		$scope.id = false;
		$scope.user = {};
		$scope.profileImage = '';
		
		$(".profile-carousel").owlCarousel({
			items : 5,
			navigation: true,
			pagination: false
		});
		
		var profile = function(params) {
			DashboardService
				.profileUpdate(params)
				.success(function(data) {
					// Success response
					console.log('success user image upload');
				})
				.error(function(err) {
					// err response
					console.log('error user image upload');
				});
		};
		
		$scope.uploadFiles = function(file, errFiles) {
			$scope.f = file;
			$scope.errFile = errFiles && errFiles[0];
			
			if (file) {
				file.upload = Upload.upload({
					url: 'https://booksecrets.com/api/upload', // window.location.origin + '/api/upload',
					data: { 
						file: file, 
						type: 'PROFILE_IMAGE' 
					}
				});
				
				file.upload
					.then(function (response) {
						$timeout(function () {
							$scope.profileImage = response.data.data.fileurl;
							profile({ imagefilecode:  response.data.data.filecode });
						});
					}, function (response) {
						if (response.status > 0){
							$scope.profileImage = response.data.data.fileurl;
							profile({ imagefilecode:  response.data.data.filecode });
						}
					}, function (evt) {
						// file.progress = Math.min(100, parseInt(100.0 * 
						// evt.loaded / evt.total));
					});
			}   
	    };
	    
		$scope.update = function(info){
			$scope.edit = false;
			
			$scope.user.summary = info;
			
			if ( info !== '' ){
				profile($scope.user);
			}
		};
		
		$scope.updateSocial = function(){
			$scope.editSocial = false;
			
			if ( $scope.user.youtube && $scope.user.youtube !='' ){
				if ( $scope.user.youtube.indexOf("http://") == -1 && $scope.user.youtube.indexOf("https://") == -1 ){
					$scope.user.youtube = 'http://'+$scope.user.youtube;
				}
			}
			if ( $scope.user.twitter && $scope.user.twitter !=''){
				if ( $scope.user.twitter.indexOf("http://") == -1 && $scope.user.twitter.indexOf("https://") == -1 ){
					$scope.user.twitter = 'http://'+$scope.user.twitter;
				}
			}
			if ( $scope.user.facebook && $scope.user.facebook !=''){
				if ( $scope.user.facebook.indexOf("http://") == -1 && $scope.user.facebook.indexOf("https://") == -1 ){
					$scope.user.facebook = 'http://'+$scope.user.facebook;
				}
			}
			if ( $scope.user.website && $scope.user.website !=''){
				if ( $scope.user.website.indexOf("http://") == -1 && $scope.user.website.indexOf("https://") == -1 ){
					$scope.user.website = 'http://'+$scope.user.website;
				}
			}
			
			profile($scope.user);
		};
		
		$scope.$watch('id', function () {
			
			if ($scope.id) {
				DashboardService.profileInfo({ id: $scope.id })
					.success(function(data) {
						if (data.success) {
							$scope.name = data.data.name;
							var media = [];							
							$scope.user.summary = data.data.summary !== null
								? data.data.summary 
								: '';
							$scope.profileImage = data.data.profileImage !== null
								? $rootScope.base_url + data.data.profileImage.filePath
								: window.location.origin + '/dist/img/user.jpg';
							media = data.data.media;
							
							
							if (media){
								
								$.each( media, function( key, value ) {
									if (value.type == 'YOUTUBE') {
										if (value.value.indexOf("http://") === -1 && value.value.indexOf("https://") === -1) {
											$scope.user.youtube = 'http://' + value.value;
										} else {
											$scope.user.youtube = value.value;
										}
									} else if (value.type == 'FACEBOOK') {
										if (value.value.indexOf("http://") === -1 && value.value.indexOf("https://") === -1) {
											$scope.user.facebook = 'http://' + value.value;
										} else {
											$scope.user.facebook = value.value;
										}
									} else if (value.type == 'TWITTER'){
										if (value.value.indexOf("http://") === -1 && value.value.indexOf("https://") === -1) {
											$scope.user.twitter = 'http://' + value.value;
										} else {
											$scope.user.twitter = value.value;
										}
									} else if (value.type == 'WEBSITE'){
										if ( value.value.indexOf("http://") === -1 && value.value.indexOf("https://") === -1) {
											$scope.user.website = 'http://' + value.value;
										} else {
											$scope.user.website = value.value;
										}
									}
									
								});
							}
							
						}
						
					})
					.error(function(data) {
						// FOO
					});
			}
		});
};

module.exports = MobUserController;
