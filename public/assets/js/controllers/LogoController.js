'use strict';

var LogoController = /*@ngInject*/ function($scope, $http, $interval, toastr, $uibModal) {

        var modalInstance;
		$scope.openmodal = function () {
			modalInstance = $uibModal.open({
				animation: true,
				size: 'lg',
				templateUrl: '/assets/templates/newspaper.html',
				controller:function($uibModalInstance ,$scope){
					$scope.cancel = function () {
						$uibModalInstance.dismiss('cancel');
					};
				}
			});
		};
};

module.exports = LogoController;