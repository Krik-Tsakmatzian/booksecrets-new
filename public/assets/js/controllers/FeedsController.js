'use strict';

var FeedsController = /*@ngInject*/ function($scope, $location, DashboardService, SecretsService, HelpService, toastr, $rootScope, $http, $uibModal, $window) {
		$scope.overStar = 0;
        var	History = window.History;
		$scope.feedlist = null;
		$scope.total = null;
		$scope.loading = true;
		$scope.search_loading = false;
		$scope.query = '';
		$scope.old_query = '';
		$scope.search_res = false;
		$scope.has_search = false;
        
		var limit = HelpService.param('limit');
		if (!limit) {
			limit = 6;
			$scope.moreFeeds = 0;
		} else {
			$scope.moreFeeds = limit;
		}
		
		var filter = { page: 1, limit: parseInt(limit) };
		var type = HelpService.param('type');
		$scope.chosenType = 'all';
		
		if ( type && type === 'booksecrets' ){
			filter.type = 'BOOKSECRETS'; 
			$scope.chosenType = 'booksecrets';
		}
		else if ( type && type === 'authors' ){
			filter.type = 'AUTHOR';
			$scope.chosenType = 'authors';
		}
		else if ( type && type === 'publishers' ){
			filter.type = 'PUBLISHER';
			$scope.chosenType = 'publishers';
		}
		
		$scope.order_item = 'date';
		var order = HelpService.param('order');
		switch (order) {
			case 'asc':
				$scope.order = 'asc';
				filter.orderDir = 'asc';
				break; 
			default: 
				$scope.order = 'desc';
				filter.orderDir = 'desc';
		}
		
		var feedlist;
		(feedlist = function(params){
	    	DashboardService.feedlist(params)
				.success(function(data) {
					if (data.success) {
						$scope.feedlist = data.data;
						$scope.total = data.total;
						
						if ($scope.total <= 6) {
							$scope.moreFeeds = $scope.total;
						}
					}
					
					$scope.loading = false;
				})
				.error(function(err) {
					toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
				});
		})( filter );
		
		$scope.dynamicPopover = {
			templateUrl: '/assets/templates/infoPopover.html'
		};
		
		$scope.hoveringOver = function(value) {
			$scope.overStar = value;
			$scope.rating = value;
			$scope.percent = 100 * (value / 5);
		};
		
		$scope.loadMore = function () {
			$scope.loading = true;
			$scope.moreFeeds = SecretsService.moreFeeds($scope.moreFeeds, $scope.total);
			var params = { page: 1, limit: $scope.moreFeeds };
		
			if ( $scope.chosenType && $scope.chosenType == 'booksecrets' ){
				params.type = 'BOOKSECRETS'; 
			} else if ( $scope.chosenType && $scope.chosenType == 'authors' ){
				params.type = 'AUTHOR';
			} else if ( $scope.chosenType && $scope.chosenType == 'publishers' ){
				params.type = 'PUBLISHER';
			}
			
			params.orderDir = $scope.order == 'asc' ? 'asc' : 'desc';			
			
			var url = '?limit=' + $scope.moreFeeds + '&order=' + $scope.order + '&type=' + $scope.chosenType + '';
			History.pushState({ timestamp: (new Date().getTime()) }, 'Feed | booksecrets', window.location.pathname+url);
			
			if ($scope.has_search && $scope.search_res) {
				params.query = $scope.query;
				$scope.search_loading = true;
				feedsearch(params);
			} else {
				feedlist(params);
			}
		};
		
		$scope.sort = function(){
			if ($scope.moreFeeds === 0 || $scope.moreFeeds < 6) {
				$scope.moreFeeds = 6;
			}
			
			var filter = { page: 1, limit: parseInt($scope.moreFeeds) };

			$scope.order = $scope.order === 'asc' ? 'desc' : 'asc';
			filter.orderDir = $scope.order === 'asc' ? 'desc' : 'asc';			
			
			if ( $scope.chosenType && $scope.chosenType == 'booksecrets' ){
				filter.type = 'BOOKSECRETS'; 
			} else if ( $scope.chosenType && $scope.chosenType == 'authors' ){
				filter.type = 'AUTHOR';
			} else if ( $scope.chosenType && $scope.chosenType == 'publishers' ){
				filter.type = 'PUBLISHER';
			}
			
			$scope.feedlist = null;
			$scope.total = null;
			$scope.loading = true;
				
			var url = '?limit=' + $scope.moreFeeds + '&order=' + $scope.order + '&type=' + $scope.chosenType + '';
			History.pushState({ timestamp: (new Date().getTime()) }, 'Feed | booksecrets', window.location.pathname + url);
			
			if ($scope.has_search && $scope.search_res) {
				filter.query = $scope.query;
				$scope.search_loading = true;
				feedsearch(filter);
			} else {
				feedlist(filter);
			}
		};
		
		$scope.showType = function(type) {
			if ($scope.moreFeeds === 0 || $scope.moreFeeds < 6) {
				$scope.moreFeeds = 6;
			}
						
			var filter = { page: 1, limit: parseInt($scope.moreFeeds) };
			
			if ( type && type == 'booksecrets' ){
				filter.type = 'BOOKSECRETS'; 
				$scope.chosenType = 'booksecrets';
			} else if ( type && type == 'authors' ){
				filter.type = 'AUTHOR';
				$scope.chosenType = 'authors';
			} else if ( type && type == 'publishers' ){
				filter.type = 'PUBLISHER';
				$scope.chosenType = 'publishers';
			} else {
				$scope.chosenType = 'all';
			}
			
			$scope.feedlist = null;
			$scope.total = null;
			$scope.loading = true;
			
			var url = '?limit=' + $scope.moreFeeds + '&order=' + $scope.order + '&type=' + type + '';
			History.pushState({ timestamp: (new Date().getTime()) }, 'Feed | booksecrets', window.location.pathname+url);
			
			if ($scope.has_search && $scope.search_res) {
				filter.query = $scope.query;
				$scope.search_loading = true;
				feedsearch(filter);
			} else {
				feedlist(filter);
			}
		};
		
		var feedsearch;
		feedsearch = function(params){
	    	DashboardService.feedsearch(params)
				.success(function(data) {
					$scope.old_query = params.query;
					if (data.success){
						$scope.search_res = true;
						$scope.feedlist = data.data;
						$scope.total = data.total;
						
						if ( $scope.total <= 6){
							$scope.moreFeeds = $scope.total;
						}
					}
					
					$scope.search_loading = false;
					$scope.loading = false;
				})
				.error(function(err) {
					toastr.error('Σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
				});
		};
		
		$scope.feedSearch = function (){
			if (!$scope.query || $scope.query == ''){
				toastr.error('Παρακαλούμε είσαγεται κάτι.');
			}
			else {
				var filter = { page: 1, limit: 6, query: $scope.query };
				$scope.search_loading = true;
				$scope.chosenType = 'all';
				$scope.order = 'desc';
				History.pushState({timestamp: (new Date().getTime())}, 'Feed | booksecrets', window.location.pathname);
				feedsearch(filter);
			}
		};
		
		$scope.clearSearch = function(){
			$scope.search_res = false;
			$scope.loading = true;
			$scope.feedlist = null;
			$scope.chosenType = 'all';
			$scope.order = 'desc';
			$scope.query = '';
			$scope.old_query = '';
			History.pushState({ timestamp: (new Date().getTime()) }, 'Feed | booksecrets', window.location.pathname);
			feedlist({ page: 1, limit: 6 });
		};
		
		$scope.$watch('query', function () {
			if ( $scope.query === '' || !$scope.query ){
				$scope.has_search = false;
			}else {
				$scope.has_search = true;
			}
		});

		$scope.redirectToSecret = function(secret, iscollapsed) {
			if (secret.userLocked === 1) {
				return !iscollapsed;				
			}
			$window.location.href = '/secrets/' + secret.book.id + '/' + secret.id;
		}
				
};

module.exports = FeedsController;
