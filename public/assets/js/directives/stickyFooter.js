'use strict';

var stickyFooter = function($timeout) { 
    return {
        restrict: 'A',
        link: function(scope, iElement, iAttrs) {
            var stickyFooterWrapper = $(iAttrs.stickyFooter);
            stickyFooterWrapper.parents().css('height', '100%');
            stickyFooterWrapper.css({
                'min-height': '100%',
                'height': 'auto'
            });

            var stickyFooterPush = $('<div class="push"></div>');
            stickyFooterWrapper.append(stickyFooterPush);

            var setHeights = function() {
                var height = iElement.outerHeight();
                stickyFooterPush.height(height);
                stickyFooterWrapper.css('margin-bottom', -(height-100));
            };

            $timeout(setHeights, 0);
            $(window).on('resize', setHeights);
        }
    };
};

module.exports = stickyFooter;