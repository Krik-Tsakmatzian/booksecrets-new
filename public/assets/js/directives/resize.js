'use strict';

var resize = function($window) { 
    return function(scope, element) {
        var w = angular.element($window);
        scope.getWindowDimensions = function() {
            return { 'h': w.height() };
        };
        scope.$watch(scope.getWindowDimensions, function(newValue, oldValue) {
            scope.windowHeight = newValue.h;
        
            scope.style = function() {
                return { 
                    'height': (newValue.h - 36) + 'px'
                };
            };
        
        }, true);
    
        w.bind('resize', function() {
            scope.$apply();
        });
    };
};

module.exports = resize;