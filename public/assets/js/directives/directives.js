'use strict';

/* Directives */


angular.module('directive', [])
	.directive('resize', function ($window) {
        return function (scope, element) {
            var w = angular.element($window);
            scope.getWindowDimensions = function() {
                return { 'h': w.height() };
            };
            scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
                scope.windowHeight = newValue.h;
            
                scope.style = function() {
                    return { 
                        'height': (newValue.h - 36) + 'px'
                    };
                };
            
            }, true);
        
            w.bind('resize', function() {
                scope.$apply();
            });
        };
    })
    .directive('toggleClass', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.bind('click', function() {
                    if (element.attr("class") === "glyphicon glyphicon-pencil") {
                        element.removeClass("glyphicon glyphicon-pencil");
                        element.addClass(attrs.toggleClass);
                    } else {
                        element.removeClass("glyphicon glyphicon-ok");
                        element.addClass("glyphicon glyphicon-pencil");
                    }
                });
            }
        };
    })
    .directive('stickyFooter', [
        '$timeout',
        function ($timeout) {
            return {
                restrict: 'A',
                link: function(scope, iElement, iAttrs) {
                    var stickyFooterWrapper = $(iAttrs.stickyFooter);
                    stickyFooterWrapper.parents().css('height', '100%');
                    stickyFooterWrapper.css({
                        'min-height': '100%',
                        'height': 'auto'
                    });

                    var stickyFooterPush = $('<div class="push"></div>');
                    stickyFooterWrapper.append(stickyFooterPush);

                    var setHeights = function() {
                        var height = iElement.outerHeight();
                        stickyFooterPush.height(height);
                        stickyFooterWrapper.css('margin-bottom', -(height-100));
                    };

                    $timeout(setHeights, 0);
                    $(window).on('resize', setHeights);
                }
            };
        }
    ])
    .directive('setClassWhenAtTop', function($window) {
        var $win = angular.element($window); // wrap window object as jQuery object
    
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var topClass = attrs.setClassWhenAtTop, // get CSS class from directive's attribute value
                    offsetTop = element.offset().top; // get element's offset top relative to document
                
                $win.on('scroll', function(e) {
                    element[($win.scrollTop() >= offsetTop) ? 'addClass' : 'removeClass'](topClass);
                });
            }
        };
    })
    .directive('errSrc', function() {
        return {
            link: function(scope, element, attrs) {
                element.bind('error', function() {
                    if (attrs.src !== attrs.errSrc) {
                        attrs.$set('src', attrs.errSrc);
                    }
                });
                
                attrs.$observe('ngSrc', function(value) {
                    if (!value && attrs.errSrc) {
                        attrs.$set('src', attrs.errSrc);
                    }
                });
            }
        };
    })
    .directive('imageSrc', function() {
        return {
            link: function(scope, element, attrs) {
               // foo
            }
        };
    });
