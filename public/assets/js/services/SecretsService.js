'use strict';

var SecretsService = function($http, BASE, HelpService) { 
    return {
		rate: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/secret-rate',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		report: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/secret-report',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		favorite: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/edit-favorite',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		addBook: function(params){
			return $http({
				method: 'POST',
				url: BASE + 'api/library-add',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		removeBook: function(params){
			return $http({
				method: 'POST',
				url: BASE + 'api/library-remove',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		updateBook: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/book-update',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		moreFeeds: function(num, total) {
			var limit = HelpService.param('limit');
						
			if (!limit) {
				limit = 6;
			}
			
			if (num > limit) {
				return num < total ? parseInt(num) + 6 : total;
			} 

			return num < total 	? parseInt(limit) + 6 : total;				
			
		},
		postSecret: function(params) {
			return $http({
				method: 'POST',
				url: BASE+'api/secret-post',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		deleteSecret: function(params){
			return $http({
				method: 'POST',
				url: BASE + 'api/secret-delete',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		mySecrets: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/my-secrets',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		bookSecrets: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/secret-books',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		secretView: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/secret-view',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		}
	};
};

module.exports = SecretsService;