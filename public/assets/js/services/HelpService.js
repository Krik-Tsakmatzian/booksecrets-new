'use strict';

var HelpService = function($http, BASE) { 
    return {
		param: function (name) {
			name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
			var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
			var	results = regex.exec(location.search);
			
			return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
		},
		changeState: function(title, limit) {
			var order_by = this.param('order_by');
			var order = this.param('order');
			var type = this.param('type');
			var	History = window.History;
			
			// Check
			if (!History.enabled) {
				throw new Error('History.js is disabled');
			}
			
			var url;
			
			if (order_by && order) {
				url = '&order_by=' + order_by + '&order=' + order;
			} else if ( order_by && !order ){
				url = '&order_by=' + order_by;
			} else if ( !order_by && order ){
				url = '&order=' + order;
			} else {
				url = '';
			}

			if (type) {
				url = url + '&type=' + type;
			}
			
			History.pushState({ timestamp: (new Date().getTime())}, title, window.location.pathname + "?limit=" + limit + url);
		}
	};
};

module.exports = HelpService;