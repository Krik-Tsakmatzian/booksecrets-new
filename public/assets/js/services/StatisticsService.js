'use strict';

var StatisticsService = function($http, BASE, HelpService) { 
    return {
		booksCount: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/books-report',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		booksUsersCount: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/books-users-report',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		pubAuthorsCount: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/pub-authors-count',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		secretsCount: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/secrets-report',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		secretsReportsStats: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/secrets-stats-report',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		}
	};
};

module.exports = StatisticsService;