'use strict';

var DashboardService = function($http, BASE) { 
    return {
    	feedlist: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/feedlist',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			})
			.success(function(data){
	            console.timeEnd("XHR");
	            return data;
	        });
		},
		feedsearch: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/feedsearch',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			})
			.success(function(data){
	            console.timeEnd("XHR");
	            return data;
	        });
		},
		feedbook: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/feed-booklist',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		getBooklist: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/adv-book-search',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		bookslist: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/bookslist',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		revealSecrets: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/bookview',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		editFavorite: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/edit-favorite',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		favorites: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/favorites',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		userBookSearch: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/user-book-search',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		advBookSearch: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/adv-book-search',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		profileUpdate: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/profile-update',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		profileInfo: function(params) {
			return $http({
				method: 'POST',
				url: BASE + 'api/profile-info',
				data: $.param(params),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		}
	};
};

module.exports = DashboardService;