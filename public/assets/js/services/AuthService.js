'use strict';

var AuthService = function($http, BASE) { 
    return {
		register: function(credentials) {
			return $http({
				method: 'POST',
				url: BASE + 'api/sign-up',
				data: $.param(credentials),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		login: function(user) {
			return $http({
				method: 'POST',
				url: BASE + 'api/login',
				data: $.param(user),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		validateEmail: function(email) {
			return $http({
				method: 'POST',
				url: BASE + 'api/validate',
				data: $.param(email),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		},
		validateBook: function(string) {
			return $http({
				method: 'POST',
				url: BASE + 'api/validate-book',
				data: $.param(string),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});
		}
	};
};

module.exports = AuthService;