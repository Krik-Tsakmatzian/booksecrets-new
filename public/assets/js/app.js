'use strict';

var angular = require('angular');

// Requirements
require('../bower_components/angular-route/angular-route.min.js');
require('../bower_components/angular-toastr/dist/angular-toastr.tpls.min.js');
require('../bower_components/angular-bootstrap/ui-bootstrap.min.js');
require('../bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js');
require('../bower_components/angular-animate/angular-animate.min.js');
require('../bower_components/tg-angular-validator/dist/angular-validator.min.js');
require('../bower_components/angular-redactor/angular-redactor.js');
require('../../../node_modules/angular-switcher/dist/angular-switcher.min.js');
require('../bower_components/ng-file-upload/ng-file-upload.min.js');
require('../bower_components/angular-socialshare/dist/angular-socialshare.min.js');
require('../../../node_modules/angular-chart.js/dist/angular-chart.min.js');
require('../../../node_modules/bootstrap-ui-datetime-picker/dist/datetime-picker.min.js');
require('./angular-locale_el-gr.js');

// App dependencies injection
var myApp = angular.module('myApp', ['ngRoute', 'toastr', 'ui.bootstrap', 'ui.bootstrap.datetimepicker', 'ngAnimate', 'angularValidator', 'angular-redactor', 'switcher', 'ngFileUpload', '720kb.socialshare', 'chart.js']);

// Services
var HelpService = require('./services/HelpService');
var AuthService = require('./services/AuthService');
var DashboardService = require('./services/DashboardService');
var SecretsService = require('./services/SecretsService');
var StatisticsService = require('./services/StatisticsService');

// Directives
var resize = require('./directives/resize');
var toggleClass = require('./directives/toggleClass');
var stickyFooter = require('./directives/stickyFooter');
var setClassWhenAtTop = require('./directives/setClassWhenAtTop');

// Controllers
var AdvancedController = require('./controllers/AdvancedController');
var AuthorController = require('./controllers/AuthorController');
var BooksController = require('./controllers/BooksController');
var BookSecretsController = require('./controllers/BookSecretsController');
var FavoritesController = require('./controllers/FavoritesController');
var FeedsController = require('./controllers/FeedsController');
var FooterController = require('./controllers/FooterController');
var LibraryController = require('./controllers/LibraryController');
var MobUserController = require('./controllers/MobUserController');
var mainController = require('./controllers/mainController');
var MySecretsController = require('./controllers/MySecretsController');
var SecretController = require('./controllers/SecretController');
var StatisticsController = require('./controllers/StatisticsController');
var UserController = require('./controllers/UserController');
var LogoController = require('./controllers/LogoController');

// Services
myApp.value('BASE', window.location.origin + '/')
	.value('BASE_API', 'https://api.booksecrets.com/')
	.value('BASE_URL', 'https://api.booksecrets.com')
	.factory('HelpService', [ '$http', 'BASE', HelpService])
	.factory('AuthService', [ '$http', 'BASE', AuthService])
	.factory('DashboardService', [ '$http', 'BASE', DashboardService])
	.factory('SecretsService', [ '$http', 'BASE', 'HelpService', SecretsService])
	.factory('StatisticsService', [ '$http', 'BASE', 'HelpService', StatisticsService]);

// Directives
myApp.directive('resize', ['$window', resize])
	.directive('toggleClass', toggleClass)
	.directive('stickyFooter', ['$timeout', stickyFooter])
	.directive('setClassWhenAtTop', ['$window', setClassWhenAtTop])
	.directive('errSrc', function() {
        return {
            link: function(scope, element, attrs) {
                element.bind('error', function() {
                    if (attrs.src !== attrs.errSrc) {
                        attrs.$set('src', attrs.errSrc);
                        element.parent().css('background-image', "url('"+attrs.src+"')");
                    }
                });
                
                attrs.$observe('ngSrc', function(value) {
                    if (!value && attrs.errSrc) {
                        attrs.$set('src', attrs.errSrc);
                    }else{
                    	attrs.$set('src', attrs.ngSrc);
                    }
                    element.parent().css('background-image', "url('"+attrs.src+"')");
                });
            }
        };
    })
    .directive('whenScrolled', function() {
		return function(scope, elm, attr) {
			var raw = elm[0];
			
			elm.bind('scroll', function() {
				if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
					scope.$apply(attr.whenScrolled);
				}
			});
		};
	});
    
// Filters 
myApp.filter('orderObjectBy', function() {
        return function(items, field, reverse) {
            var filtered = [];
            
            angular.forEach(items, function(item) {
                filtered.push(item);
            });
            
            filtered.sort(function(a, b) {

                if (typeof a[field] === 'string' && typeof b[field] === 'string') {
                    
                    var nameA = a[field].toLowerCase(), nameB = b[field].toLowerCase();
                    
                    if (nameA < nameB) //sort string ascending
                        return -1 ;
                    
                    if (nameA > nameB)
                        return 1;
                    
                    return 0; //default return value (no sorting)
                    
                } else {
                    return (a[field] > b[field] ? 1 : -1);
                }
                
            });
            
            if (reverse) {
				filtered.reverse();
			}
            
            return filtered;
        };
    })
    .filter('htmlToPlaintext', function() {
        return function(text) {
        	var x = text ? text : x;			
        	
        	x = String(x).replace(' class="video-container"', '');
			x = String(x).replace('<p></p>', '');
			x = String(x).replace('<span></span>', '');
        	x = String(x).replace(/(?:\r\n|\r|\n)/g, '');
        	x = String(x).replace(/<iframe.+?<\/iframe>?|<video.+?<\/video>/g, '');
        	x = String(x).replace(/(<(?!\/)[^>]+>)+(<\/[^>]+>)+/, "");
        	x = String(x).replace(/((?!<((\/)?p))<[^>]*>)/gi,'', '');
        	        	
			return String(x).replace(/<\s*[^>]*>|<\s*\/\s*>/gm, '');
        };
    });


myApp.run(function($rootScope, SecretsService, HelpService, toastr, $sce, BASE_API, $uibModal, BASE_URL) {
		
        $rootScope.max = 5;
		$rootScope.isCollapsed = true;
		$rootScope.hideSearch = false;
			
        $rootScope.secretRate = function(id, rating) {
			var params = { 'secretId': id, 'rating': rating };
			SecretsService.rate(params)
				.success(function(data) {
					// FOO
				})
				.error(function(err) {
					toastr.error('Υπάρχει σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
				});
		};
		
		$rootScope.report = function (secret) {
			var modalInstance = $uibModal.open({
				animation: true,
				size: 'sm',
				templateUrl: '/assets/templates/reportModal.html',
				controller: function($uibModalInstance, $scope) {
				    $scope.reason = 1;
				    $scope.otherText = '';
				    
					$scope.cancel = function () {
						$uibModalInstance.dismiss('cancel');
					};
					
					$scope.sendReport = function() {                                                
                        if ($scope.reason === 4 && ($scope.otherText === '' || $scope.otherText === ' ')) {
							toastr.error('Παρακαλώ γράψτε έναν λόγο');
							return;
						} 

						var params = { secretId: secret.id, flag: 1, flagReason: $scope.reason };
						if ($scope.reason === 4) {
							params.flagText = $scope.otherText;
						}
						
						SecretsService.report(params)
							.success(function(data) {
								secret.userFlag = 1;
								toastr.success('Ευχαριστούμε για την αναφορά σας.');
								$uibModalInstance.dismiss('cancel');
							})
							.error(function(err) {
								toastr.error('Υπάρχει σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
							});
                       
					};
				}
			});
		};
		

		$rootScope.libraryBook = function(book){
			var params = book.userBookStatus === -1 
				? { 'bookId': book.id, 'userBookStatus': 0 }
				: { 'bookId': book.id };
			
			SecretsService.addBook(params) 
				.success(function(data) {
					book.userBookStatus = book.userBookStatus === -1 ? 0 : -1;
				}).error(function(err) {
					toastr.error('Υπάρχει σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
				});			
		};
		

		$rootScope.editFavorite = function(secret){
		    
			var params = { 'secretId': secret.id, 'favorite': secret.userFavorite, 'bookId': secret.book.id };
		
			params.favorite = params.favorite === 0 ? 1 : 0;		
			
			SecretsService.favorite(params)
				.success(function(data) {
					if (data.success) {
						secret.userFavorite = params.favorite;
					}
				})
				.error(function(err) {
					toastr.error('Υπάρχει σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
				});
		};
		
		$rootScope.isbn = [];
		
		$rootScope.unlock = function (secret, bookInfo) {
			var isbn = 	$rootScope.isbn[secret.id];
			
			if (typeof bookInfo !== 'undefined'){
				secret.book.isbn13 = bookInfo.isbn13;
				secret.book.isbn10 = bookInfo.isbn10;
			}
			
			if (isbn === secret.book.isbn13 || isbn === secret.book.isbn10){
				var params = { 'bookId': secret.book.id, 'userBookStatus': 0 };
				SecretsService.addBook(params) 
					.success(function(data) {
						if (data.success) {
							params.userBookStatus = 1;
							
							SecretsService.updateBook(params) 
								.success(function(data) {
									if (data.success) {
										secret.userLocked = 0;
										secret.book.userBookStatus = 1;
										toastr.success('Επιτυχώς!');
										window.location.href = '/secrets/' + secret.book.id + '/' + secret.id;
									} else {
										toastr.error(data.msg);
									}									
								})
								.error(function(err) {
									toastr.error('Υπάρχει σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
								});
						} else {
							if (data.msg === 'EXISTING-BOOK') {
								params.userBookStatus = 1;
								
								SecretsService.updateBook(params) 
									.success(function(data) {
										if (data.success) {
											secret.userLocked = 0;
											secret.book.userBookStatus = 1;
											toastr.success('Επιτυχώς!');
											window.location.href = '/secrets/' + secret.book.id + '/' + secret.id;
										} else {
											toastr.error(data.msg);
										}
										
									}).error(function(err) {
										toastr.error('Υπάρχει σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
									});
							} else {
								toastr.error(data.msg);
							}
						}
						
					})
					.error(function(err) {
						toastr.error('Υπάρχει σφάλμα. Παρακαλώ προσπαθήστε αργότερα.');
					});
			} else {
				toastr.error('Μη έγκυρο ISBN. Παρακαλώ εισάγετε νέο.');
			}
	    };
		
		
	    $rootScope.isbnValidator = function(num) {
			if (!num) {
				return;
			}

			if ( num.length == 10 || num.length == 13 ) {
				return true;
			}

			return "Ο αριθμός ISBN Πρέπει να είναι είτε 10 είτε 13 ψηφία";			
		};
	    
		$rootScope.renderHtml = function(html_code) {
            return $sce.trustAsHtml(html_code);
        };
        
        $rootScope.base = BASE_API;
        $rootScope.base_url = BASE_URL;
        
    })
    .config(['$routeProvider', '$interpolateProvider', '$locationProvider', 'redactorOptions', 'uiDatetimePickerConfig', function($routeProvider, $interpolateProvider, $locationProvider, redactorOptions, uiDatetimePickerConfig) {
            $interpolateProvider.startSymbol('<%');
            $interpolateProvider.endSymbol('%>');
            
            $locationProvider.html5Mode(true);
            // $locationProvider.hashPrefix('!');
            
            redactorOptions.imageUpload = window.location.origin + '/api/upload';
            redactorOptions.imageEditable = true;
            redactorOptions.imageResizable = true;
            redactorOptions.imageLink = true;
            
            redactorOptions.focus = true;
            redactorOptions.enterKey = true;
            redactorOptions.linebreaks = false;
            redactorOptions.cleanStyleOnEnter = true;
            
            redactorOptions.pastePlainText = true;
            redactorOptions.convertImageLinks = false;
            redactorOptions.convertVideoLinks = false;
            redactorOptions.dragImageUpload = false;
            redactorOptions.dragFileUpload = false;
            
            redactorOptions.buttonSource = true;
            redactorOptions.visual = true;// flase for html mode
            redactorOptions.toolbarFixed = false;
            redactorOptions.minHeight= 300;
            
            redactorOptions.changeCallback = function (){
                var scope = angular.element($("#textaa")).scope();
            };
            
            redactorOptions.imagecropper = {
                imageUpload: window.location.origin + '/api/upload',
                modalWidth: 700,
                buttonCropText : 'Crop',
                buttonSaveText : 'Save without cropping',
                croppingOptions : {
                    resizable : true,
                    checkOrientation: false,
                    checkCrossOrigin: false,
                    viewMode: 1,
                    strict: true,
                    crop: function(e) {
                        var cropData = {
							x: e.x,
							y: e.y,
							height: e.height,
							width: e.width
						};
						
                        $('.js-crop-form .js-crop-data').val(JSON.stringify(cropData));
                    }
                }
                
            };
            
            redactorOptions.formatting = ['p', 'blockquote', 'h1', 'h2', 'h3', 'h4', 'h5'];
            redactorOptions.plugins = ['counter','imagecropper', 'record', 'audio'];
            redactorOptions.buttons = ['formatting', 'bold', 'italic', 'link', 'unorderedlist', 'orderedlist', 'alignment']; 
            
            uiDatetimePickerConfig.todayText = 'ΣΗΜΕΡΑ';
            uiDatetimePickerConfig.nowText = 'ΤΩΡΑ';
            uiDatetimePickerConfig.closeText = 'ΤΕΛΟΣ';
            uiDatetimePickerConfig.timeText = 'ΩΡΑ';
            uiDatetimePickerConfig.dateText = 'ΗΜΕΡΑ';
    }]);
    
// Controllers    
myApp.controller('AdvancedController', AdvancedController)
	.controller('AuthorController', AuthorController)
	.controller('BooksController', BooksController)
	.controller('BookSecretsController', BookSecretsController)
	.controller('FavoritesController', FavoritesController)
	.controller('FeedsController', FeedsController)
	.controller('FooterController', FooterController)
	.controller('LibraryController', LibraryController)
	.controller('mainController', mainController)
	.controller('MobUserController', MobUserController)
	.controller('MySecretsController', MySecretsController)
	.controller('SecretController', SecretController)
	.controller('StatisticsController', StatisticsController)
	.controller('UserController', UserController)
	.controller('LogoController', LogoController);
	
AdvancedController.$inject = ['$scope', 'DashboardService', 'SecretsService', 'HelpService', 'toastr', '$rootScope', '$http'];
AuthorController.$inject = ['$scope', 'DashboardService', 'Upload', '$timeout', '$rootScope'];
BookSecretsController.$inject = ['$scope', 'DashboardService', 'SecretsService', 'toastr', '$rootScope', '$http', '$uibModal'];
BooksController.$inject = ['$scope', 'DashboardService', 'SecretsService', 'HelpService', 'toastr', '$rootScope'];
FavoritesController.$inject = ['$scope', 'DashboardService', 'SecretsService', 'HelpService', 'toastr', '$rootScope', '$http'];
FeedsController.$inject = ['$scope', '$location', 'DashboardService', 'SecretsService', 'HelpService', 'toastr', '$rootScope', '$http', '$uibModal'];
FooterController.$inject = ['$scope', '$http', '$interval', 'toastr', '$uibModal'];
LogoController.$inject = ['$scope', '$http', '$interval', 'toastr', '$uibModal'];
LibraryController.$inject = ['$scope', 'DashboardService', 'SecretsService', 'HelpService', 'toastr', '$rootScope'];
mainController.$inject = ['$scope', '$http', '$interval', 'AuthService', 'toastr', '$rootScope', '$uibModal', '$timeout'];
MobUserController.$inject = ['$scope', 'DashboardService', 'Upload', '$timeout', '$rootScope'];
MySecretsController.$inject = ['$scope', 'DashboardService', 'SecretsService', 'HelpService', 'toastr', '$rootScope', '$http'];
SecretController.$inject = ['$scope', 'DashboardService', 'SecretsService', 'toastr', '$rootScope', '$http'];
StatisticsController.$inject = ['$scope', 'StatisticsService', 'toastr', '$rootScope', '$http'];
UserController.$inject = ['$scope', 'toastr', '$http', 'HelpService'];