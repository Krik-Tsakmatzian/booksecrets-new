# Laravel and Angular Booksecrets App

## Installation

1. clone the repo
2. cd booksecrets/
2. chmod -R 777 app/storage
2. composer install
3. npm install
4. bower install
5. mkdir public/assets/uploads (if not exists)
6. chmod -R 777 public/assets/uploads
7. ./build.sh dev ( or for production ./build.sh prod )
